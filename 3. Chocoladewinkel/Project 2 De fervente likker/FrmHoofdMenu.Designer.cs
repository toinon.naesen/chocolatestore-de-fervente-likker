﻿
namespace Project_2_De_fervente_likker
{
    partial class FrmHoofdMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnVerwerking = new System.Windows.Forms.Button();
            this.btnBestellen = new System.Windows.Forms.Button();
            this.btnPersoneel = new System.Windows.Forms.Button();
            this.btnKlanten = new System.Windows.Forms.Button();
            this.btnKassa = new System.Windows.Forms.Button();
            this.btnAssortiment = new System.Windows.Forms.Button();
            this.pnlLogo = new System.Windows.Forms.Panel();
            this.pnlTitel = new System.Windows.Forms.Panel();
            this.lbTitel = new System.Windows.Forms.Label();
            this.pnlWerkPaneel = new System.Windows.Forms.Panel();
            this.pnlButtons.SuspendLayout();
            this.pnlTitel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.AutoScroll = true;
            this.pnlButtons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(121)))), ((int)(((byte)(90)))));
            this.pnlButtons.Controls.Add(this.btnSluiten);
            this.pnlButtons.Controls.Add(this.btnAbout);
            this.pnlButtons.Controls.Add(this.btnVerwerking);
            this.pnlButtons.Controls.Add(this.btnBestellen);
            this.pnlButtons.Controls.Add(this.btnPersoneel);
            this.pnlButtons.Controls.Add(this.btnKlanten);
            this.pnlButtons.Controls.Add(this.btnKassa);
            this.pnlButtons.Controls.Add(this.btnAssortiment);
            this.pnlButtons.Controls.Add(this.pnlLogo);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(199, 630);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnSluiten
            // 
            this.btnSluiten.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSluiten.FlatAppearance.BorderSize = 0;
            this.btnSluiten.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSluiten.ForeColor = System.Drawing.Color.LightGray;
            this.btnSluiten.Location = new System.Drawing.Point(0, 534);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(199, 62);
            this.btnSluiten.TabIndex = 9;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAbout.FlatAppearance.BorderSize = 0;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.ForeColor = System.Drawing.Color.LightGray;
            this.btnAbout.Location = new System.Drawing.Point(0, 472);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(199, 62);
            this.btnAbout.TabIndex = 8;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnVerwerking
            // 
            this.btnVerwerking.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVerwerking.Enabled = false;
            this.btnVerwerking.FlatAppearance.BorderSize = 0;
            this.btnVerwerking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerwerking.ForeColor = System.Drawing.Color.LightGray;
            this.btnVerwerking.Location = new System.Drawing.Point(0, 410);
            this.btnVerwerking.Name = "btnVerwerking";
            this.btnVerwerking.Size = new System.Drawing.Size(199, 62);
            this.btnVerwerking.TabIndex = 6;
            this.btnVerwerking.Text = "Verwerking";
            this.btnVerwerking.UseVisualStyleBackColor = true;
            this.btnVerwerking.Click += new System.EventHandler(this.btnVerwerking_Click);
            // 
            // btnBestellen
            // 
            this.btnBestellen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBestellen.Enabled = false;
            this.btnBestellen.FlatAppearance.BorderSize = 0;
            this.btnBestellen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBestellen.ForeColor = System.Drawing.Color.LightGray;
            this.btnBestellen.Location = new System.Drawing.Point(0, 348);
            this.btnBestellen.Name = "btnBestellen";
            this.btnBestellen.Size = new System.Drawing.Size(199, 62);
            this.btnBestellen.TabIndex = 5;
            this.btnBestellen.Text = "Bestellen";
            this.btnBestellen.UseVisualStyleBackColor = true;
            this.btnBestellen.Click += new System.EventHandler(this.btnBestellen_Click);
            // 
            // btnPersoneel
            // 
            this.btnPersoneel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPersoneel.Enabled = false;
            this.btnPersoneel.FlatAppearance.BorderSize = 0;
            this.btnPersoneel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPersoneel.ForeColor = System.Drawing.Color.LightGray;
            this.btnPersoneel.Location = new System.Drawing.Point(0, 286);
            this.btnPersoneel.Name = "btnPersoneel";
            this.btnPersoneel.Size = new System.Drawing.Size(199, 62);
            this.btnPersoneel.TabIndex = 4;
            this.btnPersoneel.Text = "Personeel";
            this.btnPersoneel.UseVisualStyleBackColor = true;
            this.btnPersoneel.Click += new System.EventHandler(this.btnPersoneel_Click);
            // 
            // btnKlanten
            // 
            this.btnKlanten.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKlanten.Enabled = false;
            this.btnKlanten.FlatAppearance.BorderSize = 0;
            this.btnKlanten.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKlanten.ForeColor = System.Drawing.Color.LightGray;
            this.btnKlanten.Location = new System.Drawing.Point(0, 224);
            this.btnKlanten.Name = "btnKlanten";
            this.btnKlanten.Size = new System.Drawing.Size(199, 62);
            this.btnKlanten.TabIndex = 3;
            this.btnKlanten.Text = "Klanten";
            this.btnKlanten.UseVisualStyleBackColor = true;
            this.btnKlanten.Click += new System.EventHandler(this.btnKlanten_Click);
            // 
            // btnKassa
            // 
            this.btnKassa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKassa.Enabled = false;
            this.btnKassa.FlatAppearance.BorderSize = 0;
            this.btnKassa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKassa.ForeColor = System.Drawing.Color.LightGray;
            this.btnKassa.Location = new System.Drawing.Point(0, 162);
            this.btnKassa.Name = "btnKassa";
            this.btnKassa.Size = new System.Drawing.Size(199, 62);
            this.btnKassa.TabIndex = 2;
            this.btnKassa.Text = "Kassa";
            this.btnKassa.UseVisualStyleBackColor = true;
            this.btnKassa.Click += new System.EventHandler(this.btnKassa_Click);
            // 
            // btnAssortiment
            // 
            this.btnAssortiment.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAssortiment.Enabled = false;
            this.btnAssortiment.FlatAppearance.BorderSize = 0;
            this.btnAssortiment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssortiment.ForeColor = System.Drawing.Color.LightGray;
            this.btnAssortiment.Location = new System.Drawing.Point(0, 100);
            this.btnAssortiment.Name = "btnAssortiment";
            this.btnAssortiment.Size = new System.Drawing.Size(199, 62);
            this.btnAssortiment.TabIndex = 1;
            this.btnAssortiment.Text = "Assortiment";
            this.btnAssortiment.UseVisualStyleBackColor = true;
            this.btnAssortiment.Click += new System.EventHandler(this.btnAssortiment_Click);
            // 
            // pnlLogo
            // 
            this.pnlLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(86)))), ((int)(((byte)(95)))));
            this.pnlLogo.BackgroundImage = global::Project_2_De_fervente_likker.Properties.Resources.De_fervente_likker_logo;
            this.pnlLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(199, 100);
            this.pnlLogo.TabIndex = 0;
            // 
            // pnlTitel
            // 
            this.pnlTitel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(131)))), ((int)(((byte)(139)))));
            this.pnlTitel.Controls.Add(this.lbTitel);
            this.pnlTitel.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitel.Location = new System.Drawing.Point(199, 0);
            this.pnlTitel.Name = "pnlTitel";
            this.pnlTitel.Size = new System.Drawing.Size(955, 100);
            this.pnlTitel.TabIndex = 1;
            // 
            // lbTitel
            // 
            this.lbTitel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbTitel.AutoSize = true;
            this.lbTitel.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbTitel.Location = new System.Drawing.Point(354, 38);
            this.lbTitel.Name = "lbTitel";
            this.lbTitel.Size = new System.Drawing.Size(48, 25);
            this.lbTitel.TabIndex = 0;
            this.lbTitel.Text = "Titel";
            // 
            // pnlWerkPaneel
            // 
            this.pnlWerkPaneel.AutoScroll = true;
            this.pnlWerkPaneel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWerkPaneel.Location = new System.Drawing.Point(199, 100);
            this.pnlWerkPaneel.Name = "pnlWerkPaneel";
            this.pnlWerkPaneel.Size = new System.Drawing.Size(955, 530);
            this.pnlWerkPaneel.TabIndex = 2;
            // 
            // FrmHoofdMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 630);
            this.Controls.Add(this.pnlWerkPaneel);
            this.Controls.Add(this.pnlTitel);
            this.Controls.Add(this.pnlButtons);
            this.Name = "FrmHoofdMenu";
            this.Text = "De Fervente Likker";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmHoofdMenu_Load);
            this.pnlButtons.ResumeLayout(false);
            this.pnlTitel.ResumeLayout(false);
            this.pnlTitel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnVerwerking;
        private System.Windows.Forms.Button btnBestellen;
        private System.Windows.Forms.Button btnPersoneel;
        private System.Windows.Forms.Button btnKlanten;
        private System.Windows.Forms.Button btnKassa;
        private System.Windows.Forms.Button btnAssortiment;
        private System.Windows.Forms.Panel pnlLogo;
        private System.Windows.Forms.Panel pnlTitel;
        private System.Windows.Forms.Label lbTitel;
        private System.Windows.Forms.Panel pnlWerkPaneel;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Button btnSluiten;
    }
}