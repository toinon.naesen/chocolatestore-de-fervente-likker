﻿namespace Project_2_De_fervente_likker
{
    public class Administrator : Gebruiker
    {
        public Administrator(string localNaam, string localPasswoord) : base(localNaam, localPasswoord)
        {
        }
        public override bool Equals(object obj)
        {
            bool check = true;


            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Administrator k = (Administrator)obj;
                if (this.Gebruikersnaam != k.Gebruikersnaam || this.Passwoord != k.Passwoord)
                {
                    check = false;
                }

            }

            return check;
        }
    }
}
