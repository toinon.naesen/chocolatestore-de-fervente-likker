﻿using System;

namespace Project_2_De_fervente_likker
{
    public class Chocolade
    {
        //declaraties
        private string _productId;
        private string _soort;
        private double _hoeveelheid;
        private DateTime _houdbaarheidsDatum;
        private double _prijsPerKg;


        //Constructor
        public Chocolade(string productId, string soort, double hoeveelheid, DateTime houdbaarheidsDatum, double prijsPerKg)
        {
            ProductId = productId;
            Soort = soort;
            Hoeveelheid = hoeveelheid;
            HoudbaarheidsDatum = houdbaarheidsDatum;
            PrijsPerKg = prijsPerKg;
        }

        //Properties
        public string ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }

        public string Soort
        {
            get { return _soort; }
            set { _soort = value; }
        }

        public double Hoeveelheid
        {
            get { return _hoeveelheid; }
            set { _hoeveelheid = value; }
        }

        public DateTime HoudbaarheidsDatum
        {
            get { return _houdbaarheidsDatum; }
            set { _houdbaarheidsDatum = value; }
        }

        public double PrijsPerKg
        {
            get { return _prijsPerKg; }
            set { _prijsPerKg = value; }
        }

        //Methode
    }
}
