﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.WebRequestMethods;
using File = System.IO.File;

namespace Project_2_De_fervente_likker
{
    public partial class FrmPersoneel : Form    
    {
        public FrmPersoneel()
        {
            InitializeComponent();
        }
        List<Personeel> mijnDoorgekregenPersoneelsLijst;
        public FrmPersoneel(List<Personeel> MijnPersoneel)
        {
            InitializeComponent();
            mijnDoorgekregenPersoneelsLijst = MijnPersoneel;
        }
        private void btnNieuwpersoneel_Click(object sender, EventArgs e)
        {
            dgvpersonneel.Rows.Add(TxtMatricule.Text, CbGender.SelectedItem, txtNaam.Text, txtVoornaam.Text, mtbGeboortedatum.Text.ToString(), cbFunctie.SelectedItem, txtStraatnaam.Text, txtHuisnummer.Text.ToString(), txtPostecode.Text, txtGemeente.Text, txtLand.Text, mtbGsmnummer.Text.ToString());
        }

        private void btnExporttxt_Click(object sender, EventArgs e)
        {
            TextWriter writer = new StreamWriter(@"C:\folder\personeel.txt");
            int rowcount = dgvpersonneel.Rows.Count;
            for (int i = 0; i < rowcount - 2; i++)
            {
                for (int j = 0; i < dgvpersonneel.Columns.Count - 1; j++)
                {
                    if (j == dgvpersonneel.Columns.Count - 1)
                    {
                        writer.Write("\t" + dgvpersonneel.Rows[i].Cells[j].Value.ToString());
                    }
                    else
                        writer.Write("\t" + dgvpersonneel.Rows[i].Cells[j].Value.ToString() + "\t" + " |");
                }
                writer.WriteLine("");
                writer.WriteLine("-------------------------------------------");
            }
            writer.Close();
            MessageBox.Show("Data exported");
        }

        private void FrmPersoneel_Load(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines(@"C:\folder\personeel.txt");
            string[] values;
            for (int i = 0; i < lines.Length; i++)
            {
                values = lines[i].Split("|");
                string[] row = new string[values.Length];
                for (int j = 0; j < values.Length; j++)
                {
                    row[j] = values[j].Trim();
                }
                dgvpersonneel.Rows.Add(row);
            }

        }

        private void btnWissenpersoneel_Click(object sender, EventArgs e)
        {
            int personeeltodelete = dgvpersonneel.CurrentCell.RowIndex;
            dgvpersonneel.Rows.RemoveAt(personeeltodelete);
        }
        int indice = -1;
        private void btnUpdatepersoneel_Click(object sender, EventArgs e)
        {
            if (indice == -1)
            {
                indice = dgvpersonneel.CurrentCell.RowIndex;
                TxtMatricule.Text = dgvpersonneel.Rows[indice].Cells[0].ToString();
                CbGender.Text = dgvpersonneel.Rows[indice].Cells[1].ToString();
                txtNaam.Text = dgvpersonneel.Rows[indice].Cells[2].ToString();
                txtVoornaam.Text = dgvpersonneel.Rows[indice].Cells[3].ToString();
                mtbGeboortedatum.Text = dgvpersonneel.Rows[indice].Cells[4].ToString();
                CbGender.Text = dgvpersonneel.Rows[indice].Cells[5].ToString();
                txtStraatnaam.Text = dgvpersonneel.Rows[indice].Cells[6].ToString();
                txtHuisnummer.Text = dgvpersonneel.Rows[indice].Cells[7].ToString();
                txtGemeente.Text = dgvpersonneel.Rows[indice].Cells[8].ToString();
                txtLand.Text = dgvpersonneel.Rows[indice].Cells[10].ToString();
                mtbGsmnummer.Text = dgvpersonneel.Rows[indice].Cells[11].ToString();
                btnWissenpersoneel.Enabled = false;
                btnNieuwpersoneel.Enabled = false;
                btnExporttxt.Enabled = false;
                btnNieuwpersoneel.Enabled = false;

            }
        }

        private void btnUpdatepersoneel_Click_1(object sender, EventArgs e)
        {

        }

        private void btnShowpersoneel_Click(object sender, EventArgs e)
        {

        }
    }
}
