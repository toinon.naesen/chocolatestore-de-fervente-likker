﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Project_2_De_fervente_likker
{
    public partial class FrmKlantMenu : Form
    {
        public FrmKlantMenu()
        {
            InitializeComponent();
        }
        List<Klant> mijnDoorgekregenKlantenlijst;
        public FrmKlantMenu(List<Klant> MijnKlant)
        {
            InitializeComponent();
            mijnDoorgekregenKlantenlijst = MijnKlant;
        }
        
        List<Klant> mijnTempZoekLijst = new List<Klant>();
        Klant tempKlant;
        private void FrmKlantMenu_Load(object sender, EventArgs e)
        {

            if (mijnDoorgekregenKlantenlijst != null)
            {
                foreach (Klant item in mijnDoorgekregenKlantenlijst)
                {
                    lbZoekresult.Items.Add(item);
                }
            }

        }


        

        private void btnAanmaken_Click(object sender, EventArgs e)
        {
            FrmKlantAanmaken mijnAanmaakFormulier = new FrmKlantAanmaken(mijnDoorgekregenKlantenlijst);

            mijnAanmaakFormulier.ShowDialog();
            UpdateKlanten();
        }
        public void UpdateKlanten()
        {
            lbZoekresult.Items.Clear();
            foreach (Klant item in mijnDoorgekregenKlantenlijst)
            {
                lbZoekresult.Items.Add(item);
            }
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            if (lbZoekresult.SelectedIndex != -1)
            {
                MessageBox.Show(mijnDoorgekregenKlantenlijst[lbZoekresult.SelectedIndex].DetailsKlanten(), "Details Klanten", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Gelieve iets aan te duiden op de lijst om de details ervan te bekijken.", "Kan geen details van leegte bekijken.", MessageBoxButtons.OK);
            }
        }

        private void btnVerwijderen_Click(object sender, EventArgs e)
        {
            if (lbZoekresult.SelectedIndex != -1)
            {
                if (MessageBox.Show($"Zeker dat je dit wil verwijderen?: {mijnDoorgekregenKlantenlijst[lbZoekresult.SelectedIndex].Voornaam} {mijnDoorgekregenKlantenlijst[lbZoekresult.SelectedIndex].Achternaam}?", "Verwijderen?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    mijnDoorgekregenKlantenlijst.Remove(mijnDoorgekregenKlantenlijst[lbZoekresult.SelectedIndex]);
                }
            }
            else
            {
                MessageBox.Show("Gelieve iets aan te duiden op de lijst om te verwijderen.", "Kan geen leegte verwijderen.", MessageBoxButtons.OK);
            }
            UpdateKlanten();
        }

        private void btnZoeken_Click(object sender, EventArgs e)
        {
            int Id, naamOfID = 0;
            if (txtVoornaam.Text.Any(char.IsDigit) || txtAchternaam.Text.Any(char.IsDigit))
            {
                MessageBox.Show("Voornaam of achternaam kunnen geen numerieke input hebben. Tenzij je vader Elon Musk is.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (string.IsNullOrEmpty(txtVoornaam.Text) && string.IsNullOrEmpty(txtAchternaam.Text) && string.IsNullOrEmpty(txtKlantID.Text))
            {
                MessageBox.Show("Voornaam, achternaam of klantID kunnen niet allemaal leeg zijn. Vul minstens 1 zoekwaarde in.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!string.IsNullOrEmpty(txtKlantID.Text) && !int.TryParse(txtKlantID.Text, out Id))
            {
                MessageBox.Show("ID kan alleen numeriek zijn.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else // Welk object aanmaken adhv info gegeven
            {
                
                if (string.IsNullOrEmpty(txtVoornaam.Text) && string.IsNullOrEmpty(txtAchternaam.Text) && !string.IsNullOrEmpty(txtKlantID.Text) && int.TryParse(txtKlantID.Text, out Id))
                {
                    tempKlant = new Klant("", "", "", "", "", "", "", 0, 0, "", Id, "", ""); // id
                    naamOfID = 2;
                }
                else if (!string.IsNullOrEmpty(txtVoornaam.Text) && !string.IsNullOrEmpty(txtAchternaam.Text) && string.IsNullOrEmpty(txtKlantID.Text))
                {
                    tempKlant = new Klant(txtVoornaam.Text, txtAchternaam.Text, "", "", "", "", "", 0, 0, "", 0, "", ""); // voornaam + achternaam
                    naamOfID = 1;
                }
                else
                {
                    MessageBox.Show("Gelieve op naam OF Id te zoeken, niet allebij.", "Error double find.", MessageBoxButtons.OK);
                    
                }

                if (naamOfID > 0)
                {
                    mijnTempZoekLijst.Clear();
                    for (int i = 0; i < mijnDoorgekregenKlantenlijst.Count; i++) // Check voor matches
                    {
                        if (tempKlant.Equals(mijnDoorgekregenKlantenlijst[i], naamOfID))
                        {
                            mijnTempZoekLijst.Add(mijnDoorgekregenKlantenlijst[i]);
                        }
                    }
                    
                }
                if (mijnTempZoekLijst.Count == 0)
                {
                    MessageBox.Show("Er werd niemand gevonden die voldaan aan de criteria.", "Error 7: Niemand gevonden.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    lbZoekresult.Items.Clear();
                    foreach (Klant item in mijnTempZoekLijst)
                    {
                        lbZoekresult.Items.Add(item);
                    }
                }

            }
        }

        private void btnZoekClear_Click(object sender, EventArgs e)
        {
            txtAchternaam.Text = "";
            txtVoornaam.Text = "";
            txtKlantID.Text = "";
            UpdateKlanten();
        }

        private void btnWijzigen_Click(object sender, EventArgs e)
        {

            if (lbZoekresult.SelectedIndex != -1)
            {
                for (int i = 0; i < mijnDoorgekregenKlantenlijst.Count; i++)
                {
                    if (lbZoekresult.SelectedItem.Equals(mijnDoorgekregenKlantenlijst[i]))
                    {
                        FrmWijzigenKlant mijnWijzigen = new FrmWijzigenKlant(mijnDoorgekregenKlantenlijst, i);
                        mijnWijzigen.ShowDialog();
                    }
                }
            }
            else
            {
                MessageBox.Show("Gelieve iets aan te duiden op de lijst om te wijzigen.", "Kan geen leegte wijzigen.", MessageBoxButtons.OK);
            }
            
            
        }
    }
}
