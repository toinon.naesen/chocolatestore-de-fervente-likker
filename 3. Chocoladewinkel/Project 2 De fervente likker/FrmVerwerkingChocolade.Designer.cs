﻿
namespace Project_2_De_fervente_likker
{
    partial class FrmVerwerkingChocolade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbWeergaveStock = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbGoudenEi = new System.Windows.Forms.CheckBox();
            this.cbPuzzel = new System.Windows.Forms.CheckBox();
            this.cbGereedschapset = new System.Windows.Forms.CheckBox();
            this.cbAsperge = new System.Windows.Forms.CheckBox();
            this.cbVoetbalschoen = new System.Windows.Forms.CheckBox();
            this.cbEendDucky = new System.Windows.Forms.CheckBox();
            this.btnVerwerk = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTruffels = new System.Windows.Forms.Label();
            this.lblTablets = new System.Windows.Forms.Label();
            this.lblPralines = new System.Windows.Forms.Label();
            this.lblVentjes = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAantalGewenst = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbMixTablet = new System.Windows.Forms.CheckBox();
            this.cbWitHazelnoot = new System.Windows.Forms.CheckBox();
            this.cbWitGepofteRijst = new System.Windows.Forms.CheckBox();
            this.cb55Sinaasappel = new System.Windows.Forms.CheckBox();
            this.cb55Puur = new System.Windows.Forms.CheckBox();
            this.cb70Puur = new System.Windows.Forms.CheckBox();
            this.cb85Puur = new System.Windows.Forms.CheckBox();
            this.cb30MelkGezoutKaramel = new System.Windows.Forms.CheckBox();
            this.cb30MelkHazelnoot = new System.Windows.Forms.CheckBox();
            this.cb30Melk = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbRuby = new System.Windows.Forms.CheckBox();
            this.cbHartjeFram = new System.Windows.Forms.CheckBox();
            this.cbManon = new System.Windows.Forms.CheckBox();
            this.cbDuettoYuzu = new System.Windows.Forms.CheckBox();
            this.cbBûche = new System.Windows.Forms.CheckBox();
            this.cbEveVanille = new System.Windows.Forms.CheckBox();
            this.cbCerisette = new System.Windows.Forms.CheckBox();
            this.cbAdvocaat = new System.Windows.Forms.CheckBox();
            this.cbFinesse = new System.Windows.Forms.CheckBox();
            this.cbDéliceKokosnoot = new System.Windows.Forms.CheckBox();
            this.cbAlexanderSinaasappel = new System.Windows.Forms.CheckBox();
            this.cbLikeurPraline = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbGroeneThee = new System.Windows.Forms.CheckBox();
            this.cbLimoenKokos = new System.Windows.Forms.CheckBox();
            this.cbFrambozen = new System.Windows.Forms.CheckBox();
            this.cbGember = new System.Windows.Forms.CheckBox();
            this.cbCappuccino = new System.Windows.Forms.CheckBox();
            this.cbClassicDark = new System.Windows.Forms.CheckBox();
            this.cbClassicMelk = new System.Windows.Forms.CheckBox();
            this.cbKaramel = new System.Windows.Forms.CheckBox();
            this.cbPecannoot = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbWeergaveStock
            // 
            this.lbWeergaveStock.FormattingEnabled = true;
            this.lbWeergaveStock.ItemHeight = 15;
            this.lbWeergaveStock.Location = new System.Drawing.Point(13, 13);
            this.lbWeergaveStock.Name = "lbWeergaveStock";
            this.lbWeergaveStock.Size = new System.Drawing.Size(235, 289);
            this.lbWeergaveStock.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbGoudenEi);
            this.groupBox1.Controls.Add(this.cbPuzzel);
            this.groupBox1.Controls.Add(this.cbGereedschapset);
            this.groupBox1.Controls.Add(this.cbAsperge);
            this.groupBox1.Controls.Add(this.cbVoetbalschoen);
            this.groupBox1.Controls.Add(this.cbEendDucky);
            this.groupBox1.Location = new System.Drawing.Point(6, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(121, 179);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Figuur";
            // 
            // cbGoudenEi
            // 
            this.cbGoudenEi.AutoSize = true;
            this.cbGoudenEi.Location = new System.Drawing.Point(7, 153);
            this.cbGoudenEi.Name = "cbGoudenEi";
            this.cbGoudenEi.Size = new System.Drawing.Size(80, 19);
            this.cbGoudenEi.TabIndex = 5;
            this.cbGoudenEi.Text = "Gouden ei";
            this.cbGoudenEi.UseVisualStyleBackColor = true;
            // 
            // cbPuzzel
            // 
            this.cbPuzzel.AutoSize = true;
            this.cbPuzzel.Location = new System.Drawing.Point(7, 127);
            this.cbPuzzel.Name = "cbPuzzel";
            this.cbPuzzel.Size = new System.Drawing.Size(59, 19);
            this.cbPuzzel.TabIndex = 4;
            this.cbPuzzel.Text = "Puzzel";
            this.cbPuzzel.UseVisualStyleBackColor = true;
            // 
            // cbGereedschapset
            // 
            this.cbGereedschapset.AutoSize = true;
            this.cbGereedschapset.Location = new System.Drawing.Point(7, 101);
            this.cbGereedschapset.Name = "cbGereedschapset";
            this.cbGereedschapset.Size = new System.Drawing.Size(109, 19);
            this.cbGereedschapset.TabIndex = 3;
            this.cbGereedschapset.Text = "Gereedschapset";
            this.cbGereedschapset.UseVisualStyleBackColor = true;
            // 
            // cbAsperge
            // 
            this.cbAsperge.AutoSize = true;
            this.cbAsperge.Location = new System.Drawing.Point(7, 75);
            this.cbAsperge.Name = "cbAsperge";
            this.cbAsperge.Size = new System.Drawing.Size(69, 19);
            this.cbAsperge.TabIndex = 2;
            this.cbAsperge.Text = "Asperge";
            this.cbAsperge.UseVisualStyleBackColor = true;
            // 
            // cbVoetbalschoen
            // 
            this.cbVoetbalschoen.AutoSize = true;
            this.cbVoetbalschoen.Location = new System.Drawing.Point(7, 49);
            this.cbVoetbalschoen.Name = "cbVoetbalschoen";
            this.cbVoetbalschoen.Size = new System.Drawing.Size(103, 19);
            this.cbVoetbalschoen.TabIndex = 1;
            this.cbVoetbalschoen.Text = "Voetbalschoen";
            this.cbVoetbalschoen.UseVisualStyleBackColor = true;
            // 
            // cbEendDucky
            // 
            this.cbEendDucky.AutoSize = true;
            this.cbEendDucky.Location = new System.Drawing.Point(7, 23);
            this.cbEendDucky.Name = "cbEendDucky";
            this.cbEendDucky.Size = new System.Drawing.Size(88, 19);
            this.cbEendDucky.TabIndex = 0;
            this.cbEendDucky.Text = "Eend Ducky";
            this.cbEendDucky.UseVisualStyleBackColor = true;
            // 
            // btnVerwerk
            // 
            this.btnVerwerk.Location = new System.Drawing.Point(130, 358);
            this.btnVerwerk.Name = "btnVerwerk";
            this.btnVerwerk.Size = new System.Drawing.Size(100, 41);
            this.btnVerwerk.TabIndex = 4;
            this.btnVerwerk.Text = "Verwerk";
            this.btnVerwerk.UseVisualStyleBackColor = true;
            this.btnVerwerk.Click += new System.EventHandler(this.btnVerwerk_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblTruffels);
            this.groupBox2.Controls.Add(this.lblTablets);
            this.groupBox2.Controls.Add(this.lblPralines);
            this.groupBox2.Controls.Add(this.lblVentjes);
            this.groupBox2.Location = new System.Drawing.Point(13, 308);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(101, 179);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Aantal in stock";
            // 
            // lblTruffels
            // 
            this.lblTruffels.AutoSize = true;
            this.lblTruffels.Location = new System.Drawing.Point(7, 102);
            this.lblTruffels.Name = "lblTruffels";
            this.lblTruffels.Size = new System.Drawing.Size(57, 15);
            this.lblTruffels.TabIndex = 3;
            this.lblTruffels.Text = "Truffels: 0";
            // 
            // lblTablets
            // 
            this.lblTablets.AutoSize = true;
            this.lblTablets.Location = new System.Drawing.Point(7, 76);
            this.lblTablets.Name = "lblTablets";
            this.lblTablets.Size = new System.Drawing.Size(55, 15);
            this.lblTablets.TabIndex = 2;
            this.lblTablets.Text = "Tablets: 0";
            // 
            // lblPralines
            // 
            this.lblPralines.AutoSize = true;
            this.lblPralines.Location = new System.Drawing.Point(7, 50);
            this.lblPralines.Name = "lblPralines";
            this.lblPralines.Size = new System.Drawing.Size(60, 15);
            this.lblPralines.TabIndex = 1;
            this.lblPralines.Text = "Pralines: 0";
            // 
            // lblVentjes
            // 
            this.lblVentjes.AutoSize = true;
            this.lblVentjes.Location = new System.Drawing.Point(7, 24);
            this.lblVentjes.Name = "lblVentjes";
            this.lblVentjes.Size = new System.Drawing.Size(56, 15);
            this.lblVentjes.TabIndex = 0;
            this.lblVentjes.Text = "Ventjes: 0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(130, 308);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Aantal gewenst:";
            // 
            // txtAantalGewenst
            // 
            this.txtAantalGewenst.Location = new System.Drawing.Point(130, 326);
            this.txtAantalGewenst.Name = "txtAantalGewenst";
            this.txtAantalGewenst.Size = new System.Drawing.Size(100, 23);
            this.txtAantalGewenst.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbMixTablet);
            this.groupBox3.Controls.Add(this.cbWitHazelnoot);
            this.groupBox3.Controls.Add(this.cbWitGepofteRijst);
            this.groupBox3.Controls.Add(this.cb55Sinaasappel);
            this.groupBox3.Controls.Add(this.cb55Puur);
            this.groupBox3.Controls.Add(this.cb70Puur);
            this.groupBox3.Controls.Add(this.cb85Puur);
            this.groupBox3.Controls.Add(this.cb30MelkGezoutKaramel);
            this.groupBox3.Controls.Add(this.cb30MelkHazelnoot);
            this.groupBox3.Controls.Add(this.cb30Melk);
            this.groupBox3.Location = new System.Drawing.Point(134, 22);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 288);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tablet 100g";
            // 
            // cbMixTablet
            // 
            this.cbMixTablet.AutoSize = true;
            this.cbMixTablet.Location = new System.Drawing.Point(6, 253);
            this.cbMixTablet.Name = "cbMixTablet";
            this.cbMixTablet.Size = new System.Drawing.Size(165, 19);
            this.cbMixTablet.TabIndex = 9;
            this.cbMixTablet.Text = "Mix Tablet - 33% verdeling";
            this.cbMixTablet.UseVisualStyleBackColor = true;
            // 
            // cbWitHazelnoot
            // 
            this.cbWitHazelnoot.AutoSize = true;
            this.cbWitHazelnoot.Location = new System.Drawing.Point(7, 228);
            this.cbWitHazelnoot.Name = "cbWitHazelnoot";
            this.cbWitHazelnoot.Size = new System.Drawing.Size(109, 19);
            this.cbWitHazelnoot.TabIndex = 8;
            this.cbWitHazelnoot.Text = "Wit - Hazelnoot";
            this.cbWitHazelnoot.UseVisualStyleBackColor = true;
            // 
            // cbWitGepofteRijst
            // 
            this.cbWitGepofteRijst.AutoSize = true;
            this.cbWitGepofteRijst.Location = new System.Drawing.Point(7, 203);
            this.cbWitGepofteRijst.Name = "cbWitGepofteRijst";
            this.cbWitGepofteRijst.Size = new System.Drawing.Size(119, 19);
            this.cbWitGepofteRijst.TabIndex = 7;
            this.cbWitGepofteRijst.Text = "Wit - Gepofte rijst";
            this.cbWitGepofteRijst.UseVisualStyleBackColor = true;
            // 
            // cb55Sinaasappel
            // 
            this.cb55Sinaasappel.AutoSize = true;
            this.cb55Sinaasappel.Location = new System.Drawing.Point(7, 178);
            this.cb55Sinaasappel.Name = "cb55Sinaasappel";
            this.cb55Sinaasappel.Size = new System.Drawing.Size(149, 19);
            this.cb55Sinaasappel.TabIndex = 6;
            this.cb55Sinaasappel.Text = "55% Puur - Sinaasappel";
            this.cb55Sinaasappel.UseVisualStyleBackColor = true;
            // 
            // cb55Puur
            // 
            this.cb55Puur.AutoSize = true;
            this.cb55Puur.Location = new System.Drawing.Point(7, 153);
            this.cb55Puur.Name = "cb55Puur";
            this.cb55Puur.Size = new System.Drawing.Size(76, 19);
            this.cb55Puur.TabIndex = 5;
            this.cb55Puur.Text = "55% Puur";
            this.cb55Puur.UseVisualStyleBackColor = true;
            // 
            // cb70Puur
            // 
            this.cb70Puur.AutoSize = true;
            this.cb70Puur.Location = new System.Drawing.Point(7, 127);
            this.cb70Puur.Name = "cb70Puur";
            this.cb70Puur.Size = new System.Drawing.Size(76, 19);
            this.cb70Puur.TabIndex = 4;
            this.cb70Puur.Text = "70% Puur";
            this.cb70Puur.UseVisualStyleBackColor = true;
            // 
            // cb85Puur
            // 
            this.cb85Puur.AutoSize = true;
            this.cb85Puur.Location = new System.Drawing.Point(7, 102);
            this.cb85Puur.Name = "cb85Puur";
            this.cb85Puur.Size = new System.Drawing.Size(76, 19);
            this.cb85Puur.TabIndex = 3;
            this.cb85Puur.Text = "85% Puur";
            this.cb85Puur.UseVisualStyleBackColor = true;
            // 
            // cb30MelkGezoutKaramel
            // 
            this.cb30MelkGezoutKaramel.AutoSize = true;
            this.cb30MelkGezoutKaramel.Location = new System.Drawing.Point(7, 76);
            this.cb30MelkGezoutKaramel.Name = "cb30MelkGezoutKaramel";
            this.cb30MelkGezoutKaramel.Size = new System.Drawing.Size(184, 19);
            this.cb30MelkGezoutKaramel.TabIndex = 2;
            this.cb30MelkGezoutKaramel.Text = "30% Melk - Gezouten Karamel";
            this.cb30MelkGezoutKaramel.UseVisualStyleBackColor = true;
            // 
            // cb30MelkHazelnoot
            // 
            this.cb30MelkHazelnoot.AutoSize = true;
            this.cb30MelkHazelnoot.Location = new System.Drawing.Point(6, 50);
            this.cb30MelkHazelnoot.Name = "cb30MelkHazelnoot";
            this.cb30MelkHazelnoot.Size = new System.Drawing.Size(142, 19);
            this.cb30MelkHazelnoot.TabIndex = 1;
            this.cb30MelkHazelnoot.Text = "30% Melk - Hazelnoot";
            this.cb30MelkHazelnoot.UseVisualStyleBackColor = true;
            // 
            // cb30Melk
            // 
            this.cb30Melk.AutoSize = true;
            this.cb30Melk.Location = new System.Drawing.Point(7, 23);
            this.cb30Melk.Name = "cb30Melk";
            this.cb30Melk.Size = new System.Drawing.Size(77, 19);
            this.cb30Melk.TabIndex = 0;
            this.cb30Melk.Text = "30% Melk";
            this.cb30Melk.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbRuby);
            this.groupBox4.Controls.Add(this.cbHartjeFram);
            this.groupBox4.Controls.Add(this.cbManon);
            this.groupBox4.Controls.Add(this.cbDuettoYuzu);
            this.groupBox4.Controls.Add(this.cbBûche);
            this.groupBox4.Controls.Add(this.cbEveVanille);
            this.groupBox4.Controls.Add(this.cbCerisette);
            this.groupBox4.Controls.Add(this.cbAdvocaat);
            this.groupBox4.Controls.Add(this.cbFinesse);
            this.groupBox4.Controls.Add(this.cbDéliceKokosnoot);
            this.groupBox4.Controls.Add(this.cbAlexanderSinaasappel);
            this.groupBox4.Controls.Add(this.cbLikeurPraline);
            this.groupBox4.Location = new System.Drawing.Point(341, 23);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(176, 334);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pralines";
            // 
            // cbRuby
            // 
            this.cbRuby.AutoSize = true;
            this.cbRuby.Location = new System.Drawing.Point(6, 303);
            this.cbRuby.Name = "cbRuby";
            this.cbRuby.Size = new System.Drawing.Size(53, 19);
            this.cbRuby.TabIndex = 18;
            this.cbRuby.Text = "Ruby";
            this.cbRuby.UseVisualStyleBackColor = true;
            // 
            // cbHartjeFram
            // 
            this.cbHartjeFram.AutoSize = true;
            this.cbHartjeFram.Location = new System.Drawing.Point(6, 278);
            this.cbHartjeFram.Name = "cbHartjeFram";
            this.cbHartjeFram.Size = new System.Drawing.Size(163, 19);
            this.cbHartjeFram.TabIndex = 13;
            this.cbHartjeFram.Text = "Hartje frambozenganache";
            this.cbHartjeFram.UseVisualStyleBackColor = true;
            // 
            // cbManon
            // 
            this.cbManon.AutoSize = true;
            this.cbManon.Location = new System.Drawing.Point(6, 253);
            this.cbManon.Name = "cbManon";
            this.cbManon.Size = new System.Drawing.Size(89, 19);
            this.cbManon.TabIndex = 17;
            this.cbManon.Text = "Manon café";
            this.cbManon.UseVisualStyleBackColor = true;
            // 
            // cbDuettoYuzu
            // 
            this.cbDuettoYuzu.AutoSize = true;
            this.cbDuettoYuzu.Location = new System.Drawing.Point(6, 228);
            this.cbDuettoYuzu.Name = "cbDuettoYuzu";
            this.cbDuettoYuzu.Size = new System.Drawing.Size(151, 19);
            this.cbDuettoYuzu.TabIndex = 16;
            this.cbDuettoYuzu.Text = "Duetto yuzu drakenfruit";
            this.cbDuettoYuzu.UseVisualStyleBackColor = true;
            // 
            // cbBûche
            // 
            this.cbBûche.AutoSize = true;
            this.cbBûche.Location = new System.Drawing.Point(6, 203);
            this.cbBûche.Name = "cbBûche";
            this.cbBûche.Size = new System.Drawing.Size(59, 19);
            this.cbBûche.TabIndex = 15;
            this.cbBûche.Text = "Bûche";
            this.cbBûche.UseVisualStyleBackColor = true;
            // 
            // cbEveVanille
            // 
            this.cbEveVanille.AutoSize = true;
            this.cbEveVanille.Location = new System.Drawing.Point(6, 178);
            this.cbEveVanille.Name = "cbEveVanille";
            this.cbEveVanille.Size = new System.Drawing.Size(81, 19);
            this.cbEveVanille.TabIndex = 14;
            this.cbEveVanille.Text = "Eve vanille";
            this.cbEveVanille.UseVisualStyleBackColor = true;
            // 
            // cbCerisette
            // 
            this.cbCerisette.AutoSize = true;
            this.cbCerisette.Location = new System.Drawing.Point(6, 153);
            this.cbCerisette.Name = "cbCerisette";
            this.cbCerisette.Size = new System.Drawing.Size(137, 19);
            this.cbCerisette.TabIndex = 13;
            this.cbCerisette.Text = "Cerisette likeurcrème";
            this.cbCerisette.UseVisualStyleBackColor = true;
            // 
            // cbAdvocaat
            // 
            this.cbAdvocaat.AutoSize = true;
            this.cbAdvocaat.Location = new System.Drawing.Point(6, 127);
            this.cbAdvocaat.Name = "cbAdvocaat";
            this.cbAdvocaat.Size = new System.Drawing.Size(109, 19);
            this.cbAdvocaat.TabIndex = 13;
            this.cbAdvocaat.Text = "Advocaat (55%)";
            this.cbAdvocaat.UseVisualStyleBackColor = true;
            // 
            // cbFinesse
            // 
            this.cbFinesse.AutoSize = true;
            this.cbFinesse.Location = new System.Drawing.Point(6, 102);
            this.cbFinesse.Name = "cbFinesse";
            this.cbFinesse.Size = new System.Drawing.Size(64, 19);
            this.cbFinesse.TabIndex = 3;
            this.cbFinesse.Text = "Finesse";
            this.cbFinesse.UseVisualStyleBackColor = true;
            // 
            // cbDéliceKokosnoot
            // 
            this.cbDéliceKokosnoot.AutoSize = true;
            this.cbDéliceKokosnoot.Location = new System.Drawing.Point(6, 76);
            this.cbDéliceKokosnoot.Name = "cbDéliceKokosnoot";
            this.cbDéliceKokosnoot.Size = new System.Drawing.Size(117, 19);
            this.cbDéliceKokosnoot.TabIndex = 2;
            this.cbDéliceKokosnoot.Text = "Délice kokosnoot";
            this.cbDéliceKokosnoot.UseVisualStyleBackColor = true;
            // 
            // cbAlexanderSinaasappel
            // 
            this.cbAlexanderSinaasappel.AutoSize = true;
            this.cbAlexanderSinaasappel.Location = new System.Drawing.Point(6, 50);
            this.cbAlexanderSinaasappel.Name = "cbAlexanderSinaasappel";
            this.cbAlexanderSinaasappel.Size = new System.Drawing.Size(143, 19);
            this.cbAlexanderSinaasappel.TabIndex = 1;
            this.cbAlexanderSinaasappel.Text = "Alexander sinaasappel";
            this.cbAlexanderSinaasappel.UseVisualStyleBackColor = true;
            // 
            // cbLikeurPraline
            // 
            this.cbLikeurPraline.AutoSize = true;
            this.cbLikeurPraline.Location = new System.Drawing.Point(6, 23);
            this.cbLikeurPraline.Name = "cbLikeurPraline";
            this.cbLikeurPraline.Size = new System.Drawing.Size(97, 19);
            this.cbLikeurPraline.TabIndex = 0;
            this.cbLikeurPraline.Text = "Likeur praline";
            this.cbLikeurPraline.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbGroeneThee);
            this.groupBox5.Controls.Add(this.cbLimoenKokos);
            this.groupBox5.Controls.Add(this.cbFrambozen);
            this.groupBox5.Controls.Add(this.cbGember);
            this.groupBox5.Controls.Add(this.cbCappuccino);
            this.groupBox5.Controls.Add(this.cbClassicDark);
            this.groupBox5.Controls.Add(this.cbClassicMelk);
            this.groupBox5.Controls.Add(this.cbKaramel);
            this.groupBox5.Controls.Add(this.cbPecannoot);
            this.groupBox5.Location = new System.Drawing.Point(524, 23);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(186, 287);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Truffels";
            // 
            // cbGroeneThee
            // 
            this.cbGroeneThee.AutoSize = true;
            this.cbGroeneThee.Location = new System.Drawing.Point(6, 228);
            this.cbGroeneThee.Name = "cbGroeneThee";
            this.cbGroeneThee.Size = new System.Drawing.Size(92, 19);
            this.cbGroeneThee.TabIndex = 27;
            this.cbGroeneThee.Text = "Groene Thee";
            this.cbGroeneThee.UseVisualStyleBackColor = true;
            // 
            // cbLimoenKokos
            // 
            this.cbLimoenKokos.AutoSize = true;
            this.cbLimoenKokos.Location = new System.Drawing.Point(6, 203);
            this.cbLimoenKokos.Name = "cbLimoenKokos";
            this.cbLimoenKokos.Size = new System.Drawing.Size(117, 19);
            this.cbLimoenKokos.TabIndex = 26;
            this.cbLimoenKokos.Text = "Limoen en Kokos";
            this.cbLimoenKokos.UseVisualStyleBackColor = true;
            // 
            // cbFrambozen
            // 
            this.cbFrambozen.AutoSize = true;
            this.cbFrambozen.Location = new System.Drawing.Point(6, 178);
            this.cbFrambozen.Name = "cbFrambozen";
            this.cbFrambozen.Size = new System.Drawing.Size(85, 19);
            this.cbFrambozen.TabIndex = 25;
            this.cbFrambozen.Text = "Frambozen";
            this.cbFrambozen.UseVisualStyleBackColor = true;
            // 
            // cbGember
            // 
            this.cbGember.AutoSize = true;
            this.cbGember.Location = new System.Drawing.Point(6, 152);
            this.cbGember.Name = "cbGember";
            this.cbGember.Size = new System.Drawing.Size(68, 19);
            this.cbGember.TabIndex = 24;
            this.cbGember.Text = "Gember";
            this.cbGember.UseVisualStyleBackColor = true;
            // 
            // cbCappuccino
            // 
            this.cbCappuccino.AutoSize = true;
            this.cbCappuccino.Location = new System.Drawing.Point(6, 126);
            this.cbCappuccino.Name = "cbCappuccino";
            this.cbCappuccino.Size = new System.Drawing.Size(90, 19);
            this.cbCappuccino.TabIndex = 23;
            this.cbCappuccino.Text = "Cappuccino";
            this.cbCappuccino.UseVisualStyleBackColor = true;
            // 
            // cbClassicDark
            // 
            this.cbClassicDark.AutoSize = true;
            this.cbClassicDark.Location = new System.Drawing.Point(6, 102);
            this.cbClassicDark.Name = "cbClassicDark";
            this.cbClassicDark.Size = new System.Drawing.Size(89, 19);
            this.cbClassicDark.TabIndex = 22;
            this.cbClassicDark.Text = "Classic Dark";
            this.cbClassicDark.UseVisualStyleBackColor = true;
            // 
            // cbClassicMelk
            // 
            this.cbClassicMelk.AutoSize = true;
            this.cbClassicMelk.Location = new System.Drawing.Point(6, 76);
            this.cbClassicMelk.Name = "cbClassicMelk";
            this.cbClassicMelk.Size = new System.Drawing.Size(91, 19);
            this.cbClassicMelk.TabIndex = 21;
            this.cbClassicMelk.Text = "Classic Melk";
            this.cbClassicMelk.UseVisualStyleBackColor = true;
            // 
            // cbKaramel
            // 
            this.cbKaramel.AutoSize = true;
            this.cbKaramel.Location = new System.Drawing.Point(6, 50);
            this.cbKaramel.Name = "cbKaramel";
            this.cbKaramel.Size = new System.Drawing.Size(69, 19);
            this.cbKaramel.TabIndex = 20;
            this.cbKaramel.Text = "Karamel";
            this.cbKaramel.UseVisualStyleBackColor = true;
            // 
            // cbPecannoot
            // 
            this.cbPecannoot.AutoSize = true;
            this.cbPecannoot.Location = new System.Drawing.Point(6, 23);
            this.cbPecannoot.Name = "cbPecannoot";
            this.cbPecannoot.Size = new System.Drawing.Size(83, 19);
            this.cbPecannoot.TabIndex = 19;
            this.cbPecannoot.Text = "Pecannoot";
            this.cbPecannoot.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox1);
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Controls.Add(this.groupBox3);
            this.groupBox6.Controls.Add(this.groupBox4);
            this.groupBox6.Location = new System.Drawing.Point(274, 13);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(727, 368);
            this.groupBox6.TabIndex = 14;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Selecteer een optie";
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(130, 410);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(100, 41);
            this.btnSluiten.TabIndex = 15;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // FrmVerwerkingChocolade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1018, 630);
            this.Controls.Add(this.btnSluiten);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.txtAantalGewenst);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnVerwerk);
            this.Controls.Add(this.lbWeergaveStock);
            this.Name = "FrmVerwerkingChocolade";
            this.Text = "Verwerking";
            this.Load += new System.EventHandler(this.FrmVerwerkingChocolade_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbWeergaveStock;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbGoudenEi;
        private System.Windows.Forms.CheckBox cbPuzzel;
        private System.Windows.Forms.CheckBox cbGereedschapset;
        private System.Windows.Forms.CheckBox cbAsperge;
        private System.Windows.Forms.CheckBox cbVoetbalschoen;
        private System.Windows.Forms.CheckBox cbEendDucky;
        private System.Windows.Forms.Button btnVerwerk;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblTruffels;
        private System.Windows.Forms.Label lblTablets;
        private System.Windows.Forms.Label lblPralines;
        private System.Windows.Forms.Label lblVentjes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAantalGewenst;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cbMixTablet;
        private System.Windows.Forms.CheckBox cbWitHazelnoot;
        private System.Windows.Forms.CheckBox cbWitGepofteRijst;
        private System.Windows.Forms.CheckBox cb55Sinaasappel;
        private System.Windows.Forms.CheckBox cb55Puur;
        private System.Windows.Forms.CheckBox cb70Puur;
        private System.Windows.Forms.CheckBox cb85Puur;
        private System.Windows.Forms.CheckBox cb30MelkGezoutKaramel;
        private System.Windows.Forms.CheckBox cb30MelkHazelnoot;
        private System.Windows.Forms.CheckBox cb30Melk;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox cbRuby;
        private System.Windows.Forms.CheckBox cbHartjeFram;
        private System.Windows.Forms.CheckBox cbManon;
        private System.Windows.Forms.CheckBox cbDuettoYuzu;
        private System.Windows.Forms.CheckBox cbBûche;
        private System.Windows.Forms.CheckBox cbEveVanille;
        private System.Windows.Forms.CheckBox cbCerisette;
        private System.Windows.Forms.CheckBox cbAdvocaat;
        private System.Windows.Forms.CheckBox cbFinesse;
        private System.Windows.Forms.CheckBox cbDéliceKokosnoot;
        private System.Windows.Forms.CheckBox cbAlexanderSinaasappel;
        private System.Windows.Forms.CheckBox cbLikeurPraline;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox cbGroeneThee;
        private System.Windows.Forms.CheckBox cbLimoenKokos;
        private System.Windows.Forms.CheckBox cbFrambozen;
        private System.Windows.Forms.CheckBox cbGember;
        private System.Windows.Forms.CheckBox cbCappuccino;
        private System.Windows.Forms.CheckBox cbClassicDark;
        private System.Windows.Forms.CheckBox cbClassicMelk;
        private System.Windows.Forms.CheckBox cbKaramel;
        private System.Windows.Forms.CheckBox cbPecannoot;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnSluiten;
    }
}