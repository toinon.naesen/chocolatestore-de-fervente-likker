﻿namespace Project_2_De_fervente_likker
{
    public class Gebruiker
    {
        /* Gebruiken om:
         *          in te loggen
         *                  -Naam + Passwoord
         *          Zowel klanten als personeel te identificeren
         *          
         *          
         *          Rekeningen op te zetten
         */


        //Declaraties
        private string _gebruikersnaam;
        private string _passwoord;

        public Gebruiker()
        {
            Gebruikersnaam = "Default";
            Passwoord = "Default";
        }

        //Consructor
        public Gebruiker(string localNaam, string localPasswoord)
        {
            Gebruikersnaam = localNaam;
            Passwoord = localPasswoord;
        }

        //Properties 
        public string Gebruikersnaam
        {
            get { return _gebruikersnaam; }
            set { _gebruikersnaam = value; }
        }

        public string Passwoord
        {
            get { return _passwoord; }
            set { _passwoord = value; }
        }
        public override bool Equals(object obj)
        {
            bool check = true;


            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Gebruiker k = (Gebruiker)obj;
                if (this.Gebruikersnaam != k.Gebruikersnaam || this.Passwoord != k.Passwoord)
                {
                    check = false;
                }

            }

            return check;
        }

        //Methoden
        

    }
}
