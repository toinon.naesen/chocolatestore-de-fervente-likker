﻿using System;

namespace Project_2_De_fervente_likker
{
    public class Tablet : Product
    {
        //Declaraties



        //Constructors
        public Tablet(double prijs, string soortChocolade, double gewicht, string houdbaarheid, string beschrijving, int id) :
            base(prijs, soortChocolade, gewicht, houdbaarheid, beschrijving, id)
        {


        }
        public Tablet()
        {

        }

        //Properties


        //methode
        public override DateTime HoudbaarheidsDatum()
        {
            DateTime dateTimeNow = DateTime.Now;
            DateTime todayPlusOneYear = dateTimeNow.Date.AddDays(365);
            return todayPlusOneYear;
        }

        public override string ToString()
        {
            return HoudbaarheidsDatum().ToString("dd-MM-yyyy");
        }

        public override bool Equals(object obj)
        {
            bool check = true;

            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Tablet t = (Tablet)obj;
                if (this.Beschrijving != t.Beschrijving || this.Prijs != t.Prijs || this.SoortChocolade != t.SoortChocolade || this.Gewicht != t.Gewicht || this.Houdbaarheid != t.Houdbaarheid || this.ID != t.ID)
                {
                    check = false;
                }
            }
            return check;
        }
    }
}
