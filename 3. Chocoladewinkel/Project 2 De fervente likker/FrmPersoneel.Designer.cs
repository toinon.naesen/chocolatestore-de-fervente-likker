﻿
namespace Project_2_De_fervente_likker
{
    partial class FrmPersoneel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowpersoneel = new System.Windows.Forms.Button();
            this.dgvpersonneel = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Geslacht = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Naam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Voornaam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Functie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StraatNaam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Huisnummer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PostCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gemeente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Land = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefoon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLand = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbFunctie = new System.Windows.Forms.ComboBox();
            this.btnWissenpersoneel = new System.Windows.Forms.Button();
            this.btnUpdatepersoneel = new System.Windows.Forms.Button();
            this.btnNieuwpersoneel = new System.Windows.Forms.Button();
            this.btnExporttxt = new System.Windows.Forms.Button();
            this.CbGender = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.mtbGeboortedatum = new System.Windows.Forms.MaskedTextBox();
            this.mtbGsmnummer = new System.Windows.Forms.MaskedTextBox();
            this.txtGemeente = new System.Windows.Forms.TextBox();
            this.txtPostecode = new System.Windows.Forms.TextBox();
            this.txtHuisnummer = new System.Windows.Forms.TextBox();
            this.txtStraatnaam = new System.Windows.Forms.TextBox();
            this.txtVoornaam = new System.Windows.Forms.TextBox();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.TxtMatricule = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvpersonneel)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShowpersoneel
            // 
            this.btnShowpersoneel.Location = new System.Drawing.Point(1659, 122);
            this.btnShowpersoneel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnShowpersoneel.Name = "btnShowpersoneel";
            this.btnShowpersoneel.Size = new System.Drawing.Size(227, 37);
            this.btnShowpersoneel.TabIndex = 58;
            this.btnShowpersoneel.Text = "Toon Persooneelsbestand";
            this.btnShowpersoneel.UseVisualStyleBackColor = true;
            // 
            // dgvpersonneel
            // 
            this.dgvpersonneel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvpersonneel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Geslacht,
            this.Naam,
            this.Voornaam,
            this.dataGridViewTextBoxColumn1,
            this.Functie,
            this.StraatNaam,
            this.Huisnummer,
            this.PostCode,
            this.Gemeente,
            this.Land,
            this.Telefoon});
            this.dgvpersonneel.Location = new System.Drawing.Point(44, 257);
            this.dgvpersonneel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgvpersonneel.Name = "dgvpersonneel";
            this.dgvpersonneel.RowHeadersWidth = 51;
            this.dgvpersonneel.RowTemplate.Height = 29;
            this.dgvpersonneel.Size = new System.Drawing.Size(1934, 470);
            this.dgvpersonneel.TabIndex = 54;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.Width = 125;
            // 
            // Geslacht
            // 
            this.Geslacht.HeaderText = "Geslacht";
            this.Geslacht.MinimumWidth = 6;
            this.Geslacht.Name = "Geslacht";
            this.Geslacht.Width = 125;
            // 
            // Naam
            // 
            this.Naam.HeaderText = "Naam";
            this.Naam.MinimumWidth = 6;
            this.Naam.Name = "Naam";
            this.Naam.Width = 125;
            // 
            // Voornaam
            // 
            this.Voornaam.HeaderText = "Voornaam";
            this.Voornaam.MinimumWidth = 6;
            this.Voornaam.Name = "Voornaam";
            this.Voornaam.Width = 125;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Geboortedatum";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // Functie
            // 
            this.Functie.HeaderText = "Functie";
            this.Functie.MinimumWidth = 6;
            this.Functie.Name = "Functie";
            this.Functie.Width = 125;
            // 
            // StraatNaam
            // 
            this.StraatNaam.HeaderText = "Straatnaam";
            this.StraatNaam.MinimumWidth = 6;
            this.StraatNaam.Name = "StraatNaam";
            this.StraatNaam.Width = 125;
            // 
            // Huisnummer
            // 
            this.Huisnummer.HeaderText = "Huisnummer";
            this.Huisnummer.MinimumWidth = 6;
            this.Huisnummer.Name = "Huisnummer";
            this.Huisnummer.Width = 125;
            // 
            // PostCode
            // 
            this.PostCode.HeaderText = "Postcode";
            this.PostCode.MinimumWidth = 6;
            this.PostCode.Name = "PostCode";
            this.PostCode.Width = 125;
            // 
            // Gemeente
            // 
            this.Gemeente.HeaderText = "Gemeente";
            this.Gemeente.MinimumWidth = 6;
            this.Gemeente.Name = "Gemeente";
            this.Gemeente.Width = 125;
            // 
            // Land
            // 
            this.Land.HeaderText = "Land";
            this.Land.MinimumWidth = 6;
            this.Land.Name = "Land";
            this.Land.Width = 125;
            // 
            // Telefoon
            // 
            this.Telefoon.HeaderText = "Telefoonummer";
            this.Telefoon.MinimumWidth = 6;
            this.Telefoon.Name = "Telefoon";
            this.Telefoon.Width = 125;
            // 
            // txtLand
            // 
            this.txtLand.Location = new System.Drawing.Point(854, 158);
            this.txtLand.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtLand.Name = "txtLand";
            this.txtLand.Size = new System.Drawing.Size(155, 31);
            this.txtLand.TabIndex = 53;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(786, 163);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 25);
            this.label12.TabIndex = 52;
            this.label12.Text = "Land";
            // 
            // cbFunctie
            // 
            this.cbFunctie.FormattingEnabled = true;
            this.cbFunctie.Items.AddRange(new object[] {
            "Directeur",
            "Bediende"});
            this.cbFunctie.Location = new System.Drawing.Point(499, 83);
            this.cbFunctie.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbFunctie.Name = "cbFunctie";
            this.cbFunctie.Size = new System.Drawing.Size(188, 33);
            this.cbFunctie.TabIndex = 51;
            // 
            // btnWissenpersoneel
            // 
            this.btnWissenpersoneel.Location = new System.Drawing.Point(1369, 122);
            this.btnWissenpersoneel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnWissenpersoneel.Name = "btnWissenpersoneel";
            this.btnWissenpersoneel.Size = new System.Drawing.Size(234, 37);
            this.btnWissenpersoneel.TabIndex = 57;
            this.btnWissenpersoneel.Text = "Wissen Personeelslid";
            this.btnWissenpersoneel.UseVisualStyleBackColor = true;
            // 
            // btnUpdatepersoneel
            // 
            this.btnUpdatepersoneel.Location = new System.Drawing.Point(1659, 17);
            this.btnUpdatepersoneel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnUpdatepersoneel.Name = "btnUpdatepersoneel";
            this.btnUpdatepersoneel.Size = new System.Drawing.Size(227, 37);
            this.btnUpdatepersoneel.TabIndex = 56;
            this.btnUpdatepersoneel.Text = "Wijzig Personeelslid";
            this.btnUpdatepersoneel.UseVisualStyleBackColor = true;
            this.btnUpdatepersoneel.Click += new System.EventHandler(this.btnUpdatepersoneel_Click_1);
            // 
            // btnNieuwpersoneel
            // 
            this.btnNieuwpersoneel.Location = new System.Drawing.Point(1369, 17);
            this.btnNieuwpersoneel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNieuwpersoneel.Name = "btnNieuwpersoneel";
            this.btnNieuwpersoneel.Size = new System.Drawing.Size(234, 37);
            this.btnNieuwpersoneel.TabIndex = 55;
            this.btnNieuwpersoneel.Text = "Nieuw Personeelslid";
            this.btnNieuwpersoneel.UseVisualStyleBackColor = true;
            this.btnNieuwpersoneel.Click += new System.EventHandler(this.btnNieuwpersoneel_Click);
            // 
            // btnExporttxt
            // 
            this.btnExporttxt.Location = new System.Drawing.Point(1369, 187);
            this.btnExporttxt.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExporttxt.Name = "btnExporttxt";
            this.btnExporttxt.Size = new System.Drawing.Size(117, 37);
            this.btnExporttxt.TabIndex = 59;
            this.btnExporttxt.Text = "Opslaan";
            this.btnExporttxt.UseVisualStyleBackColor = true;
            // 
            // CbGender
            // 
            this.CbGender.FormattingEnabled = true;
            this.CbGender.Items.AddRange(new object[] {
            "M",
            "V",
            "X"});
            this.CbGender.Location = new System.Drawing.Point(499, 13);
            this.CbGender.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.CbGender.Name = "CbGender";
            this.CbGender.Size = new System.Drawing.Size(188, 33);
            this.CbGender.TabIndex = 50;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(396, 17);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 25);
            this.label11.TabIndex = 49;
            this.label11.Text = "Geslacht";
            // 
            // mtbGeboortedatum
            // 
            this.mtbGeboortedatum.Location = new System.Drawing.Point(177, 80);
            this.mtbGeboortedatum.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.mtbGeboortedatum.Mask = "00/00/0000";
            this.mtbGeboortedatum.Name = "mtbGeboortedatum";
            this.mtbGeboortedatum.Size = new System.Drawing.Size(155, 31);
            this.mtbGeboortedatum.TabIndex = 48;
            this.mtbGeboortedatum.ValidatingType = typeof(System.DateTime);
            // 
            // mtbGsmnummer
            // 
            this.mtbGsmnummer.Location = new System.Drawing.Point(1171, 157);
            this.mtbGsmnummer.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.mtbGsmnummer.Mask = "(999) 000-0000";
            this.mtbGsmnummer.Name = "mtbGsmnummer";
            this.mtbGsmnummer.Size = new System.Drawing.Size(155, 31);
            this.mtbGsmnummer.TabIndex = 47;
            // 
            // txtGemeente
            // 
            this.txtGemeente.Location = new System.Drawing.Point(499, 157);
            this.txtGemeente.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtGemeente.Name = "txtGemeente";
            this.txtGemeente.Size = new System.Drawing.Size(155, 31);
            this.txtGemeente.TabIndex = 46;
            // 
            // txtPostecode
            // 
            this.txtPostecode.Location = new System.Drawing.Point(177, 153);
            this.txtPostecode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtPostecode.Name = "txtPostecode";
            this.txtPostecode.Size = new System.Drawing.Size(155, 31);
            this.txtPostecode.TabIndex = 45;
            // 
            // txtHuisnummer
            // 
            this.txtHuisnummer.Location = new System.Drawing.Point(1171, 83);
            this.txtHuisnummer.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtHuisnummer.Name = "txtHuisnummer";
            this.txtHuisnummer.Size = new System.Drawing.Size(155, 31);
            this.txtHuisnummer.TabIndex = 44;
            // 
            // txtStraatnaam
            // 
            this.txtStraatnaam.Location = new System.Drawing.Point(851, 85);
            this.txtStraatnaam.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtStraatnaam.Name = "txtStraatnaam";
            this.txtStraatnaam.Size = new System.Drawing.Size(155, 31);
            this.txtStraatnaam.TabIndex = 43;
            // 
            // txtVoornaam
            // 
            this.txtVoornaam.Location = new System.Drawing.Point(1171, 7);
            this.txtVoornaam.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtVoornaam.Name = "txtVoornaam";
            this.txtVoornaam.Size = new System.Drawing.Size(155, 31);
            this.txtVoornaam.TabIndex = 42;
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(846, 13);
            this.txtNaam.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(155, 31);
            this.txtNaam.TabIndex = 41;
            // 
            // TxtMatricule
            // 
            this.TxtMatricule.Location = new System.Drawing.Point(177, 12);
            this.TxtMatricule.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TxtMatricule.Name = "TxtMatricule";
            this.TxtMatricule.Size = new System.Drawing.Size(155, 31);
            this.TxtMatricule.TabIndex = 40;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(397, 93);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 25);
            this.label10.TabIndex = 39;
            this.label10.Text = "Functie";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 87);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 25);
            this.label9.TabIndex = 38;
            this.label9.Text = "Geboortedatum";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1043, 163);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 25);
            this.label8.TabIndex = 37;
            this.label8.Text = "Telefoonnummer";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(384, 167);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 25);
            this.label7.TabIndex = 36;
            this.label7.Text = "Gemeente";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(60, 162);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 25);
            this.label6.TabIndex = 35;
            this.label6.Text = "Postcode";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1044, 88);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 25);
            this.label5.TabIndex = 34;
            this.label5.Text = "Huisnummer";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(733, 93);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 25);
            this.label4.TabIndex = 33;
            this.label4.Text = "Straatnaam";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1044, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 25);
            this.label3.TabIndex = 32;
            this.label3.Text = "Voornaam";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(776, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 25);
            this.label2.TabIndex = 31;
            this.label2.Text = "Naam";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 25);
            this.label1.TabIndex = 30;
            this.label1.Text = "ID";
            // 
            // FrmPersoneel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1924, 733);
            this.Controls.Add(this.btnShowpersoneel);
            this.Controls.Add(this.dgvpersonneel);
            this.Controls.Add(this.txtLand);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cbFunctie);
            this.Controls.Add(this.btnWissenpersoneel);
            this.Controls.Add(this.btnUpdatepersoneel);
            this.Controls.Add(this.btnNieuwpersoneel);
            this.Controls.Add(this.btnExporttxt);
            this.Controls.Add(this.CbGender);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.mtbGeboortedatum);
            this.Controls.Add(this.mtbGsmnummer);
            this.Controls.Add(this.txtGemeente);
            this.Controls.Add(this.txtPostecode);
            this.Controls.Add(this.txtHuisnummer);
            this.Controls.Add(this.txtStraatnaam);
            this.Controls.Add(this.txtVoornaam);
            this.Controls.Add(this.txtNaam);
            this.Controls.Add(this.TxtMatricule);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "FrmPersoneel";
            this.Text = "Personeel";
            ((System.ComponentModel.ISupportInitialize)(this.dgvpersonneel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShowpersoneel;
        private System.Windows.Forms.DataGridView dgvpersonneel;
        private System.Windows.Forms.TextBox txtLand;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbFunctie;
        private System.Windows.Forms.Button btnWissenpersoneel;
        private System.Windows.Forms.Button btnUpdatepersoneel;
        private System.Windows.Forms.Button btnNieuwpersoneel;
        private System.Windows.Forms.Button btnExporttxt;
        private System.Windows.Forms.ComboBox CbGender;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox mtbGeboortedatum;
        private System.Windows.Forms.MaskedTextBox mtbGsmnummer;
        private System.Windows.Forms.TextBox txtGemeente;
        private System.Windows.Forms.TextBox txtPostecode;
        private System.Windows.Forms.TextBox txtHuisnummer;
        private System.Windows.Forms.TextBox txtStraatnaam;
        private System.Windows.Forms.TextBox txtVoornaam;
        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.TextBox TxtMatricule;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Geslacht;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naam;
        private System.Windows.Forms.DataGridViewTextBoxColumn Voornaam;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Functie;
        private System.Windows.Forms.DataGridViewTextBoxColumn StraatNaam;
        private System.Windows.Forms.DataGridViewTextBoxColumn Huisnummer;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gemeente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Land;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefoon;
    }
}