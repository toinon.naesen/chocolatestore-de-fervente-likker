﻿namespace Project_2_De_fervente_likker
{
    public class Klant : Gebruiker
    {


        private string _voornaam;
        private string _achternaam;
        private string _straatnaam;
        private string _huisnummer;
        private string _postcode;
        private string _gemeente;
        private string _provincie;
        private double _gsmnummer;
        private double _huistelefoon;
        private string _email;
        private int _klantid;

        public Klant() : base()
        {
        }
        public Klant(string loginname, string wachtwoord) : base(loginname, wachtwoord)
        {

        }

        public Klant(string voornaam, string achternaam, string straatnaam, string huisnummer, string postcode, string gemeente, string provincie, double gsmnummer, double huistelefoon, string email, int klantid, string loginname, string wachtwoord) : base(loginname, wachtwoord)
        {
            Voornaam = voornaam;
            Achternaam = achternaam;
            Straatnaam = straatnaam;
            Huisnummer = huisnummer;
            Postcode = postcode;
            Gemeente = gemeente;
            Provincie = provincie;
            Gsmnummer = gsmnummer;
            Huistelefoon = huistelefoon;
            Email = email;
            Klantid = klantid;
        }

        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }


        public string Achternaam
        {
            get { return _achternaam; }
            set { _achternaam = value; }
        }



        public string Straatnaam
        {
            get { return _straatnaam; }
            set { _straatnaam = value; }
        }



        public string Huisnummer
        {
            get { return _huisnummer; }
            set { _huisnummer = value; }
        }


        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }


        public string Gemeente
        {
            get { return _gemeente; }
            set { _gemeente = value; }
        }


        public string Provincie
        {
            get { return _provincie; }
            set { _provincie = value; }
        }


        public double Gsmnummer
        {
            get { return _gsmnummer; }
            set { _gsmnummer = value; }
        }


        public double Huistelefoon
        {
            get { return _huistelefoon; }
            set { _huistelefoon = value; }
        }


        public string Email
        {
            get { return _email; }
            set
            {

                _email = value;

            }
        }
        public int Klantid
        {
            get { return _klantid; }
            set { _klantid = value; }
        }
        public override string ToString()
        {
            string opmaakuitvoer;
            opmaakuitvoer = $"ID:{Klantid}   Naam: {Voornaam} {Achternaam}  E-mail: {Email}";
            return opmaakuitvoer;
        }
        public string DetailsKlanten()
        {
            return $"ID: {Klantid}\nNaam: {Voornaam} {Achternaam}\nAdres: {Straatnaam} {Huisnummer}\n           {Postcode} {Gemeente}\n           {Provincie}\nGsm: +{Gsmnummer}\nVaste telefoon: 0{Huistelefoon}\nE-mail: {Email}";
        }
        public override bool Equals(object obj)
        {
            bool check = true;


            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Klant k = (Klant)obj;
                if (this.Gebruikersnaam != k.Gebruikersnaam || this.Passwoord != k.Passwoord)
                {
                    check = false;
                }

            }

            return check;
        }
        public bool Equals(object obj, int naamOfID)
        {
            bool check = true;


            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                switch (naamOfID)
                {
                    case 1:
                        Klant k = (Klant)obj;
                        if (this.Voornaam != k.Voornaam || this.Achternaam != k.Achternaam)
                        {
                            check = false;
                        }
                        break;
                    case 2:
                        Klant i = (Klant)obj;
                        if (this.Klantid != i.Klantid)
                        {
                            check = false;
                        }
                        break;
                    default:
                        break;
                }
            }

            return check;


        }
    }
}
