﻿
namespace Project_2_De_fervente_likker
{
    partial class FrmBestellen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnBestellingAanmaken = new System.Windows.Forms.Button();
            this.btnOverzicht = new System.Windows.Forms.Button();
            this.pnlMainWindow = new System.Windows.Forms.Panel();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlButtons.Controls.Add(this.btnBestellingAanmaken);
            this.pnlButtons.Controls.Add(this.btnOverzicht);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(182, 530);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnBestellingAanmaken
            // 
            this.btnBestellingAanmaken.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBestellingAanmaken.Location = new System.Drawing.Point(0, 52);
            this.btnBestellingAanmaken.Name = "btnBestellingAanmaken";
            this.btnBestellingAanmaken.Size = new System.Drawing.Size(182, 59);
            this.btnBestellingAanmaken.TabIndex = 1;
            this.btnBestellingAanmaken.Text = "Bestelling aanmaken";
            this.btnBestellingAanmaken.UseVisualStyleBackColor = true;
            this.btnBestellingAanmaken.Click += new System.EventHandler(this.btnBestellingAanmaken_Click);
            // 
            // btnOverzicht
            // 
            this.btnOverzicht.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOverzicht.Location = new System.Drawing.Point(0, 0);
            this.btnOverzicht.Name = "btnOverzicht";
            this.btnOverzicht.Size = new System.Drawing.Size(182, 52);
            this.btnOverzicht.TabIndex = 0;
            this.btnOverzicht.Text = "Overzicht";
            this.btnOverzicht.UseVisualStyleBackColor = true;
            this.btnOverzicht.Click += new System.EventHandler(this.btnOverzicht_Click);
            // 
            // pnlMainWindow
            // 
            this.pnlMainWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMainWindow.Location = new System.Drawing.Point(182, 0);
            this.pnlMainWindow.Name = "pnlMainWindow";
            this.pnlMainWindow.Size = new System.Drawing.Size(810, 530);
            this.pnlMainWindow.TabIndex = 1;
            // 
            // FrmBestellen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(992, 530);
            this.Controls.Add(this.pnlMainWindow);
            this.Controls.Add(this.pnlButtons);
            this.Name = "FrmBestellen";
            this.Text = "Bestellen van chocolade";
            this.Load += new System.EventHandler(this.FrmBestellen_Load);
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnBestellingAanmaken;
        private System.Windows.Forms.Button btnOverzicht;
        private System.Windows.Forms.Panel pnlMainWindow;
    }
}