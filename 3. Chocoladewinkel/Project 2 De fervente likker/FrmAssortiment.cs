﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace Project_2_De_fervente_likker
{
    public partial class FrmAssortiment : Form
    {
        List<ChocoladeVentje> mijnDoorgekregenChocoladeVentjesLijst;
        List<Praline> mijnDoorgekregenPralineLijst;
        List<Tablet> mijnDoorgekregenTabletLijst;
        List<Truffel> mijnDoorgekregenTruffelLijst;

        public FrmAssortiment()
        {
            InitializeComponent();
        }
        public FrmAssortiment(List<ChocoladeVentje> mijnChocoladeVentjesLijst, List<Praline> mijnPralineLijst, List<Tablet> mijnTabletLijst, List<Truffel> mijnTruffelLijst)
        {
            InitializeComponent();
            mijnDoorgekregenChocoladeVentjesLijst = mijnChocoladeVentjesLijst;
            mijnDoorgekregenPralineLijst = mijnPralineLijst;
            mijnDoorgekregenTabletLijst = mijnTabletLijst;
            mijnDoorgekregenTruffelLijst = mijnTruffelLijst;
        }
        //XML files
        private const string AssortimentFile = @"..\..\..\XML Files\Assortiment.xml";
        private const string NieuwArtikelFile = @"..\..\..\XML Files\AssortimentNieuwArtikel.xml";
        private const string DeleteArtikelFile = @"..\..\..\XML Files\AssortimentDeleteArtikel.xml";
        private const string AssortimentUitStockFile = @"..\..\..\XML Files\AssortimentStock.xml";

        //datagridview
        DataSet myDataSet = new DataSet();
        DataTable myTable = new DataTable("Artikel");

        //om delen uit klasse te halen
        ChocoladeVentje myVentje = new ChocoladeVentje();
        Praline myPraline = new Praline();
        Tablet myTablet = new Tablet();
        Truffel myTruffel = new Truffel();

        //attributen declareren die nodig zijn in de hele form
        int indexRow;
        string categorie;
        double prijs = 0;
        string soortChocolade = "";
        double gewicht = 0;
        string artikelNaam = "";
        int aantal = 0;
        string houdbaarheidsDatum = "";
        int id = 0;

        private void FrmAssortiment_Load(object sender, EventArgs e)
        {
            //comboboxen invullen als deze form laadt
            cmbSoortChocolade.Items.Add("Puur");
            cmbSoortChocolade.Items.Add("Melk");
            cmbSoortChocolade.Items.Add("Wit");
            cmbSoortChocolade.Items.Add("Mix");
            cmbSoortChocolade.Items.Add("Goud");
            cmbSoortChocolade.Items.Add("Ruby");
            cmbCategorie.Items.Add("Figuur");
            cmbCategorie.Items.Add("Praline");
            cmbCategorie.Items.Add("Tablet");
            cmbCategorie.Items.Add("Truffel");

            //combobox zoeken invullen als form laadt (soort chocolade)
            cmbZoekOpSoortChocolade.Items.Add("Pure chocolade");
            cmbZoekOpSoortChocolade.Items.Add("Melkchocolade");
            cmbZoekOpSoortChocolade.Items.Add("Witte chocolade");
            cmbZoekOpSoortChocolade.Items.Add("Mix");

            //naam kolommen toevoegen als deze form laadt
            myTable.TableName = "Artikel";
            myTable.Columns.Add("ID", typeof(int));
            myTable.Columns.Add("Categorie", typeof(string));
            myTable.Columns.Add("Artikelnaam", typeof(string));
            myTable.Columns.Add("Chocoladesoort", typeof(string));
            myTable.Columns.Add("Gewicht per stuk (g)", typeof(double));
            myTable.Columns.Add("Houdbaarheid", typeof(DateTime));
            myTable.Columns.Add("Prijs per stuk (€)", typeof(double));
            myTable.Columns.Add("Aantal in stock", typeof(int));
            dgvStock.DataSource = myTable;

            //lees XML in Assortiment.xml en voeg alles van de lijst van verwerking toe aan Assortiment.XML
            //VoegStockToe();
            LeesAssortimentXML();

        }

        //item toevoegen aan de stock (nieuw item)
        private void btnStockItemToevoegen_Click(object sender, EventArgs e)
        {
            SchrijfXML();
            XMLNieuw();
            ClearAllTextboxes();

        }

        //item updaten in de stock indien er iets aangepast moet worden
        private void btnStockItemUpdaten_Click(object sender, EventArgs e)
        {
            //try
            //{
            categorie = cmbCategorie.Text;

            foreach (DataGridViewRow row in dgvStock.SelectedRows)
            {
                dgvStock.Rows.RemoveAt(row.Index);
            }
            //{
            //    switch (categorie)
            //    {
            //        case "Figuur":
            //            mijnDoorgekregenChocoladeVentjesLijst.RemoveAt(row.Index);
            //            break;
            //        case "Praline":
            //            mijnDoorgekregenPralineLijst.RemoveAt(row.Index);
            //            break;
            //        case "Tablet":
            //            mijnDoorgekregenTabletLijst.RemoveAt(row.Index);
            //            break;
            //        case "Truffel":
            //            mijnDoorgekregenTruffelLijst.RemoveAt(row.Index);
            //            break;
            //        default:
            //            break;
            //    }

            MessageBox.Show("U staat op het punt een item uit de lijst te wijzigen. Bent u zeker?", "Wijzigen", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            SchrijfXML();
            XMLNieuw();
            ClearAllTextboxes();
            //}
        }

        //item uit stock verwijderen indien dit artikel niet meer bestaat
        private void btnStockItemVerwijderen_Click(object sender, EventArgs e)
        {
            categorie = cmbCategorie.Text;
            //foreach (DataGridViewRow row in dgvStock.SelectedRows)
            //{
            //    switch (categorie)
            //    {
            //        case "Figuur":
            //            mijnDoorgekregenChocoladeVentjesLijst.RemoveAt(row.Index);
            //            break;
            //        case "Praline":
            //            mijnDoorgekregenPralineLijst.RemoveAt(row.Index);
            //            break;
            //        case "Tablet":
            //            mijnDoorgekregenTabletLijst.RemoveAt(row.Index);
            //            break;
            //        case "Truffel":
            //            mijnDoorgekregenTruffelLijst.RemoveAt(row.Index);
            //            break;
            //        default:
            //            break;
            //    }
            foreach (DataGridViewRow row in dgvStock.SelectedRows)
            {
                dgvStock.Rows.RemoveAt(row.Index);
            }
            DeleteXMLArtikel();
            ClearAllTextboxes();
        }
        //Aantallen toevoegen aan stock. Indien er meer worden toegevoegd dit optellen met elkaar. 
        //Bij kassa zal er moeten worden afgetrokken en hier worden getoond dat er  
        private void btnToevoegenAanStock_Click_2(object sender, EventArgs e)
        {
            int aantalNieuw;
            if (txtAantalInStock.Text != string.Empty)
            {
                if (int.TryParse(txtAantalInStock.Text, out _))
                {
                    aantalNieuw = Convert.ToInt32(txtAantalInStock.Text);
                    aantal += aantalNieuw;
                }
                else
                {
                    MessageBox.Show("Vul een getal in.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("Het tekstvak van aantal mag niet leeg zijn.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            foreach (DataGridViewRow row in dgvStock.SelectedRows)
            {
                dgvStock.Rows.RemoveAt(row.Index);
            }
            SchrijfXML();
            XMLNieuw();
            ClearAllTextboxes();
            txtAantalInStock.Text = "";
        }

        //artikel zoeken in de lijst afhankelijk van de naam of de soort chocolade.
        private void btnZoekArtikel_Click(object sender, EventArgs e)
        {
            if (rdbtnArtikelNaam.Checked == true)
            {
                if (string.IsNullOrEmpty(txtZoekArtikelOpNaam.Text))
                {
                    (dgvStock.DataSource as DataTable).DefaultView.RowFilter = string.Empty;
                }
                else
                {
                    (dgvStock.DataSource as DataTable).DefaultView.RowFilter = string.Format("Artikelnaam='{0}'", txtZoekArtikelOpNaam.Text);
                }
                rdbtnArtikelNaam.Checked = false;
            }
            else if (rdbtnSoortChocolade.Checked == true)
            {

                if (cmbZoekOpSoortChocolade.Text == string.Empty)
                {
                    (dgvStock.DataSource as DataTable).DefaultView.RowFilter = string.Empty;
                }
                else
                {
                    (dgvStock.DataSource as DataTable).DefaultView.RowFilter = string.Format("Chocoladesoort ='{0}'", cmbZoekOpSoortChocolade.Text);
                }
                rdbtnSoortChocolade.Checked = false;
            }
        }

        private void btnToonStock_Click(object sender, EventArgs e)
        {
            myDataSet.ReadXml(AssortimentFile);
            dgvStock.DataSource = myDataSet.Tables[0];
        }

        //deze form sluiten
        private void btnStockSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Clear textboxes
        private void ClearAllTextboxes()
        {
            txtArtikelNaam.Text = "";
            txtGewichtInKg.Text = "";
            cmbCategorie.Text = null;
            txtPrijsPerStuk.Text = "";
            cmbSoortChocolade.Text = null;
            txtID.Text = "";
            txtAantalInStock.Text = "";
        }

        //terugplaatsen van gegevens in textboxen en comboboxen
        private void dgvStock_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                indexRow = e.RowIndex;
                DataGridViewRow selectedRow = dgvStock.Rows[indexRow];
                txtID.Text = selectedRow.Cells[0].Value.ToString();
                cmbCategorie.Text = selectedRow.Cells[1].Value.ToString();
                txtArtikelNaam.Text = selectedRow.Cells[2].Value.ToString();
                cmbSoortChocolade.Text = selectedRow.Cells[3].Value.ToString();
                txtGewichtInKg.Text = selectedRow.Cells[4].Value.ToString();
                txtPrijsPerStuk.Text = selectedRow.Cells[5].Value.ToString();
                txtAantalInStock.Text = selectedRow.Cells[7].Value.ToString();
            }
            else
            {
                MessageBox.Show("Duid een rij aan.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //Radiobuttons enable txt of combobox
        private void rdbtnArtikelNaam_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnArtikelNaam.Checked == true)
            {
                txtZoekArtikelOpNaam.Enabled = true;
                cmbZoekOpSoortChocolade.Enabled = false;
            }
        }

        private void rdbtnSoortChocolade_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnSoortChocolade.Checked == true)
            {
                txtZoekArtikelOpNaam.Enabled = false;
                cmbZoekOpSoortChocolade.Enabled = true;
            }
        }

        private void XMLOud()
        {
            try
            {
                myDataSet.ReadXml(NieuwArtikelFile);
            }
            catch (XmlException)
            {
                MessageBox.Show("Er ging iets fout bij het lezen van de XML assortiment nieuw artikel.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception)
            {
                MessageBox.Show("Er ging iets fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void XMLNieuw()
        {
            try
            {
                XMLOud();
                myDataSet.WriteXml(AssortimentFile);
                dgvStock.DataSource = myDataSet.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("Er ging iets fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void LeesAssortimentXML()
        {
            if (AssortimentFile.Length > 0)
            {
                myDataSet.ReadXml(AssortimentFile);
                dgvStock.DataSource = myDataSet.Tables[0];
            }
            else
            {
                MessageBox.Show("U heeft nog geen artikels toegevoegd aan uw stock.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DeleteXMLArtikel()
        {
            try
            {
                myTable.WriteXml(DeleteArtikelFile);
                myDataSet.WriteXml(AssortimentFile);
            }
            catch (XmlException)
            {
                MessageBox.Show("Er ging iets fout bij het schrijven van de XML assortiment delete artikel.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception)
            {
                MessageBox.Show("Er ging iets fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void VoegStockToe()
        {
            AssortimentUitStock();
            myDataSet.ReadXml(AssortimentUitStockFile);
            myDataSet.WriteXml(NieuwArtikelFile);
            XMLNieuw();
        }

        private void SchrijfXML()
        {
            try
            {
                if (string.IsNullOrEmpty(txtArtikelNaam.Text))
                {
                    MessageBox.Show("Er mogen geen lege vakken zijn. Vul het vak van artikelnaam in.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (string.IsNullOrEmpty(txtGewichtInKg.Text))
                {
                    MessageBox.Show("Er mogen geen lege vakken zijn. Vul het vak van gewicht in.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (string.IsNullOrEmpty(txtPrijsPerStuk.Text))
                {
                    MessageBox.Show("Er mogen geen lege vakken zijn. Vul het vak van de prijs in.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (File.Exists(NieuwArtikelFile) == false)
                {
                    MessageBox.Show("Er bestaat geen bestand met de naam assortiment.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (File.Exists(NieuwArtikelFile) == true)
                {
                    id = Convert.ToInt32(txtID.Text);
                    artikelNaam = txtArtikelNaam.Text;
                    soortChocolade = cmbSoortChocolade.Text;
                    gewicht = Convert.ToDouble(txtGewichtInKg.Text);
                    prijs = Convert.ToDouble(txtPrijsPerStuk.Text);
                    categorie = cmbCategorie.Text;

                    using (XmlWriter writer = XmlWriter.Create(NieuwArtikelFile))
                    {
                        writer.WriteStartElement("Assortiment");
                        writer.WriteStartElement("Artikel");
                        writer.WriteElementString("ID", id.ToString());
                        writer.WriteElementString("Categorie", categorie);
                        writer.WriteElementString("Artikelnaam", artikelNaam);
                        writer.WriteElementString("Chocoladesoort", soortChocolade);
                        writer.WriteElementString("Gewicht", gewicht.ToString());
                        writer.WriteElementString("Prijs", prijs.ToString());
                        switch (categorie)
                        {
                            case "Figuur":
                                houdbaarheidsDatum = myVentje.ToString();
                                writer.WriteElementString("Houdbaarheid", houdbaarheidsDatum);
                                break;
                            case "Praline":
                                houdbaarheidsDatum = myPraline.ToString();
                                writer.WriteElementString("Houdbaarheid", houdbaarheidsDatum);
                                break;
                            case "Tablet":
                                houdbaarheidsDatum = myTablet.ToString();
                                writer.WriteElementString("Houdbaarheid", houdbaarheidsDatum);
                                break;
                            case "Truffel":
                                houdbaarheidsDatum = myTruffel.ToString();
                                writer.WriteElementString("Houdbaarheid", houdbaarheidsDatum);
                                break;
                            default:
                                break;
                        }
                        writer.WriteElementString("Aantal", "");
                        writer.WriteEndElement();
                    }
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Duid een rij aan.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Duid een rij aan.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (OverflowException)
            {
                MessageBox.Show("U heeft een te grote waarde ingegeven.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (FormatException)
            {
                MessageBox.Show("Geef de juiste waarde in", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (XmlException)
            {
                MessageBox.Show("Er ging iets fout bij het schrijven van de XML assortiment.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception)
            {
                MessageBox.Show("Duid een rij aan.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
        //methode om gegevens uit de lijst van verwerking te halen
        private void AssortimentUitStock()
        {
            try
            {
                using (XmlWriter writer = XmlWriter.Create(AssortimentUitStockFile))
                {
                    writer.WriteStartElement("Assortiment");
                    if (mijnDoorgekregenChocoladeVentjesLijst.Equals("Figuur"))
                    {
                        foreach (var item in mijnDoorgekregenChocoladeVentjesLijst)
                        {
                            writer.WriteStartElement("Artikel");
                            writer.WriteElementString("ID", myVentje.ID.ToString());
                            writer.WriteElementString("Categorie", "Figuur");
                            writer.WriteElementString("Artikelnaam", myVentje.Beschrijving);
                            writer.WriteElementString("Chocoladesoort", myVentje.SoortChocolade);
                            writer.WriteElementString("Gewicht", myVentje.Gewicht.ToString());
                            writer.WriteElementString("Prijs", myVentje.Prijs.ToString());
                            writer.WriteElementString("Houdbaarheid", myVentje.Houdbaarheid.ToString());
                            if (mijnDoorgekregenChocoladeVentjesLijst.Equals("Eend Ducky") && mijnDoorgekregenChocoladeVentjesLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenChocoladeVentjesLijst.Equals("Voetbalschoen") && mijnDoorgekregenChocoladeVentjesLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenChocoladeVentjesLijst.Equals("Asperge") && mijnDoorgekregenChocoladeVentjesLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenChocoladeVentjesLijst.Equals("Gereedschapset") && mijnDoorgekregenChocoladeVentjesLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenChocoladeVentjesLijst.Equals("Puzzel") && mijnDoorgekregenChocoladeVentjesLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenChocoladeVentjesLijst.Equals("Gouden ei") && mijnDoorgekregenChocoladeVentjesLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            writer.WriteElementString("Aantal", aantal.ToString());
                        }
                    }

                    else if (mijnDoorgekregenPralineLijst.Equals("Praline"))
                    {
                        foreach (var item in mijnDoorgekregenPralineLijst)
                        {
                            writer.WriteStartElement("Artikel");
                            writer.WriteElementString("ID", myPraline.ID.ToString());
                            writer.WriteElementString("Categorie", "Praline");
                            writer.WriteElementString("Artikelnaam", myPraline.Beschrijving);
                            writer.WriteElementString("Chocoladesoort", myVentje.SoortChocolade);
                            writer.WriteElementString("Gewicht", myVentje.Gewicht.ToString());
                            writer.WriteElementString("Prijs", myVentje.Prijs.ToString());
                            writer.WriteElementString("Houdbaarheid", myVentje.Houdbaarheid.ToString());

                            if (mijnDoorgekregenPralineLijst.Equals("Likeur praline") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Alexander sinaasappel") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Délice kokosnoot") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Finesse") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Advocaat (55%)") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Cerisette likeurcrème") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Eve vanille") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Bûche") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Duetto yuzu drakenfruit") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Manon café") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Hartje frambozenganache") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenPralineLijst.Equals("Ruby") && mijnDoorgekregenPralineLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            writer.WriteElementString("Aantal", aantal.ToString());
                        }
                    }
                    else if (mijnDoorgekregenTabletLijst.Equals("Tablet"))
                    {
                        foreach (var item in mijnDoorgekregenTabletLijst)
                        {
                            writer.WriteStartElement("Artikel");
                            writer.WriteElementString("ID", myTablet.ID.ToString());
                            writer.WriteElementString("Categorie", "Tablet");
                            writer.WriteElementString("Artikelnaam", myTablet.Beschrijving);
                            writer.WriteElementString("Chocoladesoort", myTablet.SoortChocolade);
                            writer.WriteElementString("Gewicht", myTablet.Gewicht.ToString());
                            writer.WriteElementString("Prijs", myTablet.Prijs.ToString());
                            writer.WriteElementString("Houdbaarheid", myTablet.Houdbaarheid.ToString());

                            if (mijnDoorgekregenTabletLijst.Equals("30% Melk") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("30% Melk - Hazelnoot") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("30% Melk - Gezouten Karamel") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("85% Puur") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("70% Puur") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("55% Puur") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("55% Puur - Sinaasappel") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("Wit - Gepofte rijst") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTabletLijst.Equals("Wit - Hazelnoot") && mijnDoorgekregenTabletLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            writer.WriteElementString("Aantal", aantal.ToString());
                        }
                    }
                    else if (mijnDoorgekregenTruffelLijst.Equals("Truffel"))
                    {
                        foreach (var item in mijnDoorgekregenTruffelLijst)
                        {
                            writer.WriteStartElement("Artikel");
                            writer.WriteElementString("ID", myTruffel.ID.ToString());
                            writer.WriteElementString("Categorie", "Truffel");
                            writer.WriteElementString("Artikelnaam", myTruffel.Beschrijving);
                            writer.WriteElementString("Chocoladesoort", myTruffel.SoortChocolade);
                            writer.WriteElementString("Gewicht", myTruffel.Gewicht.ToString());
                            writer.WriteElementString("Prijs", myTruffel.Prijs.ToString());
                            writer.WriteElementString("Houdbaarheid", myTruffel.Houdbaarheid.ToString());

                            if (mijnDoorgekregenTruffelLijst.Equals("Pecannoot") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Karamel") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Classic Melk") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Classic Dark") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Cappuccino") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Gember") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Frambozen") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Limoen en Kokos") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            else if (mijnDoorgekregenTruffelLijst.Equals("Groene Thee") && mijnDoorgekregenTruffelLijst.Equals(houdbaarheidsDatum))
                            {
                                aantal++;
                            }
                            writer.WriteElementString("Aantal", aantal.ToString());
                        }
                    }
                }
            }
            catch (XmlException)
            {
                MessageBox.Show("Er ging iets fout bij het schrijven van de XML assortiment uit stock.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}



