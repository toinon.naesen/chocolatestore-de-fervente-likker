﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Project_2_De_fervente_likker
{
    public partial class FrmVerwerkingChocolade : Form
    {
        List<Chocolade> mijnDoorgekregenChocoladeLijst;
        List<ChocoladeVentje> mijnDoorgekregenChocoladeVentjesLijst;
        List<Praline> mijnDoorgekregenPralineLijst;
        List<Tablet> mijnDoorgekregenTabletLijst;
        List<Truffel> mijnDoorgekregenTruffelLijst;
        string beschrijving;
        double prijs;
        double gewicht;
        public FrmVerwerkingChocolade()
        {
            InitializeComponent();
        }
        public FrmVerwerkingChocolade(List<Chocolade> mijnChocoladeLijst, List<ChocoladeVentje> mijnChocoladeVentjesLijst, List<Praline> mijnPralineLijst, List<Tablet> mijnTabletLijst, List<Truffel> mijnTruffelLijst)
        {
            InitializeComponent();
            mijnDoorgekregenChocoladeLijst = mijnChocoladeLijst;
            mijnDoorgekregenChocoladeVentjesLijst = mijnChocoladeVentjesLijst;
            mijnDoorgekregenPralineLijst = mijnPralineLijst;
            mijnDoorgekregenTabletLijst = mijnTabletLijst;
            mijnDoorgekregenTruffelLijst = mijnTruffelLijst;
        }
        private void FrmVerwerkingChocolade_Load(object sender, EventArgs e)
        {

            UpdateStockAantal();
        }
        public void VerwerkingVanChocolade()
        {
            ChocoladeVentje nieuwFiguur, DateTimeCallF = new ChocoladeVentje();
            Praline nieuwPraline, DateTimeCallP = new Praline();
            Tablet nieuwTablet, DateTimeCallTA = new Tablet();
            Truffel nieuwTruffel, DateTimeCallTR = new Truffel();
            
            int aantalMaken, checkBoxIdentifier = CheckBoxCheck();
            
            
            if (!string.IsNullOrEmpty(txtAantalGewenst.Text) && int.TryParse(txtAantalGewenst.Text, out aantalMaken))
            {
                if (lbWeergaveStock.SelectedIndex != -1) 
                {
                    if ( checkBoxIdentifier < 200) // figuur
                    {
                        for (int i = 0; i < aantalMaken; i++)
                        {
                            nieuwFiguur = new ChocoladeVentje(prijs, mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Soort, gewicht, DateTimeCallF.ToString(), beschrijving, checkBoxIdentifier);
                            mijnDoorgekregenChocoladeVentjesLijst.Add(nieuwFiguur);
                            mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Hoeveelheid -= (gewicht/ 1000);
                        }

                    }
                    else if (checkBoxIdentifier > 200 && checkBoxIdentifier < 300) // tablet
                    {
                        for (int i = 0; i < aantalMaken; i++)
                        {
                            nieuwTablet = new Tablet(prijs, mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Soort, gewicht, DateTimeCallTA.ToString(), beschrijving, checkBoxIdentifier);
                            mijnDoorgekregenTabletLijst.Add(nieuwTablet);
                            mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Hoeveelheid -= (gewicht / 1000);
                        }

                    }
                    else if (checkBoxIdentifier > 300 && checkBoxIdentifier < 400) // Praline
                    {
                        for (int i = 0; i < aantalMaken; i++)
                        {
                            nieuwPraline = new Praline(prijs, mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Soort, gewicht, DateTimeCallP.ToString(), beschrijving, checkBoxIdentifier);
                            mijnDoorgekregenPralineLijst.Add(nieuwPraline);
                            mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Hoeveelheid -= (gewicht / 1000);
                        }
                    }
                    else // Truffel
                    {
                        for (int i = 0; i < aantalMaken; i++)
                        {
                            nieuwTruffel = new Truffel(prijs, mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Soort, gewicht, DateTimeCallTR.ToString(), beschrijving, checkBoxIdentifier);
                            mijnDoorgekregenTruffelLijst.Add(nieuwTruffel);
                            mijnDoorgekregenChocoladeLijst[lbWeergaveStock.SelectedIndex].Hoeveelheid -= (gewicht / 1000);
                        }
                    }

                }
                else
                {
                    MessageBox.Show("Gelieve de soort chocolade te selecteren.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("De ingave mag niet leeg zijn en mag ook alleen numerieken bevatten.", "Error", MessageBoxButtons.OK);
            }

        }
        public int CheckBoxCheck()
        {
            int identifier = 0;
            // start figuur
            if (cbEendDucky.Checked)
            {
                identifier = 101;
                prijs = 5.95;
                gewicht = 150;
                beschrijving = cbEendDucky.Text;
            }
            else if (cbVoetbalschoen.Checked)
            {
                identifier = 102;
                prijs = 4.50;
                gewicht = 125;
                beschrijving = cbVoetbalschoen.Text;
            }
            else if (cbAsperge.Checked)
            {
                identifier = 103;
                prijs = 6.85;
                gewicht = 250;
                beschrijving = cbAsperge.Text;
            }
            else if (cbGereedschapset.Checked)
            {
                identifier = 104;
                prijs = 9.95;
                gewicht = 225;
                beschrijving = cbGereedschapset.Text;
            }
            else if (cbPuzzel.Checked)
            {
                identifier = 105;
                prijs = 8.50;
                gewicht = 250;
                beschrijving = cbPuzzel.Text;
            }
            else if (cbGoudenEi.Checked)
            {
                identifier = 106;
                prijs = 12.90;
                gewicht = 160;
                beschrijving = cbGoudenEi.Text;
            }
            //end figuur - start tablet
            else if (cb30Melk.Checked)
            {
                identifier = 201;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cb30Melk.Text;
            }
            else if (cb30MelkHazelnoot.Checked)
            {
                identifier = 202;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cb30MelkHazelnoot.Text;
            }
            else if (cb30MelkGezoutKaramel.Checked)
            {
                identifier = 203;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cb30MelkGezoutKaramel.Text;
            }
            else if (cb85Puur.Checked)
            {
                identifier = 204;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cb85Puur.Text;
            }
            else if (cb70Puur.Checked)
            {
                identifier = 205;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cb70Puur.Text;
            }
            else if (cb55Puur.Checked)
            {
                identifier = 206;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cb55Puur.Text;
            }
            else if (cb55Sinaasappel.Checked)
            {
                identifier = 207;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cb55Sinaasappel.Text;
            }
            else if (cbWitGepofteRijst.Checked)
            {
                identifier = 208;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cbWitGepofteRijst.Text;
            }
            else if (cbWitHazelnoot.Checked)
            {
                identifier = 209;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cbWitHazelnoot.Text;
            }
            else if (cbMixTablet.Checked)
            {
                identifier = 210;
                prijs = 2.70;
                gewicht = 100;
                beschrijving = cbMixTablet.Text;
            }
            // end tablet - start praline
            else if (cbLikeurPraline.Checked)
            {
                identifier = 301;
                prijs = 0.90;
                gewicht = 13.8;
                beschrijving = cbLikeurPraline.Text;
            }
            else if (cbAlexanderSinaasappel.Checked)
            {
                identifier = 302;
                prijs = 13.5;
                gewicht = 1.10;
                beschrijving = cbAlexanderSinaasappel.Text;
            }
            else if (cbDéliceKokosnoot.Checked)
            {
                identifier = 303;
                prijs = 13.5;
                gewicht = 1.10;
                beschrijving = cbDéliceKokosnoot.Text;
            }
            else if (cbFinesse.Checked)
            {
                identifier = 304;
                prijs = 4.5;
                gewicht = 1.10;
                beschrijving = cbFinesse.Text;
            }
            else if (cbAdvocaat.Checked)
            {
                identifier = 305;
                prijs = 15;
                gewicht = 1.10;
                beschrijving = cbAdvocaat.Text;
            }
            else if (cbCerisette.Checked)
            {
                identifier = 306;
                prijs = 9.5;
                gewicht = 1.10;
                beschrijving = cbCerisette.Text;
            }
            else if (cbEveVanille.Checked)
            {
                identifier = 307;
                prijs = 14;
                gewicht = 1.10;
                beschrijving = cbEveVanille.Text;
            }
            else if (cbBûche.Checked)
            {
                identifier = 308;
                prijs = 7.5;
                gewicht = 1.10;
                beschrijving = cbBûche.Text;
            }
            else if (cbDuettoYuzu.Checked)
            {
                identifier = 309;
                prijs = 12;
                gewicht = 1.10;
                beschrijving = cbDuettoYuzu.Text;
            }
            else if (cbManon.Checked)
            {
                identifier = 310;
                prijs = 13.5;
                gewicht = 1.10;
                beschrijving = cbManon.Text;
            }
            else if (cbHartjeFram.Checked)
            {
                identifier = 311;
                prijs = 10;
                gewicht = 1.10;
                beschrijving = cbHartjeFram.Text;
            }
            else if (cbRuby.Checked)
            {
                identifier = 312;
                prijs = 10;
                gewicht = 1.10;
                beschrijving = cbRuby.Text;
            }
            // end praline - start truffel
            else if (cbPecannoot.Checked)
            {
                identifier = 401;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbPecannoot.Text;
            }
            else if (cbKaramel.Checked)
            {
                identifier = 402;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbKaramel.Text;
            }
            else if (cbClassicMelk.Checked)
            {
                identifier = 403;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbClassicMelk.Text;
            }
            else if (cbClassicDark.Checked)
            {
                identifier = 404; // Error 404 - Not Found
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbClassicDark.Text;
            }
            else if (cbCappuccino.Checked)
            {
                identifier = 405;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbCappuccino.Text;
            }
            else if (cbGember.Checked)
            {
                identifier = 406;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbGember.Text;
            }
            else if (cbFrambozen.Checked)
            {
                identifier = 407;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbFrambozen.Text;
            }
            else if (cbLimoenKokos.Checked)
            {
                identifier = 408;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbLimoenKokos.Text;
            }
            else if (cbGroeneThee.Checked)
            {
                identifier = 409;
                prijs = 12.5;
                gewicht = 0.90;
                beschrijving = cbGroeneThee.Text;
            }
            // end truffel
            else
            {
                MessageBox.Show("Gelieve een optie aan te vinken.", "Geen selectie gemaakt.", MessageBoxButtons.OK);
            }
            return identifier;
        }
        
        private void btnVerwerk_Click(object sender, EventArgs e)
        {
            VerwerkingVanChocolade();
            UpdateStockAantal();
        }
        public void UpdateStockAantal()
        {
            if (mijnDoorgekregenChocoladeLijst != null)
            {
                foreach (Chocolade item in mijnDoorgekregenChocoladeLijst)
                {
                    lbWeergaveStock.Items.Add($"{item.Soort} {item.Hoeveelheid * 1000}g");
                }
            }
            lblVentjes.Text = $"Ventjes:   {mijnDoorgekregenChocoladeVentjesLijst.Count}";
            lblPralines.Text = $"Pralines:  {mijnDoorgekregenPralineLijst.Count}";
            lblTablets.Text = $"Tablets:   {mijnDoorgekregenTabletLijst.Count}";
            lblTruffels.Text = $"Truffels:  {mijnDoorgekregenTruffelLijst.Count}";
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
