﻿
namespace Project_2_De_fervente_likker
{
    partial class FrmKlantAanmaken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtVoornaam = new System.Windows.Forms.TextBox();
            this.txtAchternaam = new System.Windows.Forms.TextBox();
            this.txtVasteTelefoon = new System.Windows.Forms.TextBox();
            this.txtStraatnaam = new System.Windows.Forms.TextBox();
            this.txtHuisnummer = new System.Windows.Forms.TextBox();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.txtGemeente = new System.Windows.Forms.TextBox();
            this.cmbProvincie = new System.Windows.Forms.ComboBox();
            this.txtGSMnummer = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtGebruikersnaam = new System.Windows.Forms.TextBox();
            this.txtWachtwoord = new System.Windows.Forms.TextBox();
            this.btnAanmaken = new System.Windows.Forms.Button();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.cbEmailUsername = new System.Windows.Forms.CheckBox();
            this.btnsluiten = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Voornaam:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(313, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Achternaam:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Straatnaam:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(308, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Huisnummer:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(87, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Postcode:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(324, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Gemeente:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(87, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Provincie:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 214);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "GSM nummer: +32";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(133, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Vaste telefoon nummer:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(102, 286);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "E-Mail:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(394, 248);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "Gebruikersnaam:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(412, 274);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 15);
            this.label12.TabIndex = 11;
            this.label12.Text = "Wachtwoord:";
            // 
            // txtVoornaam
            // 
            this.txtVoornaam.Location = new System.Drawing.Point(152, 10);
            this.txtVoornaam.Name = "txtVoornaam";
            this.txtVoornaam.Size = new System.Drawing.Size(131, 23);
            this.txtVoornaam.TabIndex = 12;
            // 
            // txtAchternaam
            // 
            this.txtAchternaam.Location = new System.Drawing.Point(394, 10);
            this.txtAchternaam.Name = "txtAchternaam";
            this.txtAchternaam.Size = new System.Drawing.Size(131, 23);
            this.txtAchternaam.TabIndex = 13;
            // 
            // txtVasteTelefoon
            // 
            this.txtVasteTelefoon.Location = new System.Drawing.Point(152, 248);
            this.txtVasteTelefoon.Name = "txtVasteTelefoon";
            this.txtVasteTelefoon.Size = new System.Drawing.Size(131, 23);
            this.txtVasteTelefoon.TabIndex = 14;
            // 
            // txtStraatnaam
            // 
            this.txtStraatnaam.Location = new System.Drawing.Point(152, 64);
            this.txtStraatnaam.Name = "txtStraatnaam";
            this.txtStraatnaam.Size = new System.Drawing.Size(131, 23);
            this.txtStraatnaam.TabIndex = 15;
            // 
            // txtHuisnummer
            // 
            this.txtHuisnummer.Location = new System.Drawing.Point(394, 64);
            this.txtHuisnummer.Name = "txtHuisnummer";
            this.txtHuisnummer.Size = new System.Drawing.Size(65, 23);
            this.txtHuisnummer.TabIndex = 16;
            // 
            // txtPostcode
            // 
            this.txtPostcode.Location = new System.Drawing.Point(152, 100);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.Size = new System.Drawing.Size(131, 23);
            this.txtPostcode.TabIndex = 17;
            // 
            // txtGemeente
            // 
            this.txtGemeente.Location = new System.Drawing.Point(394, 100);
            this.txtGemeente.Name = "txtGemeente";
            this.txtGemeente.Size = new System.Drawing.Size(131, 23);
            this.txtGemeente.TabIndex = 18;
            // 
            // cmbProvincie
            // 
            this.cmbProvincie.FormattingEnabled = true;
            this.cmbProvincie.Location = new System.Drawing.Point(153, 147);
            this.cmbProvincie.Name = "cmbProvincie";
            this.cmbProvincie.Size = new System.Drawing.Size(145, 23);
            this.cmbProvincie.TabIndex = 19;
            // 
            // txtGSMnummer
            // 
            this.txtGSMnummer.Location = new System.Drawing.Point(152, 211);
            this.txtGSMnummer.Name = "txtGSMnummer";
            this.txtGSMnummer.Size = new System.Drawing.Size(131, 23);
            this.txtGSMnummer.TabIndex = 20;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(152, 283);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(131, 23);
            this.txtEmail.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label13.Location = new System.Drawing.Point(384, 212);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 17);
            this.label13.TabIndex = 22;
            this.label13.Text = "Login gegevens:";
            // 
            // txtGebruikersnaam
            // 
            this.txtGebruikersnaam.Location = new System.Drawing.Point(496, 245);
            this.txtGebruikersnaam.Name = "txtGebruikersnaam";
            this.txtGebruikersnaam.Size = new System.Drawing.Size(131, 23);
            this.txtGebruikersnaam.TabIndex = 23;
            // 
            // txtWachtwoord
            // 
            this.txtWachtwoord.Location = new System.Drawing.Point(496, 271);
            this.txtWachtwoord.Name = "txtWachtwoord";
            this.txtWachtwoord.Size = new System.Drawing.Size(131, 23);
            this.txtWachtwoord.TabIndex = 24;
            // 
            // btnAanmaken
            // 
            this.btnAanmaken.Location = new System.Drawing.Point(312, 324);
            this.btnAanmaken.Name = "btnAanmaken";
            this.btnAanmaken.Size = new System.Drawing.Size(75, 23);
            this.btnAanmaken.TabIndex = 25;
            this.btnAanmaken.Text = "Aanmaken";
            this.btnAanmaken.UseVisualStyleBackColor = true;
            this.btnAanmaken.Click += new System.EventHandler(this.btnAanmaken_Click);
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.Location = new System.Drawing.Point(412, 324);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(75, 23);
            this.btnAnnuleren.TabIndex = 26;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // cbEmailUsername
            // 
            this.cbEmailUsername.AutoSize = true;
            this.cbEmailUsername.Location = new System.Drawing.Point(499, 220);
            this.cbEmailUsername.Name = "cbEmailUsername";
            this.cbEmailUsername.Size = new System.Drawing.Size(165, 19);
            this.cbEmailUsername.TabIndex = 27;
            this.cbEmailUsername.Text = "E-mail als gebruikersnaam";
            this.cbEmailUsername.UseVisualStyleBackColor = true;
            this.cbEmailUsername.CheckedChanged += new System.EventHandler(this.cbEmailUsername_CheckedChanged);
            // 
            // btnsluiten
            // 
            this.btnsluiten.Location = new System.Drawing.Point(509, 324);
            this.btnsluiten.Name = "btnsluiten";
            this.btnsluiten.Size = new System.Drawing.Size(75, 23);
            this.btnsluiten.TabIndex = 28;
            this.btnsluiten.Text = "Sluiten";
            this.btnsluiten.UseVisualStyleBackColor = true;
            this.btnsluiten.Click += new System.EventHandler(this.btnsluiten_Click);
            // 
            // FrmKlantAanmaken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(686, 369);
            this.Controls.Add(this.btnsluiten);
            this.Controls.Add(this.cbEmailUsername);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.btnAanmaken);
            this.Controls.Add(this.txtWachtwoord);
            this.Controls.Add(this.txtGebruikersnaam);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtGSMnummer);
            this.Controls.Add(this.cmbProvincie);
            this.Controls.Add(this.txtGemeente);
            this.Controls.Add(this.txtPostcode);
            this.Controls.Add(this.txtHuisnummer);
            this.Controls.Add(this.txtStraatnaam);
            this.Controls.Add(this.txtVasteTelefoon);
            this.Controls.Add(this.txtAchternaam);
            this.Controls.Add(this.txtVoornaam);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmKlantAanmaken";
            this.Text = "Nieuwe klant aanmaken";
            this.Load += new System.EventHandler(this.FrmKlanten_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtVoornaam;
        private System.Windows.Forms.TextBox txtAchternaam;
        private System.Windows.Forms.TextBox txtVasteTelefoon;
        private System.Windows.Forms.TextBox txtStraatnaam;
        private System.Windows.Forms.TextBox txtHuisnummer;
        private System.Windows.Forms.TextBox txtPostcode;
        private System.Windows.Forms.TextBox txtGemeente;
        private System.Windows.Forms.ComboBox cmbProvincie;
        private System.Windows.Forms.TextBox txtGSMnummer;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtGebruikersnaam;
        private System.Windows.Forms.TextBox txtWachtwoord;
        private System.Windows.Forms.Button btnAanmaken;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.CheckBox cbEmailUsername;
        private System.Windows.Forms.Button btnsluiten;
    }
}