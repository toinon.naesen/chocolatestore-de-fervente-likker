﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2_De_fervente_likker
{
    public class BestellingItem : PrijsLijstItem
    {
        //Declaraties

        private int _hoeveelheid;

        //Constructors
        public BestellingItem(string iD, string omschrijving, double prijsperKg, int hoeveelheid) : base(iD, omschrijving, prijsperKg)
        {
            Hoeveelheid = hoeveelheid;
        }

        //Propperties

        public int Hoeveelheid
        {
            get { return _hoeveelheid; }
            set { _hoeveelheid = value; }
        }

        //Methoden

    }
}
