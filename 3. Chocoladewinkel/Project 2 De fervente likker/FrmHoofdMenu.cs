﻿using Project_2_De_fervente_likker.SubForms.Bestellen;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Project_2_De_fervente_likker
{
    public partial class FrmHoofdMenu : Form
    {
        //Declaraties
        private Form _huidigeMenuSelectie; //Om bij te houden welk menu open is
        private Button _huidigeKnop;    //Om bij te houden welke knop gehighlight moet worden, zie activeerknop
        public List<ChocoladeVentje> _mijnChocoladeVentjes;
        public List<Praline> _mijnPralines;
        public List<Tablet> _mijnTablets;
        public List<Truffel> _mijnTruffels;
        public List<Chocolade> _mijnChocoladeStock;
        public List<Klant> _mijnKlant;
        public List<Personeel> _mijnPersoneel;
        public List<Administrator> _mijnAdmin;
        public int accesIdentifier = -1;
        //Constructors
        public FrmHoofdMenu()
        {
            InitializeComponent();

        }
        

        //Properties
        public void FrmHoofdMenu_Load(object sender, EventArgs e)
        {
            MijnChocoladeVentjes = new List<ChocoladeVentje>();
            MijnPralines = new List<Praline>();
            MijnTablets = new List<Tablet>();
            MijnTruffels = new List<Truffel>();
            MijnKlant = new List<Klant>();
            MijnPersoneel = new List<Personeel>();
            MijnAdmin = new List<Administrator>();
            MijnChocoladeStock = new List<Chocolade>();

            Administrator newAdmin = new Administrator("Adam", "1st");
            MijnAdmin.Add(newAdmin);
            Klant newKlant = new Klant("Benjamin", "Lauwereins", "Einde were", "254", "9000", "Gent", "Oost-Vlaanderen", 32472600341, 092279090, "benjamin.lauwereins@gmail.com", 0, "Oppa", "1234567890");
            MijnKlant.Add(newKlant);
            openMenuSelectie(new FrmLogin(this, MijnKlant, MijnPersoneel, MijnAdmin, accesIdentifier));

        }
        //Ben niet zeker of deze altijd moet maken maar heb het zo van hans geleerd dus doe het ook zo
        public Form HuidigeMenuSelectie
        {
            get { return _huidigeMenuSelectie; }
            set { _huidigeMenuSelectie = value; }
        }
        public Button Huidigeknop
        {
            get { return _huidigeKnop; }
            set { _huidigeKnop = value; }
        }
        public List<ChocoladeVentje> MijnChocoladeVentjes
        {
            get { return _mijnChocoladeVentjes; }
            set { _mijnChocoladeVentjes = value; }
        }
        public List<Praline> MijnPralines
        {
            get { return _mijnPralines; }
            set { _mijnPralines = value; }
        }
        public List<Tablet> MijnTablets
        {
            get { return _mijnTablets; }
            set { _mijnTablets = value; }
        }
        public List<Truffel> MijnTruffels
        {
            get { return _mijnTruffels; }
            set { _mijnTruffels = value; }
        }
        public List<Chocolade> MijnChocoladeStock
        {
            get { return _mijnChocoladeStock; }
            set { _mijnChocoladeStock = value; }
        }
        public List<Klant> MijnKlant
        {
            get { return _mijnKlant; }
            set { _mijnKlant = value; }
        }
        public List<Personeel> MijnPersoneel
        {
            get { return _mijnPersoneel; }
            set { _mijnPersoneel = value; }
        }
        public List<Administrator> MijnAdmin
        {
            get { return _mijnAdmin; }
            set { _mijnAdmin = value; }
        }
        //Methoden


        public void btnAssortiment_Click(object sender, EventArgs e)
        {
            if (accesIdentifier < 2)
            {
                openMenuSelectie(new FrmAssortiment(MijnChocoladeVentjes, MijnPralines, MijnTablets, MijnTruffels));
                ActiveerKnop(sender);
            }
            

        }

        private void btnKassa_Click(object sender, EventArgs e)
        {
            if (accesIdentifier < 2)
            {
                openMenuSelectie(new FrmKassa(MijnKlant));
                ActiveerKnop(sender);
            }
            

        }

        private void btnKlanten_Click(object sender, EventArgs e)
        {
            //FrmKlanten newKlant = new FrmKlanten();
            //newKlant.Show();
            if (accesIdentifier < 2)
            {
                openMenuSelectie(new FrmKlantMenu(MijnKlant));
                ActiveerKnop(sender);
            }
           

        }

        private void btnPersoneel_Click(object sender, EventArgs e)
        {
            //testcode!!
            if (accesIdentifier < 2)
            {
                openMenuSelectie(new FrmPersoneel(MijnPersoneel));

                ActiveerKnop(sender);
            }
            
        }

        private void btnBestellen_Click(object sender, EventArgs e)
        {
            if (accesIdentifier < 2)
            {
                openMenuSelectie(new FrmBestellenAanmaken());

                ActiveerKnop(sender);
            }
            
        }
        private void btnVerwerking_Click(object sender, EventArgs e)  
        {
            if (accesIdentifier < 2)
            {
                openMenuSelectie(new FrmVerwerkingChocolade(MijnChocoladeStock, MijnChocoladeVentjes, MijnPralines, MijnTablets, MijnTruffels));
                ActiveerKnop(sender);
            }
            
        }

        private void openMenuSelectie(Form nieuweMenuSelectie)
        {
            if (HuidigeMenuSelectie != null)
            {
                HuidigeMenuSelectie.Close();
            }

            HuidigeMenuSelectie = nieuweMenuSelectie;

            lbTitel.Text = nieuweMenuSelectie.Text;

            nieuweMenuSelectie.TopLevel = false;
            nieuweMenuSelectie.FormBorderStyle = FormBorderStyle.None;
            nieuweMenuSelectie.Dock = DockStyle.Fill;
            this.pnlWerkPaneel.Controls.Add(nieuweMenuSelectie);
            this.pnlWerkPaneel.Tag = nieuweMenuSelectie;
            nieuweMenuSelectie.BringToFront();
            nieuweMenuSelectie.Show();
        }

        public void EnableKnoppen()
        {
            btnAssortiment.Enabled = true;
            btnKassa.Enabled = true;
            btnKlanten.Enabled = true;
            btnPersoneel.Enabled = true;
            btnBestellen.Enabled = true;
            btnVerwerking.Enabled = true;
        }

        public void ActiveerKnop(object btnSender)
        {
            if (btnSender != null)
            {
                if (Huidigeknop != (Button)btnSender)
                {
                    DeactiveerKnop();

                    Huidigeknop = (Button)btnSender;
                    Color mijnKleur = Color.FromArgb(127, 85, 57);

                    Huidigeknop.BackColor = mijnKleur;
                    Huidigeknop.ForeColor = Color.White;
                    Huidigeknop.Font = new System.Drawing.Font("Segoe UI", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
                }
            }
        }

        public void DeactiveerKnop()
        /*  Heb deze methode momenteel overgenomen (wel zelf getypt ofc) van het voorbeeld waarop we ons baseren
         *          Zoals deze nu werkt zal ze telkens heel de form doorlopen om alle buttons te resetten
         *          zou nog eens kunnen nakijken of dit wel nodig is of ik mss enkel de betreffende button kan disablen
         *                  Dan zou ik bijvoorbeeld gewoon meegeven welke btn disabled moet worden (of gwn currentbtn)
         *                  
         *                  maar allicht is de performance increase hiervan verwaarloosbaar en kan ik het evengoed zo laten ¯\_(ツ)_/¯
         *         
         */
        {
            foreach (Control knop in pnlButtons.Controls)
            {
                if (knop.GetType() == typeof(Button))
                {
                    knop.BackColor = Color.FromArgb(166, 121, 90);
                    knop.ForeColor = Color.Gainsboro;
                    knop.Font = default;
                }
            }
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            openMenuSelectie(new FrmAbout());

            ActiveerKnop(sender);
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
