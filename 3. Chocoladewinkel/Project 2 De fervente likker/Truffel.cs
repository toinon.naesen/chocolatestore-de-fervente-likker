﻿using System;

namespace Project_2_De_fervente_likker
{
    public class Truffel : Product
    {
        //Declaraties



        //Constructors
        public Truffel(double prijs, string soortChocolade, double gewicht, string houdbaarheid, string beschrijving, int id) : base(prijs, soortChocolade, gewicht, houdbaarheid, beschrijving, id)
        {


        }
        public Truffel()
        {

        }

        //Properties


        //methode
        public override DateTime HoudbaarheidsDatum()
        {
            DateTime dateTimeNow = DateTime.Now;
            DateTime todayPlus5Days = dateTimeNow.Date.AddDays(5);
            return todayPlus5Days;
        }

        public override string ToString()
        {
            return HoudbaarheidsDatum().ToString("dd-MM-yyyy");
        }

        public override bool Equals(object obj)
        {
            bool check = true;

            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Truffel t = (Truffel)obj;
                if (this.Beschrijving != t.Beschrijving || this.Prijs != t.Prijs || this.SoortChocolade != t.SoortChocolade || this.Gewicht != t.Gewicht || this.Houdbaarheid != t.Houdbaarheid || this.ID != t.ID)
                {
                    check = false;
                }
            }
            return check;
        }
    }
}
