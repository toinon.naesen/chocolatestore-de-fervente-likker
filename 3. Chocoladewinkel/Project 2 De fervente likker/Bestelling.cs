﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2_De_fervente_likker
{
    public class Bestelling
    {
        /*      Deze klasse dient als compositie om daarna te converteren naar een xlm file die aan groep 2 gegeven word
         *      
         *      ik tast hier een beetje in het duister, weet zelf niet goed wat nodig
         *      
         *      in de eerste plaats denk ik aan datum en ordernummer
         *      
         *      mogelijks welke gebruiker deze geplaatst heeft 
         * 
         * 
         * 
         */



        //Declaraties
        private DateTime _aanmakingsDatum;
        private string _orderNummer;
        private List<BestellingItem> _bestellingItemlijst;

        private string _gebruikersnaamOpmaker;
        private string _idOpmaker;



        //Constructors
        public Bestelling(List<BestellingItem> bestellingItemLijst)
        {
            BestellingItemLijst = bestellingItemLijst;
            GebruikersnaamOpmaker = "Default";
            IDOpmaker = "0000";

            Aanmakingsdatum = DateTime.Now;
            OrderNummer = $"{Aanmakingsdatum:ddMMyyyyHHmmss}{IDOpmaker}";
        }

        public Bestelling(List<BestellingItem> bestellingItemLijst, string gebruikersnaamOpmaker, string iDOpmaker) //dit was voor als we er toe zouden komen bij te houden welke gebruiker wat doet
        {
            BestellingItemLijst = bestellingItemLijst;
            GebruikersnaamOpmaker = gebruikersnaamOpmaker;
            IDOpmaker = iDOpmaker;

            Aanmakingsdatum = DateTime.Now;
            OrderNummer = $"{Aanmakingsdatum:ddMMyyyyHHmmss}{IDOpmaker}";
        }

        //Properties
        public DateTime Aanmakingsdatum
        {
            get { return _aanmakingsDatum; }
            set { _aanmakingsDatum = value; }
        }

        public string OrderNummer
        {
            get { return _orderNummer; }
            set { _orderNummer = value; }
        }

        public List<BestellingItem> BestellingItemLijst
        {
            get { return _bestellingItemlijst; }
            set { _bestellingItemlijst = value; }
        }

        public string GebruikersnaamOpmaker
        {
            get { return _gebruikersnaamOpmaker; }
            set { _gebruikersnaamOpmaker = value; }
        }

        public string IDOpmaker
        {
            get { return _idOpmaker; }
            set { _idOpmaker = value; }
        }


        //Methoden



    }
}
