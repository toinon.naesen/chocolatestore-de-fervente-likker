﻿using Project_2_De_fervente_likker.SubForms.Bestellen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_2_De_fervente_likker
{
    public partial class FrmBestellen : Form
    {
        /*          Ga voor de moment methode om menu's open te doen dubbel inzetten
         *              wil later nog eens zien naar een manier waarop deze form zou kunnen overerven zodat ik deze methode vanuit hoofdmenu kan gebruike,
         *              
         *              
         *              wegens tijdgebrek wil ik nu eerst bestellen afwerken en daarvoor heb ik dit nodig
         * 
         * 
         * 
         * 
         */


        private Form _huidigeMenuSelectie;
        //private List<Bestelling> _bestellingenLijst = new List<Bestelling>();

        //public List<Bestelling> BestellingenLijst
        //{
        //    get { return _bestellingenLijst; }
        //    set { _bestellingenLijst = value; }
        //}



        public Form HuidigeMenuSelectie
        {
            get { return _huidigeMenuSelectie; }
            set { _huidigeMenuSelectie = value; }
        }

        public FrmBestellen()
        {
            InitializeComponent();
        }

        private void FrmBestellen_Load(object sender, EventArgs e)
        {
            openMenuSelectie(new FrmBestellenOverzicht());
        }

        private void openMenuSelectie(Form nieuweMenuSelectie)
        {
            if (HuidigeMenuSelectie != null)
            {
                HuidigeMenuSelectie.Close();
            }

            HuidigeMenuSelectie = nieuweMenuSelectie;


            nieuweMenuSelectie.TopLevel = false;
            nieuweMenuSelectie.FormBorderStyle = FormBorderStyle.None;
            nieuweMenuSelectie.Dock = DockStyle.Fill;
            this.pnlMainWindow.Controls.Add(nieuweMenuSelectie);
            this.pnlMainWindow.Tag = nieuweMenuSelectie;
            nieuweMenuSelectie.BringToFront();
            nieuweMenuSelectie.Show();
        }

        private void btnOverzicht_Click(object sender, EventArgs e)
        {
            openMenuSelectie(new FrmBestellenOverzicht());
        }

        private void btnBestellingAanmaken_Click(object sender, EventArgs e)
        {
            openMenuSelectie(new FrmBestellenAanmaken());
        }
    }
}
