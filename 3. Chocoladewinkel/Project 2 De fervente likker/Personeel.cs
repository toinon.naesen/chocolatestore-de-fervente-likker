﻿using System;

namespace Project_2_De_fervente_likker
{
    public class Personeel : Gebruiker
    {
        private int _matricule;
        private char _gender;
        private string _naam;
        private string _voornaam;
        private DateTime _geboortedatum;
        private string _functie;
        private string _straatnaam;
        private string _huisnummer;
        private string _postcode;
        private string _gemeente;
        private string _land;
        private int _telefoonummer;
        public Personeel(string loginname, string wachtwoord) : base(loginname, wachtwoord)
        {

        }
        public Personeel(int matricul, char geder, string nam, string voornaam, DateTime geboorte, string fonct, string straat, string huisnum, string codepos, string gemeent, string lan, int telefoon, string loginNaam, string wachtwoord) : base(loginNaam, wachtwoord)
        {
            Matricule = matricul;
            Gender = geder;
            Naam = nam;
            Voornaam = voornaam;
            Geboortedatum = geboorte;
            Functie = fonct;
            Straatnaam = straat;
            Huisnummer = huisnum;
            Postcode = codepos;
            Gemeente = gemeent;
            Land = lan;
            Telefoonummer = telefoon;
        }
        public int Matricule
        {
            get { return _matricule; }
            set { _matricule = value; }
        }
        public char Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }
        public DateTime Geboortedatum
        {
            get { return _geboortedatum; }
            set { _geboortedatum = value; }
        }
        public string Functie
        {
            get { return _functie; }
            set { _functie = value; }
        }
        public string Straatnaam
        {
            get { return _straatnaam; }
            set { _straatnaam = value; }
        }
        public string Huisnummer
        {
            get { return _huisnummer; }
            set { _huisnummer = value; }
        }
        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }
        public string Gemeente
        {
            get { return _gemeente; }
            set { _gemeente = value; }
        }
        public string Land
        {
            get { return _land; }
            set { _land = value; }
        }
        public int Telefoonummer
        {
            get { return _telefoonummer; }
            set { _telefoonummer = value; }
        }
        public override bool Equals(object obj)
        {
            bool check = true;


            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Personeel k = (Personeel)obj;
                if (this.Gebruikersnaam != k.Gebruikersnaam || this.Passwoord != k.Passwoord)
                {
                    check = false;
                }

            }

            return check;
        }
    }
}
