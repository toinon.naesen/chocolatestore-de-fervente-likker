﻿using System;

namespace Project_2_De_fervente_likker
{
    public class Product
    {
        /*      Moet hebben:
         *              -naam
         *              -prijs
         *              
         *      Eventueel:
         *              -houdbaarheidsdatum
         *              -lotnummer
         *              -omschrijving
         */


        //Declaraties
        private double _prijs;
        private string _soortChocolade;
        private double _gewicht;
        private string _houdbaarheid;
        private string _beschrijving;
        private int _id;

        //Constructor
        public Product(double prijs, string soortChocolade, double gewicht, string houdbaarheid, string beschrijving, int id)
        {
            Prijs = prijs;
            SoortChocolade = soortChocolade;
            Gewicht = gewicht;
            Beschrijving = beschrijving;
            ID = id;
            Houdbaarheid = houdbaarheid;
        }
        public Product()
        {


        }

        //Properties 
        public double Prijs
        {
            get { return _prijs; }
            set { _prijs = value; }
        }

        public string SoortChocolade
        {
            get { return _soortChocolade; }
            set { _soortChocolade = value; }
        }
        public double Gewicht
        {
            get { return _gewicht; }
            set { _gewicht = value; }
        }
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Beschrijving
        {
            get { return _beschrijving; }
            set { _beschrijving = value; }
        }

        public string Houdbaarheid
        {
            get { return _houdbaarheid; }
            set { _houdbaarheid = value; }
        }


        public virtual DateTime HoudbaarheidsDatum()
        {
            DateTime dateTimeNow = DateTime.Now;
            DateTime datePartOnly = dateTimeNow.Date;
            return datePartOnly;
        }
        //methode
        public override string ToString()
        {
            return HoudbaarheidsDatum().ToString("dd-MM-yyyy");
        }
        public override bool Equals(object obj)
        {
            bool check = true;

            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Product p = (Product)obj;
                if (this.Beschrijving != p.Beschrijving || this.Prijs != p.Prijs || this.SoortChocolade != p.SoortChocolade || this.Gewicht != p.Gewicht || this.Houdbaarheid != p.Houdbaarheid || this.ID != p.ID)
                {
                    check = false;
                }
            }
            return check;
        }
    }
}
