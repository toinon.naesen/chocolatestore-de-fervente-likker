﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Project_2_De_fervente_likker.Behaviour_Test_Forms
{
    public partial class FrmBestellenTestViewsV1 : Form
    {

        //heb alles hierin gekopieerd vanuit project zodat later nog eens kan gebruiken, zou comments hier nog voor moeten updaten
        public FrmBestellenTestViewsV1()
        {
            InitializeComponent();
        }

        private void FrmBestellenTestViewsV1_Load(object sender, EventArgs e)
        {
            LeesPijslijstXLM();
            VullListBoxAan();
            VullDatagridViewAan();

            lbPrijslijstLeverancier.DataSource = mijnPrijsLijst;
        }

        //Declaraties

        List<PrijsLijstItem> mijnPrijsLijst = new List<PrijsLijstItem>(); //Moet hier propperties voor gebruiken? weet dit nooit met lists...

        public void VullListBoxAan()
        {
            foreach (var item in mijnPrijsLijst)
            {
                lvPrijslijstLeverancier.Items.Add(new ListViewItem(item.ToStringArray()));
            }
        }

        public void VullDatagridViewAan()
        {
            foreach (var item in mijnPrijsLijst)
            {
                dgvPrijslijstLeverancier.Rows.Add(item.ToStringArray());
            }
        }



        public void LeesPijslijstXLM()
        {
            /*    Volgende methode zal een lijst opvullen op basis van de gegeven XML file
             *    
             *    ik zou hier nog eens moeten naar kijken want dit kan zeker beter (wat ik hier mee bedoel zal ik wss zelf vergeten) waarom schrijf ik het dan niet op? ik weet het niet...
             *    
             *    ik zou gewoon eerst een pak meer met xml bezig moeten zijn voor ik het zeker kan zeggen
             * 
             */

            string tempID = "";
            string tempOmschrijving = "";
            double tempPrijsPerKg = 0;


            using (XmlTextReader reader = new XmlTextReader(@"C:\Users\luise\source\repos\project-2-de-fervente-likker\Project 2 De fervente likker\XML Files\prijslijst.xml"))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name == "artikel")
                            {
                                reader.MoveToNextAttribute();
                                tempID = reader.Value;
                            }
                            else if (reader.Name == "naam")
                            {
                                reader.Read();
                                tempOmschrijving = reader.Value;
                            }
                            else if (reader.Name == "prijsperkg")
                            {
                                reader.Read();
                                tempPrijsPerKg = Convert.ToDouble(reader.Value);
                            }
                            break;
                        case XmlNodeType.Text:
                            break;
                        case XmlNodeType.EndElement:
                            if (reader.Name == "artikel")
                            {
                                mijnPrijsLijst.Add(new PrijsLijstItem(tempID, tempOmschrijving, tempPrijsPerKg));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }




        }
    }
}
