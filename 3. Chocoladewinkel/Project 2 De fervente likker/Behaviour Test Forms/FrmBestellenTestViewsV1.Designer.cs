﻿
namespace Project_2_De_fervente_likker.Behaviour_Test_Forms
{
    partial class FrmBestellenTestViewsV1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvPrijslijstLeverancier = new System.Windows.Forms.ListView();
            this.columnID = new System.Windows.Forms.ColumnHeader();
            this.columnOmschrijving = new System.Windows.Forms.ColumnHeader();
            this.columnPrijsPerKg = new System.Windows.Forms.ColumnHeader();
            this.dgvPrijslijstLeverancier = new System.Windows.Forms.DataGridView();
            this.ID_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Omschrijving = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_PrijsPerKg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbPrijslijstLeverancier = new System.Windows.Forms.ListBox();
            this.lblPrijslijst = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrijslijstLeverancier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lvPrijslijstLeverancier
            // 
            this.lvPrijslijstLeverancier.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnID,
            this.columnOmschrijving,
            this.columnPrijsPerKg});
            this.lvPrijslijstLeverancier.FullRowSelect = true;
            this.lvPrijslijstLeverancier.GridLines = true;
            this.lvPrijslijstLeverancier.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvPrijslijstLeverancier.HideSelection = false;
            this.lvPrijslijstLeverancier.Location = new System.Drawing.Point(154, 323);
            this.lvPrijslijstLeverancier.MultiSelect = false;
            this.lvPrijslijstLeverancier.Name = "lvPrijslijstLeverancier";
            this.lvPrijslijstLeverancier.Size = new System.Drawing.Size(476, 197);
            this.lvPrijslijstLeverancier.TabIndex = 7;
            this.lvPrijslijstLeverancier.UseCompatibleStateImageBehavior = false;
            this.lvPrijslijstLeverancier.View = System.Windows.Forms.View.Details;
            // 
            // columnID
            // 
            this.columnID.Tag = "ID";
            this.columnID.Text = "ID";
            // 
            // columnOmschrijving
            // 
            this.columnOmschrijving.Tag = "Omschrijving";
            this.columnOmschrijving.Text = "Omschrijving";
            this.columnOmschrijving.Width = 100;
            // 
            // columnPrijsPerKg
            // 
            this.columnPrijsPerKg.Tag = "PrijsPerKg";
            this.columnPrijsPerKg.Text = "Prijs (per Kg)";
            this.columnPrijsPerKg.Width = 100;
            // 
            // dgvPrijslijstLeverancier
            // 
            this.dgvPrijslijstLeverancier.AllowUserToAddRows = false;
            this.dgvPrijslijstLeverancier.AllowUserToDeleteRows = false;
            this.dgvPrijslijstLeverancier.AllowUserToResizeColumns = false;
            this.dgvPrijslijstLeverancier.AllowUserToResizeRows = false;
            this.dgvPrijslijstLeverancier.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPrijslijstLeverancier.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPrijslijstLeverancier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrijslijstLeverancier.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Column,
            this.Column_Omschrijving,
            this.Column_PrijsPerKg});
            this.dgvPrijslijstLeverancier.Location = new System.Drawing.Point(154, 561);
            this.dgvPrijslijstLeverancier.MultiSelect = false;
            this.dgvPrijslijstLeverancier.Name = "dgvPrijslijstLeverancier";
            this.dgvPrijslijstLeverancier.ReadOnly = true;
            this.dgvPrijslijstLeverancier.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.dgvPrijslijstLeverancier.RowTemplate.Height = 25;
            this.dgvPrijslijstLeverancier.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPrijslijstLeverancier.Size = new System.Drawing.Size(476, 197);
            this.dgvPrijslijstLeverancier.TabIndex = 10;
            // 
            // ID_Column
            // 
            this.ID_Column.HeaderText = "ID";
            this.ID_Column.Name = "ID_Column";
            this.ID_Column.ReadOnly = true;
            // 
            // Column_Omschrijving
            // 
            this.Column_Omschrijving.HeaderText = "Omschrijving";
            this.Column_Omschrijving.Name = "Column_Omschrijving";
            this.Column_Omschrijving.ReadOnly = true;
            // 
            // Column_PrijsPerKg
            // 
            this.Column_PrijsPerKg.HeaderText = "Prijs per Kg (in euro)";
            this.Column_PrijsPerKg.Name = "Column_PrijsPerKg";
            this.Column_PrijsPerKg.ReadOnly = true;
            this.Column_PrijsPerKg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // lbPrijslijstLeverancier
            // 
            this.lbPrijslijstLeverancier.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPrijslijstLeverancier.FormattingEnabled = true;
            this.lbPrijslijstLeverancier.ItemHeight = 15;
            this.lbPrijslijstLeverancier.Location = new System.Drawing.Point(154, 89);
            this.lbPrijslijstLeverancier.Name = "lbPrijslijstLeverancier";
            this.lbPrijslijstLeverancier.Size = new System.Drawing.Size(476, 199);
            this.lbPrijslijstLeverancier.TabIndex = 9;
            // 
            // lblPrijslijst
            // 
            this.lblPrijslijst.AutoSize = true;
            this.lblPrijslijst.Location = new System.Drawing.Point(154, 42);
            this.lblPrijslijst.Name = "lblPrijslijst";
            this.lblPrijslijst.Size = new System.Drawing.Size(50, 15);
            this.lblPrijslijst.TabIndex = 8;
            this.lblPrijslijst.Text = "Prijslijst:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 561);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 15);
            this.label3.TabIndex = 13;
            this.label3.Text = "DatagridView";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 323);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 12;
            this.label2.Text = "Listview";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 11;
            this.label1.Text = "Listbox";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dataGridView1.Location = new System.Drawing.Point(743, 561);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(476, 197);
            this.dataGridView1.TabIndex = 14;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Omschrijving";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Prijs per Kg (in euro)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FrmBestellenTestViewsV1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 896);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lvPrijslijstLeverancier);
            this.Controls.Add(this.dgvPrijslijstLeverancier);
            this.Controls.Add(this.lbPrijslijstLeverancier);
            this.Controls.Add(this.lblPrijslijst);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmBestellenTestViewsV1";
            this.Text = "FrmBestellenTestViewsV1";
            this.Load += new System.EventHandler(this.FrmBestellenTestViewsV1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrijslijstLeverancier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvPrijslijstLeverancier;
        private System.Windows.Forms.ColumnHeader columnID;
        private System.Windows.Forms.ColumnHeader columnOmschrijving;
        private System.Windows.Forms.ColumnHeader columnPrijsPerKg;
        private System.Windows.Forms.DataGridView dgvPrijslijstLeverancier;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Omschrijving;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_PrijsPerKg;
        private System.Windows.Forms.ListBox lbPrijslijstLeverancier;
        private System.Windows.Forms.Label lblPrijslijst;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}