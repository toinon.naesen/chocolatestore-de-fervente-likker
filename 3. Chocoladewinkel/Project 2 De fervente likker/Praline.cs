﻿using System;

namespace Project_2_De_fervente_likker
{
    public class Praline : Product
    {
        //Declaraties



        //Constructors
        public Praline(double prijs, string soortChocolade, double gewicht, string houdbaarheid, string beschrijving, int id) : base(prijs, soortChocolade, gewicht, houdbaarheid, beschrijving, id)
        {


        }
        public Praline()
        {

        }


        //Properties


        //methode
        public override DateTime HoudbaarheidsDatum()
        {
            DateTime dateTimeNow = DateTime.Now;
            DateTime todayPlus6Months = dateTimeNow.Date.AddDays(180);
            return todayPlus6Months;
        }

        public override string ToString()
        {
            return HoudbaarheidsDatum().ToString("dd-MM-yyyy");
        }

        public override bool Equals(object obj)
        {
            bool check = true;

            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                Praline p = (Praline)obj;
                if (this.Beschrijving != p.Beschrijving || this.Prijs != p.Prijs || this.SoortChocolade != p.SoortChocolade || this.Gewicht != p.Gewicht || this.Houdbaarheid != p.Houdbaarheid || this.ID != p.ID)
                {
                    check = false;
                }
            }
            return check;
        }
    }
}
