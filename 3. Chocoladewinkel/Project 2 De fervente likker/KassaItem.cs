﻿namespace Project_2_De_fervente_likker
{
    class KassaItem
    {
        //attributen
        private int _aantal;
        private string _artikel;
        private double _prijs;

        //constructors
        public KassaItem() { }

        public KassaItem(int aantal, string artikel, double prijs)
        {
            Aantal = aantal;
            Artikel = artikel;
            Prijs = prijs;
        }

        //properties
        public int Aantal
        {
            get { return _aantal; }
            set { _aantal = value; }
        }

        public double Prijs
        {
            get { return _prijs; }
            set { _prijs = value; }
        }

        public string Artikel
        {
            get { return _artikel; }
            set { _artikel = value; }
        }

        public double TotalePrijs
        {
            get
            {
                return Aantal * Prijs;
            }
        }

        //methodes
        public string FormattedTotalePrijs()
        {
            string totaleKassa;
            totaleKassa = TotalePrijs.ToString("0.00");
            return totaleKassa;
        }

        public override string ToString()
        {
            string toonKassa = "";

            toonKassa = Aantal.ToString().PadLeft(5) + Artikel.PadLeft(27) + "€".PadLeft(23) + Prijs.ToString("0.00") + " €".PadLeft(20) + FormattedTotalePrijs();

            return toonKassa;

        }
    }
}

