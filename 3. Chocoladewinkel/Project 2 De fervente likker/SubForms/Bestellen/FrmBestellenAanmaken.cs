﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace Project_2_De_fervente_likker.SubForms.Bestellen
{
    public partial class FrmBestellenAanmaken : Form
    {
        //Declaraties

        List<PrijsLijstItem> mijnPrijsLijst = new List<PrijsLijstItem>(); //nog proppertie maken


        //Moet eigenlijk niveau hogerop, voor test
        private List<Bestelling> _bestellingenLijst = new List<Bestelling>();

        public List<Bestelling> BestellingenLijst
        {
            get { return _bestellingenLijst; }
            set { _bestellingenLijst = value; }
        }



        public FrmBestellenAanmaken()
        {
            InitializeComponent();
        }

        private void FrmBestellenAanmaken_Load(object sender, EventArgs e)
        {
            LeesPijslijstXLM();
            VullDatagridViewAan();
        }

        public void VullDatagridViewAan()
        {
            foreach (var item in mijnPrijsLijst)
            {
                dgvPrijslijstLeverancier.Rows.Add(item.ToStringArray());
            }
        }

        public void LeesPijslijstXLM()
        {
            /*    Volgende methode zal een lijst opvullen op basis van de gegeven XML file
             *    is me opgevallen dat we normal geen xmltextreader gebruiken maar een gewone xmlreader, moet nog eens uitzoeken wat de verschillen zijn...
             * 
             */

            string tempID = "";
            string tempOmschrijving = "";
            double tempPrijsPerKg = 0;


            using (XmlTextReader reader = new XmlTextReader(@"..\..\..\XML Files\prijslijst.xml"))//..\..\..\XML Files\prijslijst.xml
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name == "artikel")
                            {
                                reader.MoveToNextAttribute();
                                tempID = reader.Value;
                            }
                            else if (reader.Name == "naam")
                            {
                                reader.Read();
                                tempOmschrijving = reader.Value;
                            }
                            else if (reader.Name == "prijsperkg")
                            {
                                reader.Read();
                                tempPrijsPerKg = Convert.ToDouble(reader.Value);
                            }
                            break;
                        case XmlNodeType.Text:
                            break;
                        case XmlNodeType.EndElement:
                            if (reader.Name == "artikel")
                            {
                                mijnPrijsLijst.Add(new PrijsLijstItem(tempID, tempOmschrijving, tempPrijsPerKg));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void btnToevoegen_Click(object sender, EventArgs e)
        {
            /*      Heb deze methode vluchtich geschreven dus moet nog eens opschonen
             *      
             *      Was ook nog vollop aant het leren met datagridview werken
             *          ben zeker dat foreach kan wegdoen 
             *          en zou toch eens moeten uitzoeken wat voordelen zijn van datatable
             * 
             */


            //Dit stukje zal checken of het id al in de lijst staat. Mss kan dit ook beter dan altijd door de hele array loopen
            bool alAanwezig = false;
            int aanwezigOpIndex = 0;

            for (int i = 0; i < dgvOverzicht.Rows.Count; i++)
            {
                if (dgvOverzicht.Rows[i].Cells[0].Value == dgvPrijslijstLeverancier.SelectedRows[0].Cells[0].Value)
                {
                    alAanwezig = true;
                    aanwezigOpIndex = i;
                }
            }

            if (!alAanwezig)
            {
                string[] arrTempStringArray = new string[4];


                //uit datagrid halen zonder foreach
                arrTempStringArray[0] = dgvPrijslijstLeverancier.SelectedRows[0].Cells[0].Value.ToString();
                arrTempStringArray[1] = dgvPrijslijstLeverancier.SelectedRows[0].Cells[1].Value.ToString();
                arrTempStringArray[2] = dgvPrijslijstLeverancier.SelectedRows[0].Cells[2].Value.ToString();

                ////Oude manier met foreach maar lijkt me onnodig
                //int index = 0;
                //foreach (DataGridViewCell item in dgvPrijslijstLeverancier.SelectedCells)
                //{
                //    arrTempStringArray[index] = item.Value.ToString();
                //    index++;
                //}

                arrTempStringArray[3] = nudHoeveelheid.Value.ToString(); //Hoeveelheid toevoegen

                dgvOverzicht.Rows.Add(arrTempStringArray);
            }
            else
            {
                //Dit stukje code is zodat je items kan blijven optellen op de juiste plek met de knopt toevoegen
                decimal currentValue = Convert.ToDecimal(dgvOverzicht.Rows[aanwezigOpIndex].Cells[3].Value);
                decimal newValue = currentValue + nudHoeveelheid.Value;

                dgvOverzicht.Rows[aanwezigOpIndex].Cells[3].Value = newValue.ToString();
            }

        }

        private void btnAanmaken_Click(object sender, EventArgs e)
        {
            List<BestellingItem> mijnBestelling = new List<BestellingItem>();

            string tempID = "";
            string tempOmschrijving = "";
            double tempPrijsPerKg = 0;
            int tempHoeveelheid = 0;

            foreach (DataGridViewRow item in dgvOverzicht.Rows)
            {
                tempID = item.Cells[0].Value.ToString();
                tempOmschrijving = item.Cells[1].Value.ToString();
                tempPrijsPerKg = Convert.ToDouble(item.Cells[2].Value);
                tempHoeveelheid = Convert.ToInt32(item.Cells[3].Value);

                mijnBestelling.Add(new BestellingItem(tempID, tempOmschrijving, tempPrijsPerKg, tempHoeveelheid));
            }
            Bestelling nieuweBestelling = new Bestelling(mijnBestelling);
            BestellingNaarXML(nieuweBestelling);

            BestellingenLijst.Add(nieuweBestelling);

            MessageBox.Show($"Bestelling met odernummer {nieuweBestelling.OrderNummer} is aangemaakt!","Bestelling Aangemaakt");
        }

        private void BestellingNaarXML(Bestelling localBestelling)
        {
            XmlWriterSettings settings = new XmlWriterSettings();

            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.OmitXmlDeclaration = true;

            using (XmlWriter writer = XmlWriter.Create($"{localBestelling.OrderNummer}-DFLOrder.xml", settings))
            {
                //Beginnen met de bestelling en ordenummer als atribuut
                writer.WriteStartElement("Bestelling");
                writer.WriteAttributeString("OrderNummer", localBestelling.OrderNummer);

                //Dan de datum van aanmaak en wie opgesteld heeft als elementen van bestelling
                writer.WriteStartElement("Info");
                writer.WriteElementString("AanmaakDatum",localBestelling.Aanmakingsdatum.ToString());
                writer.WriteElementString("OpgesteldDoor", localBestelling.GebruikersnaamOpmaker);
                writer.WriteElementString("BijhorendGebruikerID", localBestelling.IDOpmaker);
                writer.WriteEndElement();

                //Dan de opsomming van alle bestelitems in de bestelling
                writer.WriteStartElement("BesteldeArtikels");

                foreach (BestellingItem item in localBestelling.BestellingItemLijst)
                {
                    writer.WriteStartElement("Artikel");
                    writer.WriteAttributeString("ID", item.ID.ToString());

                    writer.WriteElementString("Omschrijving", item.Omschrijving);
                    writer.WriteElementString("PrijsPerKg", item.PrijsperKg.ToString());
                    writer.WriteElementString("AantalKgBesteld", item.Hoeveelheid.ToString());

                    writer.WriteEndElement();
                }

                //Als laatste alles dichtdoen
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        private void btnWijzigenPlus_Click(object sender, EventArgs e)
        {
            int currentValue = Convert.ToInt32(dgvOverzicht.SelectedRows[0].Cells[3].Value);

            currentValue += Convert.ToInt32(nudWijzigen.Value);

            dgvOverzicht.SelectedRows[0].Cells[3].Value = currentValue;
        }

        private void btnWijzigenMin_Click(object sender, EventArgs e)
        {
            int currentValue = Convert.ToInt32(dgvOverzicht.SelectedRows[0].Cells[3].Value);

            if (currentValue == 1)
            {
                MessageBox.Show("Je kan niet minder dan 1 exemplaar bestellen, verwijder dit item indien u dit niet meer wenst","Melding", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                currentValue -= Convert.ToInt32(nudWijzigen.Value);

                dgvOverzicht.SelectedRows[0].Cells[3].Value = currentValue;
            }
        }

        private void btnSelectieVerwijderen_Click(object sender, EventArgs e)
        {
            if (dgvOverzicht.SelectedRows.Count > 0)
            {
                dgvOverzicht.Rows.RemoveAt(dgvOverzicht.SelectedRows[0].Index);
            }
        }
    }
}
