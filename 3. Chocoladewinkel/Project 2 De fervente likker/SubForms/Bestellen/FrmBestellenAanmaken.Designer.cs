﻿
namespace Project_2_De_fervente_likker.SubForms.Bestellen
{
    partial class FrmBestellenAanmaken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPrijslijst = new System.Windows.Forms.Label();
            this.dgvPrijslijstLeverancier = new System.Windows.Forms.DataGridView();
            this.ID_Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Omschrijving = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_PrijsPerKg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnToevoegen = new System.Windows.Forms.Button();
            this.dgvOverzicht = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hoeveel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nudHoeveelheid = new System.Windows.Forms.NumericUpDown();
            this.btnAanmaken = new System.Windows.Forms.Button();
            this.nudWijzigen = new System.Windows.Forms.NumericUpDown();
            this.btnWijzigenPlus = new System.Windows.Forms.Button();
            this.btnWijzigenMin = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSelectieVerwijderen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrijslijstLeverancier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOverzicht)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoeveelheid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWijzigen)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPrijslijst
            // 
            this.lblPrijslijst.AutoSize = true;
            this.lblPrijslijst.Location = new System.Drawing.Point(55, 50);
            this.lblPrijslijst.Name = "lblPrijslijst";
            this.lblPrijslijst.Size = new System.Drawing.Size(50, 15);
            this.lblPrijslijst.TabIndex = 1;
            this.lblPrijslijst.Text = "Prijslijst:";
            // 
            // dgvPrijslijstLeverancier
            // 
            this.dgvPrijslijstLeverancier.AllowUserToAddRows = false;
            this.dgvPrijslijstLeverancier.AllowUserToDeleteRows = false;
            this.dgvPrijslijstLeverancier.AllowUserToResizeColumns = false;
            this.dgvPrijslijstLeverancier.AllowUserToResizeRows = false;
            this.dgvPrijslijstLeverancier.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPrijslijstLeverancier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrijslijstLeverancier.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Column,
            this.Column_Omschrijving,
            this.Column_PrijsPerKg});
            this.dgvPrijslijstLeverancier.Location = new System.Drawing.Point(55, 92);
            this.dgvPrijslijstLeverancier.MultiSelect = false;
            this.dgvPrijslijstLeverancier.Name = "dgvPrijslijstLeverancier";
            this.dgvPrijslijstLeverancier.ReadOnly = true;
            this.dgvPrijslijstLeverancier.RowHeadersWidth = 21;
            this.dgvPrijslijstLeverancier.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPrijslijstLeverancier.RowTemplate.Height = 25;
            this.dgvPrijslijstLeverancier.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPrijslijstLeverancier.Size = new System.Drawing.Size(476, 197);
            this.dgvPrijslijstLeverancier.TabIndex = 3;
            // 
            // ID_Column
            // 
            this.ID_Column.HeaderText = "ID";
            this.ID_Column.Name = "ID_Column";
            this.ID_Column.ReadOnly = true;
            this.ID_Column.Width = 80;
            // 
            // Column_Omschrijving
            // 
            this.Column_Omschrijving.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_Omschrijving.HeaderText = "Omschrijving";
            this.Column_Omschrijving.Name = "Column_Omschrijving";
            this.Column_Omschrijving.ReadOnly = true;
            // 
            // Column_PrijsPerKg
            // 
            this.Column_PrijsPerKg.HeaderText = "Prijs per Kg (in euro)";
            this.Column_PrijsPerKg.Name = "Column_PrijsPerKg";
            this.Column_PrijsPerKg.ReadOnly = true;
            this.Column_PrijsPerKg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_PrijsPerKg.Width = 151;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 334);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Bestelling overzicht:";
            // 
            // btnToevoegen
            // 
            this.btnToevoegen.Location = new System.Drawing.Point(834, 243);
            this.btnToevoegen.Name = "btnToevoegen";
            this.btnToevoegen.Size = new System.Drawing.Size(129, 46);
            this.btnToevoegen.TabIndex = 6;
            this.btnToevoegen.Text = "Toevoegen";
            this.btnToevoegen.UseVisualStyleBackColor = true;
            this.btnToevoegen.Click += new System.EventHandler(this.btnToevoegen_Click);
            // 
            // dgvOverzicht
            // 
            this.dgvOverzicht.AllowUserToAddRows = false;
            this.dgvOverzicht.AllowUserToDeleteRows = false;
            this.dgvOverzicht.AllowUserToResizeColumns = false;
            this.dgvOverzicht.AllowUserToResizeRows = false;
            this.dgvOverzicht.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvOverzicht.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvOverzicht.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOverzicht.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Hoeveel});
            this.dgvOverzicht.Location = new System.Drawing.Point(55, 372);
            this.dgvOverzicht.MultiSelect = false;
            this.dgvOverzicht.Name = "dgvOverzicht";
            this.dgvOverzicht.ReadOnly = true;
            this.dgvOverzicht.RowHeadersWidth = 21;
            this.dgvOverzicht.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvOverzicht.RowTemplate.Height = 25;
            this.dgvOverzicht.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOverzicht.Size = new System.Drawing.Size(908, 281);
            this.dgvOverzicht.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Omschrijving";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Prijs per Kg (in euro)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Hoeveel
            // 
            this.Hoeveel.HeaderText = "Hoeveel (in Kg)";
            this.Hoeveel.Name = "Hoeveel";
            this.Hoeveel.ReadOnly = true;
            // 
            // nudHoeveelheid
            // 
            this.nudHoeveelheid.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.nudHoeveelheid.Location = new System.Drawing.Point(593, 243);
            this.nudHoeveelheid.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudHoeveelheid.Name = "nudHoeveelheid";
            this.nudHoeveelheid.Size = new System.Drawing.Size(183, 46);
            this.nudHoeveelheid.TabIndex = 8;
            this.nudHoeveelheid.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnAanmaken
            // 
            this.btnAanmaken.BackColor = System.Drawing.Color.LightGreen;
            this.btnAanmaken.Location = new System.Drawing.Point(992, 607);
            this.btnAanmaken.Name = "btnAanmaken";
            this.btnAanmaken.Size = new System.Drawing.Size(283, 46);
            this.btnAanmaken.TabIndex = 9;
            this.btnAanmaken.Text = "Bestelling aanmaken";
            this.btnAanmaken.UseVisualStyleBackColor = false;
            this.btnAanmaken.Click += new System.EventHandler(this.btnAanmaken_Click);
            // 
            // nudWijzigen
            // 
            this.nudWijzigen.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.nudWijzigen.Location = new System.Drawing.Point(25, 52);
            this.nudWijzigen.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudWijzigen.Name = "nudWijzigen";
            this.nudWijzigen.Size = new System.Drawing.Size(85, 46);
            this.nudWijzigen.TabIndex = 10;
            this.nudWijzigen.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnWijzigenPlus
            // 
            this.btnWijzigenPlus.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnWijzigenPlus.Location = new System.Drawing.Point(133, 52);
            this.btnWijzigenPlus.Name = "btnWijzigenPlus";
            this.btnWijzigenPlus.Size = new System.Drawing.Size(49, 46);
            this.btnWijzigenPlus.TabIndex = 11;
            this.btnWijzigenPlus.Text = "+";
            this.btnWijzigenPlus.UseVisualStyleBackColor = true;
            this.btnWijzigenPlus.Click += new System.EventHandler(this.btnWijzigenPlus_Click);
            // 
            // btnWijzigenMin
            // 
            this.btnWijzigenMin.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnWijzigenMin.Location = new System.Drawing.Point(200, 52);
            this.btnWijzigenMin.Name = "btnWijzigenMin";
            this.btnWijzigenMin.Size = new System.Drawing.Size(49, 46);
            this.btnWijzigenMin.TabIndex = 12;
            this.btnWijzigenMin.Text = "-";
            this.btnWijzigenMin.UseVisualStyleBackColor = true;
            this.btnWijzigenMin.Click += new System.EventHandler(this.btnWijzigenMin_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnWijzigenMin);
            this.groupBox1.Controls.Add(this.nudWijzigen);
            this.groupBox1.Controls.Add(this.btnWijzigenPlus);
            this.groupBox1.Location = new System.Drawing.Point(992, 372);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 147);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selectie Wijzigen:";
            // 
            // btnSelectieVerwijderen
            // 
            this.btnSelectieVerwijderen.BackColor = System.Drawing.Color.DarkSalmon;
            this.btnSelectieVerwijderen.Location = new System.Drawing.Point(992, 537);
            this.btnSelectieVerwijderen.Name = "btnSelectieVerwijderen";
            this.btnSelectieVerwijderen.Size = new System.Drawing.Size(283, 43);
            this.btnSelectieVerwijderen.TabIndex = 14;
            this.btnSelectieVerwijderen.Text = "Selectie verwijderen";
            this.btnSelectieVerwijderen.UseVisualStyleBackColor = false;
            this.btnSelectieVerwijderen.Click += new System.EventHandler(this.btnSelectieVerwijderen_Click);
            // 
            // FrmBestellenAanmaken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1314, 719);
            this.Controls.Add(this.btnSelectieVerwijderen);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAanmaken);
            this.Controls.Add(this.nudHoeveelheid);
            this.Controls.Add(this.dgvOverzicht);
            this.Controls.Add(this.btnToevoegen);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvPrijslijstLeverancier);
            this.Controls.Add(this.lblPrijslijst);
            this.Name = "FrmBestellenAanmaken";
            this.Text = "Bestellen";
            this.Load += new System.EventHandler(this.FrmBestellenAanmaken_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrijslijstLeverancier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOverzicht)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoeveelheid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWijzigen)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblPrijslijst;
        private System.Windows.Forms.DataGridView dgvPrijslijstLeverancier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnToevoegen;
        private System.Windows.Forms.DataGridView dgvOverzicht;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hoeveel;
        private System.Windows.Forms.NumericUpDown nudHoeveelheid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Omschrijving;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_PrijsPerKg;
        private System.Windows.Forms.Button btnAanmaken;
        private System.Windows.Forms.NumericUpDown nudWijzigen;
        private System.Windows.Forms.Button btnWijzigenPlus;
        private System.Windows.Forms.Button btnWijzigenMin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSelectieVerwijderen;
    }
}