﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace Project_2_De_fervente_likker
{
    public partial class FrmKassa : Form
    {
        List<Klant> mijnDoorgekregenKlantLijst;

        //XML inlezen
        private const string XmlLezen = @"..\..\..\XML Files\Assortiment.xml";

        //KassaItems
        List<KassaItem> mijnKassa = new List<KassaItem>();
        KassaItem mykassaItem;

        //DateTime voor kassa
        DateTime mijnDatum = DateTime.Now;

        double totalePrijs = 0;
        double prijsPerStuk = 0;
        int aantal = 0;
        string artikelNaam;
        int aantalStock = 0;
        //int nieuwAantal = 0;

        public FrmKassa(List<Klant> klanten)
        {
            InitializeComponent();
            mijnDoorgekregenKlantLijst = klanten;
        }

        public FrmKassa()
        {
            InitializeComponent();
        }

        //enkele buttons kunnen niet aangeklikt worden als er niets wordt toegevoegd aan txtAantalStuks
        private void FrmKassa_Load(object sender, EventArgs e)
        {
            string overzicht;

            ButtonChangeNotEnable();

            ComboboxVullen();
            KlantenLijstToevoegen();
            cmbKlantGegevens.SelectedIndex = -1;
            cmbArtikel.SelectedIndex = -1;
            cmbPrijsPerStuk.SelectedIndex = -1;
            cmbArtikelID.SelectedIndex = -1;
            cmbCategorie.SelectedIndex = -1;

            overzicht = "AANTAL".PadLeft(8) + "ARTIKEL".PadLeft(18) + "PRIJS PER STUK".PadLeft(38) + "BEDRAG".PadLeft(20);
            lbKassaTitels.Items.Add(overzicht);
        }

        //Aan kassa toevoegen en meteen in lbKassaOverzicht en lbKassaBetalen
        //Niet vergeten aantal kan niet groter zijn dan stock --> op basis van stock
        //Prijs uit bestand halen stock
        private void btnAanKassaToevoegen_Click_1(object sender, EventArgs e)
        {
            KassaItem nieuw = null;

            if (nmudAantal.Value == 0) //|| nmudAantal.Value > aantalStock)
            {
                MessageBox.Show("Er zijn niet genoeg artikelen in stock.", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (!int.TryParse(nmudAantal.Text, out _) || !double.TryParse(cmbPrijsPerStuk.Text, out _))
            {
                MessageBox.Show("De waarde bij aantal stuks is niet numeriek.", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                aantal = Convert.ToInt32(nmudAantal.Text);
                artikelNaam = cmbArtikel.Text;
                prijsPerStuk = Convert.ToDouble(cmbPrijsPerStuk.Text);
                nieuw = new KassaItem(Convert.ToInt32(aantal), artikelNaam, prijsPerStuk);
                mijnKassa.Add(nieuw);
                ClearAllTextboxes();
                totalePrijs = 0;
                lbKassaOverzicht.Items.Clear();
                lbKassaBetalen.Items.Clear();

                foreach (KassaItem i in mijnKassa)
                {
                    lbKassaOverzicht.Items.Add(i.ToString());
                    totalePrijs += i.TotalePrijs;
                }
                lbKassaBetalen.Items.Add("TOTAAL: " + " €".PadLeft(73) + totalePrijs.ToString("0.00"));
                //NieuwAantal();
                ButtonChangeEnable();
            }
        }

        //Item aanpassen indien nodig 
        //Item terug weergeven in textboxes en combobox
        private void btnItemAanpassen_Click_1(object sender, EventArgs e)
        {
            try
            {
                int selectedItemIndex;
                double nieuwTotaal = 0;
                string nieuwTotaalString = "";

                if (!int.TryParse(nmudAantal.Text, out _))
                {
                    MessageBox.Show("De waarde bij aantal stuks is niet numeriek.", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (lbKassaOverzicht.SelectedIndex != -1)
                {
                    aantal = Convert.ToInt32(nmudAantal.Text);
                    artikelNaam = cmbArtikel.Text;
                    prijsPerStuk = Convert.ToDouble(cmbPrijsPerStuk.Text);

                    mykassaItem = new KassaItem(Convert.ToInt32(aantal), artikelNaam, Convert.ToDouble(prijsPerStuk));
                    selectedItemIndex = lbKassaOverzicht.SelectedIndex;
                    lbKassaOverzicht.Items.RemoveAt(selectedItemIndex);
                    lbKassaOverzicht.Items.Insert(selectedItemIndex, mykassaItem);

                    foreach (KassaItem j in mijnKassa)
                    {
                        nieuwTotaal += j.TotalePrijs;
                    }
                    nieuwTotaalString = "TOTAAL: " + " €".PadLeft(73) + mykassaItem.FormattedTotalePrijs().ToString();
                    lbKassaBetalen.Items.Clear();
                    lbKassaBetalen.Items.Add(nieuwTotaalString);
                    ClearAllTextboxes();
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Duid een rij aan.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (OverflowException)
            {
                MessageBox.Show("U heeft een te grote waarde ingegeven.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (FormatException)
            {
                MessageBox.Show("Geef de juiste waarde in", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception)
            {
                MessageBox.Show("Fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //Verwijderen van artikels
        private void btnItemVerwijderen_Click_1(object sender, EventArgs e)
        {
            int selectedItemIndex;
            string nieuwTotaal2String = "";

            MessageBox.Show("Bent u zeker dat u dit item wilt verwijderen?", "Let op", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Stop);

            if (lbKassaOverzicht.SelectedIndex != -1)
            {
                mykassaItem = new KassaItem(Convert.ToInt32(aantal), artikelNaam, Convert.ToDouble(prijsPerStuk));
                selectedItemIndex = lbKassaOverzicht.SelectedIndex;
                lbKassaOverzicht.Items.RemoveAt(selectedItemIndex);

                if (lbKassaOverzicht.Items.Count != 0)
                {
                    totalePrijs = totalePrijs - mykassaItem.TotalePrijs;

                    lbKassaBetalen.Items.Clear();
                    nieuwTotaal2String = "TOTAAL: " + " €".PadLeft(73) + Math.Round(totalePrijs, 2);
                    lbKassaBetalen.Items.Add(nieuwTotaal2String);
                    ClearAllTextboxes();
                }
                else if (lbKassaOverzicht.Items.Count <= 0)
                {
                    lbKassaBetalen.Items.Clear();
                    ClearAllTextboxes();
                }
            }
            else
            {
                MessageBox.Show("Selecteer een artikel die u wilt verwijderen.", "Let op!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //Terugbetalen aan klant --> cash
        private void btnTerugbetalen_Click_1(object sender, EventArgs e)
        {
            double gekregenGeld = Math.Round(Convert.ToDouble(txtGekregenGeld.Text), 2);
            double terugBetalen;
            string gegeven, geldTeruggeven, betaalmethode;

            if (string.IsNullOrEmpty(txtGekregenGeld.Text))
            {
                MessageBox.Show("De textbox bij gekregen geld mag niet leeg zijn.", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!double.TryParse(txtGekregenGeld.Text, out _))
            {
                MessageBox.Show("De waarde bij gekregen geld is niet numeriek", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                terugBetalen = totalePrijs - Math.Round(gekregenGeld, 2);

                betaalmethode = "Betaald met: cash";
                gegeven = "Gegeven: €" + gekregenGeld;
                geldTeruggeven = "Terugbetalen: €" + Math.Abs(terugBetalen).ToString("0.00");

                lbKassaBetalen.Items.Add(betaalmethode);
                lbKassaBetalen.Items.Add(gegeven).ToString("0.00");
                lbKassaBetalen.Items.Add(geldTeruggeven);
                txtGekregenGeld.Text = "";
                KlantGegevens();
                GeefDatum();
                btnKassaticketAfdrukken.Enabled = true;
            }
        }

        //Betaalmethode: bancontact of cash
        //Bancontact enkel afrekenen geen teruggave
        private void btnBancontact_Click_1(object sender, EventArgs e)
        {
            string bancontact, bancontactOk;

            bancontact = "Betaald met: bancontact";
            bancontactOk = "De betaling is in orde.";
            lbKassaBetalen.Items.Add(bancontact);
            lbKassaBetalen.Items.Add(bancontactOk);
            KlantGegevens();
            GeefDatum();
            btnKassaticketAfdrukken.Enabled = true;
        }

        //In txt-bestand plaatsen van gegevens
        //Eventueel nog kijken om dit fancy te maken --> als fancy gemaakt door te werken met een mooie vorm
        private void btnKassaticketAfdrukken_Click_1(object sender, EventArgs e)
        {

            if (!File.Exists("Kassaticket.txt"))
            {
                MessageBox.Show("Het bestand wordt aangemaakt.", "Aanmaken kassaticket", MessageBoxButtons.OK, MessageBoxIcon.Information);
                KassaTicket();
                ButtonChangeNotEnable();
            }
            else if (File.Exists("Kassaticket.txt"))
            {
                KassaTicket();
                MessageBox.Show("Het ticket wordt afgedrukt.", "Afdrukken kassaticket", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lbKassaOverzicht.Items.Clear();
                ButtonChangeNotEnable();
            }
            else if (lbKassaOverzicht.Items.Count <= 0)
            {
                MessageBox.Show("Er werden geen artikels toegevoegd aan de kassa.", "Foutmelding", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        //textboxes en combobox leegmaken
        private void ClearAllTextboxes()
        {
            nmudAantal.Text = "0";
            cmbArtikel.Text = null;
            cmbPrijsPerStuk.Text = null;
            cmbKlantGegevens.Text = null;
        }

        //buttons tonen
        private void ButtonChangeEnable()
        {
            btnBancontact.Enabled = true;
            btnItemAanpassen.Enabled = true;
            btnItemVerwijderen.Enabled = true;
            btnTerugbetalen.Enabled = true;
        }

        private void ButtonChangeNotEnable()
        {
            btnBancontact.Enabled = false;
            btnItemAanpassen.Enabled = false;
            btnItemVerwijderen.Enabled = false;
            btnTerugbetalen.Enabled = false;
            btnKassaticketAfdrukken.Enabled = false;
        }

        //alles van lbKassaOverzicht die is geselecteerd in txtbox en cmbbox weergeven 
        private void lbKassaOverzicht_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbKassaOverzicht.SelectedIndex != -1)
            {
                string lijn, artikel, aantal;

                lijn = lbKassaOverzicht.SelectedItem.ToString();
                aantal = lijn.Substring(0, 10).Trim();
                artikel = lijn.Substring(11, 20).Trim();
                nmudAantal.Text = aantal;
                cmbArtikel.Text = artikel;
            }
        }

        //in kassaticket plaatsen van items lbKassaOverzicht
        private void KassaTicket()
        {
            using (StreamWriter mijnKassaTicket = new StreamWriter("Kassaticket.txt"))
            {
                mijnKassaTicket.WriteLine("De Fervente Likker".PadLeft(55));
                mijnKassaTicket.WriteLine("Heeft u wat suiker nodig? Kom dan bij ons!".PadLeft(55));
                mijnKassaTicket.WriteLine("----------------------------------------------------------------------------------------");
                foreach (var item in lbKassaTitels.Items)
                {
                    mijnKassaTicket.WriteLine(item.ToString().PadRight(20));
                }
                foreach (var item in lbKassaOverzicht.Items)
                {
                    mijnKassaTicket.WriteLine(item.ToString().PadRight(20));
                }
                mijnKassaTicket.WriteLine(Environment.NewLine);
                mijnKassaTicket.WriteLine("----------------------------------------------------------------------------------------");
                mijnKassaTicket.WriteLine(Environment.NewLine);
                foreach (var item in lbKassaBetalen.Items)
                {
                    mijnKassaTicket.WriteLine(item.ToString().PadRight(20));
                }
                mijnKassaTicket.WriteLine(Environment.NewLine);
                mijnKassaTicket.WriteLine(Environment.NewLine);
                mijnKassaTicket.WriteLine("Bedankt en tot ziens".PadLeft(57));
                mijnKassaTicket.WriteLine(Environment.NewLine);
                mijnKassaTicket.WriteLine("OPENINGSUREN:".PadLeft(53));
                mijnKassaTicket.WriteLine("DI T.E.M. VRIJ VAN 09.00 UUR - 12.00 UUR EN VAN 13.00 UUR - 18.00 UUR".PadLeft(30));
                mijnKassaTicket.WriteLine("ZA VAN 10.00 UUR - 18.00 UUR DOORLOPEND  ".PadLeft(30));
                mijnKassaTicket.WriteLine("MA EN ZO GESLOTEN ".PadLeft(30));
                mijnKassaTicket.WriteLine(Environment.NewLine);
                mijnKassaTicket.WriteLine(Environment.NewLine);
            }
            NieuweKlant();
            mijnKassa.Clear();
        }

        //Datetime toevoegen
        public void GeefDatum()
        {
            string datum;
            datum = "Datum: " + mijnDatum;
            lbKassaBetalen.Items.Add(mijnDatum);
        }

        //Deze form sluiten
        private void btnKassaSluiten_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        //Gegevens van artikelen in combobox toevoegen --> als form laadt
        //Gegevens kunnen pas toegevoegd worden als de artikelen zijn aangemaakt --> stock
        //Prijs toevoegen wanneer artikel is geselecteerd --> uit stock halen van prijs
        private void ComboboxVullen()
        {
            try
            {
                using (XmlTextReader xmdatareader = new XmlTextReader(XmlLezen))
                {
                    DataSet ds = new DataSet();
                    ds.ReadXml(xmdatareader);
                    cmbArtikelID.DataSource = ds.Tables[0];
                    cmbArtikelID.DisplayMember = "ID";
                    cmbArtikel.DataSource = ds.Tables[0];
                    cmbArtikel.DisplayMember = "Artikelnaam";
                    cmbCategorie.DataSource = ds.Tables[0];
                    cmbCategorie.DisplayMember = "Categorie";
                    cmbPrijsPerStuk.DataSource = ds.Tables[0];
                    cmbPrijsPerStuk.DisplayMember = "Prijs";
                    //aantal bekijken in XML om te kijken of er nog genoeg in de stock zijn
                    while (xmdatareader.Read())
                    {
                        if (xmdatareader.NodeType == XmlNodeType.Element && xmdatareader.Name == "Aantal")
                        {
                            aantalStock = Convert.ToInt32(xmdatareader.ReadElementString());
                            break;
                        }
                    }
                }
            }
            catch (OverflowException)
            {
                MessageBox.Show("U heeft te veel gegevens ingegeven.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("Er staat nog niets in het assortiment.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception)
            {
                MessageBox.Show("Er liep iets fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        //klantenlijst aan combobox toevoegen
        private void KlantenLijstToevoegen()
        {
            foreach (var item in mijnDoorgekregenKlantLijst)
            {
                cmbKlantGegevens.Items.Add(item.ToString());
            }
        }

        //na afdrukken kassaticket wordt alles verwijderd
        private void NieuweKlant()
        {
            lbKassaOverzicht.Items.Clear();
            lbKassaBetalen.Items.Clear();
        }

        //klantgegevens indien er een klant in de klantenlijst staat --> gegevens weergeven in listbox en kassaticket
        //indien niet zal dit leeg gelaten worden
        private void KlantGegevens()
        {
            string klantGegevens = "";

            if (cmbKlantGegevens.SelectedIndex != -1)
            {
                klantGegevens = cmbKlantGegevens.Text;
                lbKassaBetalen.Items.Add(klantGegevens);
                cmbKlantGegevens.SelectedIndex = -1;
            }
            else if (cmbKlantGegevens.SelectedIndex == -1)
            {
                lbKassaBetalen.Items.Add("");
            }
        }

        //nieuw aantal wanneer er gekocht wordt dit aftrekken van het aantal in de stock
        //private void NieuwAantal()
        //{
        //    try
        //    {
        //        nieuwAantal = 0;
        //        nieuwAantal = aantalStock - aantal;
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.Load(XmlLezen);
        //        XmlNode node = xmlDoc.SelectSingleNode("Assortiment/Artikel/Aantal");
        //        node.Attributes[0].Value = nieuwAantal.ToString();
        //        xmlDoc.Save(XmlLezen);
        //    }
        //    catch (NullReferenceException)
        //    {
        //        MessageBox.Show("Het aantal kan niet kleiner of gelijk zijn aan 0.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //    catch (OverflowException)
        //    {
        //        MessageBox.Show("U heeft een te groot getal ingegeven.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //    catch (IndexOutOfRangeException)
        //    {
        //        MessageBox.Show("Er zijn nog geen artikelen aangemaakt.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //    catch (XmlException)
        //    {
        //        MessageBox.Show("Er ging iets fout bij het lezen van de XML.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //    catch (Exception)
        //    {
        //        MessageBox.Show("Er liep iets fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //}
    }
}
