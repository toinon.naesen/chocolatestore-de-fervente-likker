﻿
namespace Project_2_De_fervente_likker
{
    partial class FrmKassa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbArtikel = new System.Windows.Forms.ComboBox();
            this.lbKassaOverzicht = new System.Windows.Forms.ListBox();
            this.btnAanKassaToevoegen = new System.Windows.Forms.Button();
            this.btnItemAanpassen = new System.Windows.Forms.Button();
            this.btnItemVerwijderen = new System.Windows.Forms.Button();
            this.btnKassaticketAfdrukken = new System.Windows.Forms.Button();
            this.btnKassaSluiten = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBancontact = new System.Windows.Forms.Button();
            this.btnTerugbetalen = new System.Windows.Forms.Button();
            this.txtGekregenGeld = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbPrijsPerStuk = new System.Windows.Forms.ComboBox();
            this.lbKassaTitels = new System.Windows.Forms.ListBox();
            this.lbKassaBetalen = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbKlantGegevens = new System.Windows.Forms.ComboBox();
            this.cmbArtikelID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.nmudAantal = new System.Windows.Forms.NumericUpDown();
            this.cmbCategorie = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmudAantal)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Aantal stuks:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 109);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Artikel:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(323, 106);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Prijs per stuk:";
            // 
            // cmbArtikel
            // 
            this.cmbArtikel.FormattingEnabled = true;
            this.cmbArtikel.Location = new System.Drawing.Point(163, 104);
            this.cmbArtikel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbArtikel.Name = "cmbArtikel";
            this.cmbArtikel.Size = new System.Drawing.Size(143, 23);
            this.cmbArtikel.TabIndex = 5;
            // 
            // lbKassaOverzicht
            // 
            this.lbKassaOverzicht.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbKassaOverzicht.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbKassaOverzicht.FormattingEnabled = true;
            this.lbKassaOverzicht.ItemHeight = 15;
            this.lbKassaOverzicht.Location = new System.Drawing.Point(26, 160);
            this.lbKassaOverzicht.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lbKassaOverzicht.Name = "lbKassaOverzicht";
            this.lbKassaOverzicht.ScrollAlwaysVisible = true;
            this.lbKassaOverzicht.Size = new System.Drawing.Size(787, 240);
            this.lbKassaOverzicht.TabIndex = 6;
            this.lbKassaOverzicht.SelectedIndexChanged += new System.EventHandler(this.lbKassaOverzicht_SelectedIndexChanged);
            // 
            // btnAanKassaToevoegen
            // 
            this.btnAanKassaToevoegen.Location = new System.Drawing.Point(825, 136);
            this.btnAanKassaToevoegen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAanKassaToevoegen.Name = "btnAanKassaToevoegen";
            this.btnAanKassaToevoegen.Size = new System.Drawing.Size(140, 31);
            this.btnAanKassaToevoegen.TabIndex = 7;
            this.btnAanKassaToevoegen.Text = "Aan kassa toevoegen";
            this.btnAanKassaToevoegen.UseVisualStyleBackColor = true;
            this.btnAanKassaToevoegen.Click += new System.EventHandler(this.btnAanKassaToevoegen_Click_1);
            // 
            // btnItemAanpassen
            // 
            this.btnItemAanpassen.Location = new System.Drawing.Point(825, 191);
            this.btnItemAanpassen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnItemAanpassen.Name = "btnItemAanpassen";
            this.btnItemAanpassen.Size = new System.Drawing.Size(140, 31);
            this.btnItemAanpassen.TabIndex = 9;
            this.btnItemAanpassen.Text = "Item aanpassen";
            this.btnItemAanpassen.UseVisualStyleBackColor = true;
            this.btnItemAanpassen.Click += new System.EventHandler(this.btnItemAanpassen_Click_1);
            // 
            // btnItemVerwijderen
            // 
            this.btnItemVerwijderen.Location = new System.Drawing.Point(825, 251);
            this.btnItemVerwijderen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnItemVerwijderen.Name = "btnItemVerwijderen";
            this.btnItemVerwijderen.Size = new System.Drawing.Size(140, 31);
            this.btnItemVerwijderen.TabIndex = 10;
            this.btnItemVerwijderen.Text = "Item verwijderen";
            this.btnItemVerwijderen.UseVisualStyleBackColor = true;
            this.btnItemVerwijderen.Click += new System.EventHandler(this.btnItemVerwijderen_Click_1);
            // 
            // btnKassaticketAfdrukken
            // 
            this.btnKassaticketAfdrukken.Location = new System.Drawing.Point(825, 311);
            this.btnKassaticketAfdrukken.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnKassaticketAfdrukken.Name = "btnKassaticketAfdrukken";
            this.btnKassaticketAfdrukken.Size = new System.Drawing.Size(140, 31);
            this.btnKassaticketAfdrukken.TabIndex = 11;
            this.btnKassaticketAfdrukken.Text = "Kassaticket afdrukken";
            this.btnKassaticketAfdrukken.UseVisualStyleBackColor = true;
            this.btnKassaticketAfdrukken.Click += new System.EventHandler(this.btnKassaticketAfdrukken_Click_1);
            // 
            // btnKassaSluiten
            // 
            this.btnKassaSluiten.Location = new System.Drawing.Point(825, 446);
            this.btnKassaSluiten.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnKassaSluiten.Name = "btnKassaSluiten";
            this.btnKassaSluiten.Size = new System.Drawing.Size(140, 31);
            this.btnKassaSluiten.TabIndex = 12;
            this.btnKassaSluiten.Text = "Kassa sluiten";
            this.btnKassaSluiten.UseVisualStyleBackColor = true;
            this.btnKassaSluiten.Click += new System.EventHandler(this.btnKassaSluiten_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBancontact);
            this.groupBox1.Controls.Add(this.btnTerugbetalen);
            this.groupBox1.Controls.Add(this.txtGekregenGeld);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(570, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(440, 104);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Betaalmethode";
            // 
            // btnBancontact
            // 
            this.btnBancontact.Location = new System.Drawing.Point(298, 64);
            this.btnBancontact.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnBancontact.Name = "btnBancontact";
            this.btnBancontact.Size = new System.Drawing.Size(112, 29);
            this.btnBancontact.TabIndex = 3;
            this.btnBancontact.Text = "Bancontact";
            this.btnBancontact.UseVisualStyleBackColor = true;
            this.btnBancontact.Click += new System.EventHandler(this.btnBancontact_Click_1);
            // 
            // btnTerugbetalen
            // 
            this.btnTerugbetalen.Location = new System.Drawing.Point(298, 18);
            this.btnTerugbetalen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnTerugbetalen.Name = "btnTerugbetalen";
            this.btnTerugbetalen.Size = new System.Drawing.Size(112, 29);
            this.btnTerugbetalen.TabIndex = 2;
            this.btnTerugbetalen.Text = "Wisselgeld";
            this.btnTerugbetalen.UseVisualStyleBackColor = true;
            this.btnTerugbetalen.Click += new System.EventHandler(this.btnTerugbetalen_Click_1);
            // 
            // txtGekregenGeld
            // 
            this.txtGekregenGeld.Location = new System.Drawing.Point(133, 26);
            this.txtGekregenGeld.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtGekregenGeld.Name = "txtGekregenGeld";
            this.txtGekregenGeld.Size = new System.Drawing.Size(126, 23);
            this.txtGekregenGeld.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Gegeven bedrag:";
            // 
            // cmbPrijsPerStuk
            // 
            this.cmbPrijsPerStuk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cmbPrijsPerStuk.Enabled = false;
            this.cmbPrijsPerStuk.FormattingEnabled = true;
            this.cmbPrijsPerStuk.Location = new System.Drawing.Point(424, 104);
            this.cmbPrijsPerStuk.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbPrijsPerStuk.Name = "cmbPrijsPerStuk";
            this.cmbPrijsPerStuk.Size = new System.Drawing.Size(143, 21);
            this.cmbPrijsPerStuk.TabIndex = 16;
            // 
            // lbKassaTitels
            // 
            this.lbKassaTitels.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbKassaTitels.Enabled = false;
            this.lbKassaTitels.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbKassaTitels.FormattingEnabled = true;
            this.lbKassaTitels.ItemHeight = 15;
            this.lbKassaTitels.Location = new System.Drawing.Point(26, 136);
            this.lbKassaTitels.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lbKassaTitels.Name = "lbKassaTitels";
            this.lbKassaTitels.Size = new System.Drawing.Size(770, 15);
            this.lbKassaTitels.TabIndex = 17;
            // 
            // lbKassaBetalen
            // 
            this.lbKassaBetalen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbKassaBetalen.Enabled = false;
            this.lbKassaBetalen.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbKassaBetalen.FormattingEnabled = true;
            this.lbKassaBetalen.ItemHeight = 15;
            this.lbKassaBetalen.Location = new System.Drawing.Point(26, 396);
            this.lbKassaBetalen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lbKassaBetalen.Name = "lbKassaBetalen";
            this.lbKassaBetalen.Size = new System.Drawing.Size(770, 75);
            this.lbKassaBetalen.TabIndex = 18;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbKlantGegevens);
            this.groupBox2.Location = new System.Drawing.Point(334, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(210, 70);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Klantgegevens";
            // 
            // cmbKlantGegevens
            // 
            this.cmbKlantGegevens.FormattingEnabled = true;
            this.cmbKlantGegevens.Location = new System.Drawing.Point(29, 37);
            this.cmbKlantGegevens.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbKlantGegevens.Name = "cmbKlantGegevens";
            this.cmbKlantGegevens.Size = new System.Drawing.Size(143, 23);
            this.cmbKlantGegevens.TabIndex = 6;
            // 
            // cmbArtikelID
            // 
            this.cmbArtikelID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cmbArtikelID.Enabled = false;
            this.cmbArtikelID.FormattingEnabled = true;
            this.cmbArtikelID.Location = new System.Drawing.Point(163, 32);
            this.cmbArtikelID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbArtikelID.Name = "cmbArtikelID";
            this.cmbArtikelID.Size = new System.Drawing.Size(143, 21);
            this.cmbArtikelID.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 40);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 15);
            this.label5.TabIndex = 20;
            this.label5.Text = "Artikel ID:";
            // 
            // nmudAantal
            // 
            this.nmudAantal.Location = new System.Drawing.Point(163, 4);
            this.nmudAantal.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nmudAantal.Name = "nmudAantal";
            this.nmudAantal.Size = new System.Drawing.Size(141, 23);
            this.nmudAantal.TabIndex = 22;
            // 
            // cmbCategorie
            // 
            this.cmbCategorie.Enabled = false;
            this.cmbCategorie.FormattingEnabled = true;
            this.cmbCategorie.Location = new System.Drawing.Point(163, 70);
            this.cmbCategorie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbCategorie.Name = "cmbCategorie";
            this.cmbCategorie.Size = new System.Drawing.Size(143, 23);
            this.cmbCategorie.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 74);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "Categorie:";
            // 
            // FrmKassa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1024, 485);
            this.Controls.Add(this.cmbCategorie);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nmudAantal);
            this.Controls.Add(this.cmbArtikelID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lbKassaBetalen);
            this.Controls.Add(this.lbKassaTitels);
            this.Controls.Add(this.cmbPrijsPerStuk);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnKassaSluiten);
            this.Controls.Add(this.btnKassaticketAfdrukken);
            this.Controls.Add(this.btnItemVerwijderen);
            this.Controls.Add(this.btnItemAanpassen);
            this.Controls.Add(this.btnAanKassaToevoegen);
            this.Controls.Add(this.lbKassaOverzicht);
            this.Controls.Add(this.cmbArtikel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrmKassa";
            this.Text = "Kassa";
            this.Load += new System.EventHandler(this.FrmKassa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nmudAantal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbArtikel;
        private System.Windows.Forms.ListBox lbKassaOverzicht;
        private System.Windows.Forms.Button btnAanKassaToevoegen;
        private System.Windows.Forms.Button btnItemAanpassen;
        private System.Windows.Forms.Button btnItemVerwijderen;
        private System.Windows.Forms.Button btnKassaticketAfdrukken;
        private System.Windows.Forms.Button btnKassaSluiten;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBancontact;
        private System.Windows.Forms.Button btnTerugbetalen;
        private System.Windows.Forms.TextBox txtGekregenGeld;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbPrijsPerStuk;
        private System.Windows.Forms.ListBox lbKassaTitels;
        private System.Windows.Forms.ListBox lbKassaBetalen;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbKlantGegevens;
        private System.Windows.Forms.ComboBox cmbArtikelID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nmudAantal;
        private System.Windows.Forms.ComboBox cmbCategorie;
        private System.Windows.Forms.Label label6;
    }
}