﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace Project_2_De_fervente_likker
{
    public partial class FrmLogin : Form
    {
        FrmHoofdMenu hoofFormTemp;
        
        public FrmLogin()
        {
            InitializeComponent();
        }
        List<Klant> mijnDoorgekregenKlantenLijst;
        List<Personeel> mijnDoorgekregenPersoneelsLijst;
        List<Administrator> mijnDoorgekregenAdminLijst;
        int accesIdentifier;
        public FrmLogin(FrmHoofdMenu localForm, List<Klant> mijnKlant, List<Personeel> mijnPersoneel, List<Administrator> mijnAdmin, int acces)   //openMenuSelectie(new FrmLogin(this, MijnKlant, MijnPersoneel, MijnAdmin));
        {
            InitializeComponent();
            hoofFormTemp = localForm;
            mijnDoorgekregenPersoneelsLijst = mijnPersoneel;
            mijnDoorgekregenAdminLijst = mijnAdmin;
            mijnDoorgekregenKlantenLijst = mijnKlant;
            accesIdentifier = acces;
        }
        
        private void btnInloggen_Click(object sender, System.EventArgs e)
        {
            //mijnDoorgekregenKlantenLijst.Any(klant => klant.Gebruikersnaam == txtGebruikersnaam.Text);
            if (mijnDoorgekregenKlantenLijst.Any(klant => klant.Gebruikersnaam == txtGebruikersnaam.Text && klant.Passwoord == txtPasswoord.Text))
            {
                accesIdentifier = 2;
            }
            else if (mijnDoorgekregenPersoneelsLijst.Any(personeel => personeel.Gebruikersnaam == txtGebruikersnaam.Text && personeel.Passwoord == txtPasswoord.Text))
            {
                accesIdentifier = 1;
            }
            else if (mijnDoorgekregenAdminLijst.Any(admin => admin.Gebruikersnaam == txtGebruikersnaam.Text && admin.Passwoord == txtPasswoord.Text))
            {
                accesIdentifier = 0;
            }
            else
            {
                MessageBox.Show("Geen account gevonden met deze gebruikersnaam en wachtwoord.", "Geen gebruiker gevonden.", MessageBoxButtons.OK);

            }
            if (accesIdentifier >= 0)
            {
                hoofFormTemp.EnableKnoppen();
                this.Close();
            }

            //var admin = mijnDoorgekregenAdminLijst.FirstOrDefault(admin => admin.Gebruikersnaam == "Bala");




            
        }
    }
}
