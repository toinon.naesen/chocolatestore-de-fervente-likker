﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_2_De_fervente_likker
{
    public partial class FrmWijzigenKlant : Form
    {
        List<Klant> mijnDoorgekregenKlantenlijst;
        public int KlantID;
        Klant KlantWijzig;
        public string[] arrProvincies = new string[] { "Antwerpen", "Brussel", "Oost-Vlaanderen", "West-Vlaanderen", "Limburg", "Vlaams-Brabant", "Waals-Brabant", "Luik", "Henegouwen", "Namen", "Luxemburg" };

        public FrmWijzigenKlant()
        {
            InitializeComponent();
        }
        public FrmWijzigenKlant(List<Klant> localKlantenlijst, int KlantId)
        {
            InitializeComponent();
            mijnDoorgekregenKlantenlijst = localKlantenlijst;
            KlantID = KlantId;
        }
        private void btnOpslaan_Click(object sender, EventArgs e)
        {
            try
            {
                int postcode = 0, huisNummer = 0;
                double gsm = 0, vastTel = 0;
                if (txtGSMnummer.Text.Contains("+"))
                {
                    txtGSMnummer.Text.Remove(0, 1);
                }
                if ((string.IsNullOrEmpty(txtVoornaam.Text) && string.IsNullOrEmpty(txtAchternaam.Text) && string.IsNullOrEmpty(txtEmail.Text) && string.IsNullOrEmpty(txtGemeente.Text) && string.IsNullOrEmpty(txtStraatnaam.Text) && string.IsNullOrEmpty(txtGSMnummer.Text) && string.IsNullOrEmpty(txtHuisnummer.Text) && string.IsNullOrEmpty(txtPostcode.Text) && string.IsNullOrEmpty(txtVasteTelefoon.Text)))
                {
                    MessageBox.Show("Voornaam, achternaam, email, gemeente, straatnaam, Gsmnummer, huisnummer, postcode of vaste telefoon niet ingevuld.", "Tekstbox leeg", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!(double.TryParse(txtGSMnummer.Text, out gsm) && int.TryParse(txtHuisnummer.Text, out huisNummer) && int.TryParse(txtPostcode.Text, out postcode) && double.TryParse(txtVasteTelefoon.Text, out vastTel)))
                {
                    MessageBox.Show("GSM, huisnummer, postcode of vaste telefoon Geen geldig nummer ingegeven.", "Geen compleet nummer detected.", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if ((string.IsNullOrEmpty(txtGebruikersnaam.Text) && string.IsNullOrEmpty(txtWachtwoord.Text)))
                {
                    MessageBox.Show("Gelieve de inloggegevens in te vullen voor aanmaken acc.", "gebruikersnaam of wachtwoord vergeten ingeven.", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (!(txtEmail.Text.Contains('@') && txtEmail.Text.Contains('.')))
                {
                    MessageBox.Show("Dat is geen geldig email adres", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (txtVoornaam.Text.Any(char.IsDigit) || txtAchternaam.Text.Any(char.IsDigit))
                {
                    MessageBox.Show("Voornaam of achternaam kunnen geen numerieke input hebben. Tenzij je vader Elon Musk is.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {
                    KlantWijzig.Achternaam = txtAchternaam.Text;
                    KlantWijzig.Email = txtEmail.Text;
                    KlantWijzig.Gebruikersnaam = txtGebruikersnaam.Text;
                    KlantWijzig.Gemeente = txtGemeente.Text;
                    KlantWijzig.Gsmnummer = gsm;
                    KlantWijzig.Huisnummer = huisNummer.ToString();
                    KlantWijzig.Huistelefoon = vastTel;
                    KlantWijzig.Passwoord = txtWachtwoord.Text;
                    KlantWijzig.Postcode = postcode.ToString();
                    KlantWijzig.Provincie = cmbProvincie.SelectedItem.ToString();
                    KlantWijzig.Straatnaam = txtStraatnaam.Text;
                    KlantWijzig.Voornaam = txtVoornaam.Text;
                }
                this.Close();
            }
            catch (OverflowException)
            {
                MessageBox.Show("U heeft een te grote waarde ingegeven.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (FormatException)
            {
                MessageBox.Show("Geef de juiste waarde in", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception)
            {
                MessageBox.Show("Fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            


        }

        private void btnsluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmWijzigen_Load(object sender, EventArgs e)
        {
            Array.Sort(arrProvincies);
            cmbProvincie.Items.AddRange(arrProvincies);
            KlantWijzig = mijnDoorgekregenKlantenlijst[KlantID];
            txtVoornaam.Text = KlantWijzig.Voornaam;
            txtAchternaam.Text = KlantWijzig.Achternaam;
            txtEmail.Text = KlantWijzig.Email;
            txtGebruikersnaam.Text = KlantWijzig.Gebruikersnaam;
            txtGemeente.Text = KlantWijzig.Gemeente;
            txtGSMnummer.Text = KlantWijzig.Gsmnummer.ToString();
            txtHuisnummer.Text = KlantWijzig.Huisnummer;
            txtPostcode.Text = KlantWijzig.Postcode;
            txtStraatnaam.Text = KlantWijzig.Straatnaam;
            txtVasteTelefoon.Text = KlantWijzig.Huistelefoon.ToString();
            txtWachtwoord.Text = KlantWijzig.Passwoord;
            cmbProvincie.Text = KlantWijzig.Provincie;
        }
    }
}
