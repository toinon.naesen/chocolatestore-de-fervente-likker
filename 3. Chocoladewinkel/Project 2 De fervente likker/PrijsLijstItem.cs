﻿namespace Project_2_De_fervente_likker
{
    public class PrijsLijstItem
    {
        /*      Deze klasse dient als tussenstap bij het lezen van de prijslijst.xlm
         *             --> Er zullen objecten van deze klasse aangemaakt worden op basis van de gegevens die hieruit komen
         *                    --> Deze kunnen we dan gebruiken om intern oa bestellingen op te maken
         * 
         * 
         *      Voor de moment heeft deze klasse niets te maken met onze interne product.cs
         *              Ik zou later nog eens na kunnen denken of deze een rol zou kunnen uitmaken van deze hierearchie (jaja spelling) maar voorlopig hou ik deze appart om problemen te vermijden
         * 
         */


        //Declaraties (houdbaarheidsdatum zullen we niet gebruiken omdat we onze eigen toekennen)

        private string _id;
        private string _omschrijving;
        private double _prijsPerKg;


        //Constructors

        public PrijsLijstItem(string iD, string omschrijving, double prijsperKg)
        {
            ID = iD;
            Omschrijving = omschrijving;
            PrijsperKg = prijsperKg;
        }

        //Propperties

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Omschrijving
        {
            get { return _omschrijving; }
            set { _omschrijving = value; }
        }

        public double PrijsperKg
        {
            get { return _prijsPerKg; }
            set { _prijsPerKg = value; }
        }

        //Methoden

        public override string ToString()
        {
            return $"ID: {ID}".PadRight(10) + $"Naam: {Omschrijving}".PadRight(30) + $"Prijs: {PrijsperKg:F2} €".PadRight(10);
        }

        public string[] ToStringArray() //Deze methode dient om deze objectten makkelijker in een listview te krijgen, heb deze gemaakt tijdens experimenteren. mss nog eens naar kijken
        {
            string[] arrLocalStringArray = new string[3];

            arrLocalStringArray[0] = ID;
            arrLocalStringArray[1] = Omschrijving;
            arrLocalStringArray[2] = $"{PrijsperKg:F2}";

            return arrLocalStringArray;
        }
    }
}
