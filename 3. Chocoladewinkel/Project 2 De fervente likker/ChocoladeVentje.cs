﻿using System;

namespace Project_2_De_fervente_likker
{
    public class ChocoladeVentje : Product
    {
        //Declaraties



        //Constructors   
        public ChocoladeVentje()
        {

        }
        public ChocoladeVentje(double prijs, string soortChocolade, double gewicht, string houdbaarheid, string beschrijving, int id) : base(prijs, soortChocolade, gewicht, houdbaarheid, beschrijving, id)
        {


        }


        //Properties

        //methode
        public override DateTime HoudbaarheidsDatum()
        {
            DateTime dateTimeNow = DateTime.Now;
            DateTime todayPlusOneYear = dateTimeNow.Date.AddDays(365);
            return todayPlusOneYear;
        }

        public override string ToString()
        {
            return HoudbaarheidsDatum().ToString("dd-MM-yyyy");
        }

        public override bool Equals(object obj)
        {
            bool check = true;

            if (obj == null || this.GetType() != obj.GetType())
            {
                check = false;
            }
            else
            {
                ChocoladeVentje c = (ChocoladeVentje)obj;
                if (this.Beschrijving != c.Beschrijving || this.Prijs != c.Prijs || this.SoortChocolade != c.SoortChocolade || this.Gewicht != c.Gewicht || this.Houdbaarheid != c.Houdbaarheid || this.ID != c.ID)
                {
                    check = false;
                }
            }
            return check;
        }
    }
}
