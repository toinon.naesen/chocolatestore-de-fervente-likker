﻿
namespace Project_2_De_fervente_likker
{
    partial class FrmAssortiment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStockItemVerwijderen = new System.Windows.Forms.Button();
            this.btnStockItemUpdaten = new System.Windows.Forms.Button();
            this.btnStockSluiten = new System.Windows.Forms.Button();
            this.btnStockItemToevoegen = new System.Windows.Forms.Button();
            this.cmbCategorie = new System.Windows.Forms.ComboBox();
            this.lbCategorie = new System.Windows.Forms.Label();
            this.dgvStock = new System.Windows.Forms.DataGridView();
            this.cmbSoortChocolade = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGewichtInKg = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPrijsPerStuk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtArtikelNaam = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbtnSoortChocolade = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.rdbtnArtikelNaam = new System.Windows.Forms.RadioButton();
            this.cmbZoekOpSoortChocolade = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnZoekArtikel = new System.Windows.Forms.Button();
            this.txtZoekArtikelOpNaam = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnToonStock = new System.Windows.Forms.Button();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Categorie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Artikelnaam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chocoladesoort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gewicht = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Houdbaarheid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnToevoegenAanStock = new System.Windows.Forms.Button();
            this.txtAantalInStock = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStockItemVerwijderen
            // 
            this.btnStockItemVerwijderen.Location = new System.Drawing.Point(858, 137);
            this.btnStockItemVerwijderen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStockItemVerwijderen.Name = "btnStockItemVerwijderen";
            this.btnStockItemVerwijderen.Size = new System.Drawing.Size(190, 31);
            this.btnStockItemVerwijderen.TabIndex = 8;
            this.btnStockItemVerwijderen.Text = "Verwijderen";
            this.btnStockItemVerwijderen.UseVisualStyleBackColor = true;
            this.btnStockItemVerwijderen.Click += new System.EventHandler(this.btnStockItemVerwijderen_Click);
            // 
            // btnStockItemUpdaten
            // 
            this.btnStockItemUpdaten.Location = new System.Drawing.Point(402, 137);
            this.btnStockItemUpdaten.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStockItemUpdaten.Name = "btnStockItemUpdaten";
            this.btnStockItemUpdaten.Size = new System.Drawing.Size(190, 31);
            this.btnStockItemUpdaten.TabIndex = 9;
            this.btnStockItemUpdaten.Text = "Wijzigen";
            this.btnStockItemUpdaten.UseVisualStyleBackColor = true;
            this.btnStockItemUpdaten.Click += new System.EventHandler(this.btnStockItemUpdaten_Click);
            // 
            // btnStockSluiten
            // 
            this.btnStockSluiten.Location = new System.Drawing.Point(909, 583);
            this.btnStockSluiten.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStockSluiten.Name = "btnStockSluiten";
            this.btnStockSluiten.Size = new System.Drawing.Size(190, 31);
            this.btnStockSluiten.TabIndex = 10;
            this.btnStockSluiten.Text = "Sluiten";
            this.btnStockSluiten.UseVisualStyleBackColor = true;
            this.btnStockSluiten.Click += new System.EventHandler(this.btnStockSluiten_Click);
            // 
            // btnStockItemToevoegen
            // 
            this.btnStockItemToevoegen.Location = new System.Drawing.Point(26, 137);
            this.btnStockItemToevoegen.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStockItemToevoegen.Name = "btnStockItemToevoegen";
            this.btnStockItemToevoegen.Size = new System.Drawing.Size(190, 31);
            this.btnStockItemToevoegen.TabIndex = 11;
            this.btnStockItemToevoegen.Text = "Toevoegen";
            this.btnStockItemToevoegen.UseVisualStyleBackColor = true;
            this.btnStockItemToevoegen.Click += new System.EventHandler(this.btnStockItemToevoegen_Click);
            // 
            // cmbCategorie
            // 
            this.cmbCategorie.FormattingEnabled = true;
            this.cmbCategorie.Location = new System.Drawing.Point(124, 34);
            this.cmbCategorie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbCategorie.Name = "cmbCategorie";
            this.cmbCategorie.Size = new System.Drawing.Size(213, 23);
            this.cmbCategorie.TabIndex = 23;
            // 
            // lbCategorie
            // 
            this.lbCategorie.AutoSize = true;
            this.lbCategorie.Location = new System.Drawing.Point(6, 34);
            this.lbCategorie.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbCategorie.Name = "lbCategorie";
            this.lbCategorie.Size = new System.Drawing.Size(61, 15);
            this.lbCategorie.TabIndex = 22;
            this.lbCategorie.Text = "Categorie:";
            // 
            // dgvStock
            // 
            this.dgvStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStock.Location = new System.Drawing.Point(22, 312);
            this.dgvStock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvStock.MultiSelect = false;
            this.dgvStock.Name = "dgvStock";
            this.dgvStock.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvStock.RowHeadersWidth = 62;
            this.dgvStock.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvStock.RowTemplate.Height = 33;
            this.dgvStock.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStock.Size = new System.Drawing.Size(860, 302);
            this.dgvStock.TabIndex = 24;
            this.dgvStock.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStock_CellContentClick);
            // 
            // cmbSoortChocolade
            // 
            this.cmbSoortChocolade.FormattingEnabled = true;
            this.cmbSoortChocolade.Location = new System.Drawing.Point(522, 36);
            this.cmbSoortChocolade.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbSoortChocolade.Name = "cmbSoortChocolade";
            this.cmbSoortChocolade.Size = new System.Drawing.Size(213, 23);
            this.cmbSoortChocolade.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(387, 36);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 15);
            this.label4.TabIndex = 31;
            this.label4.Text = "Soort chocolade:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 82);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 15);
            this.label3.TabIndex = 30;
            this.label3.Text = "Gewicht in g:";
            // 
            // txtGewichtInKg
            // 
            this.txtGewichtInKg.Location = new System.Drawing.Point(522, 80);
            this.txtGewichtInKg.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtGewichtInKg.Name = "txtGewichtInKg";
            this.txtGewichtInKg.Size = new System.Drawing.Size(213, 23);
            this.txtGewichtInKg.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(766, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 15);
            this.label2.TabIndex = 28;
            this.label2.Text = "Prijs per stuk:";
            // 
            // txtPrijsPerStuk
            // 
            this.txtPrijsPerStuk.Location = new System.Drawing.Point(881, 36);
            this.txtPrijsPerStuk.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPrijsPerStuk.Name = "txtPrijsPerStuk";
            this.txtPrijsPerStuk.Size = new System.Drawing.Size(195, 23);
            this.txtPrijsPerStuk.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 26;
            this.label1.Text = "Naam";
            // 
            // txtArtikelNaam
            // 
            this.txtArtikelNaam.Location = new System.Drawing.Point(124, 78);
            this.txtArtikelNaam.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtArtikelNaam.Name = "txtArtikelNaam";
            this.txtArtikelNaam.Size = new System.Drawing.Size(213, 23);
            this.txtArtikelNaam.TabIndex = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtGewichtInKg);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbSoortChocolade);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtPrijsPerStuk);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtArtikelNaam);
            this.groupBox1.Controls.Add(this.lbCategorie);
            this.groupBox1.Controls.Add(this.cmbCategorie);
            this.groupBox1.Controls.Add(this.btnStockItemToevoegen);
            this.groupBox1.Controls.Add(this.btnStockItemUpdaten);
            this.groupBox1.Controls.Add(this.btnStockItemVerwijderen);
            this.groupBox1.Location = new System.Drawing.Point(22, 8);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(1088, 187);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Artikel";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(881, 82);
            this.txtID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(195, 23);
            this.txtID.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(766, 83);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 15);
            this.label8.TabIndex = 36;
            this.label8.Text = "ID:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbtnSoortChocolade);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.rdbtnArtikelNaam);
            this.groupBox2.Controls.Add(this.cmbZoekOpSoortChocolade);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnZoekArtikel);
            this.groupBox2.Controls.Add(this.txtZoekArtikelOpNaam);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(22, 207);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(1088, 90);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Zoek artikel in stock";
            // 
            // rdbtnSoortChocolade
            // 
            this.rdbtnSoortChocolade.AutoSize = true;
            this.rdbtnSoortChocolade.Location = new System.Drawing.Point(99, 60);
            this.rdbtnSoortChocolade.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rdbtnSoortChocolade.Name = "rdbtnSoortChocolade";
            this.rdbtnSoortChocolade.Size = new System.Drawing.Size(111, 19);
            this.rdbtnSoortChocolade.TabIndex = 37;
            this.rdbtnSoortChocolade.TabStop = true;
            this.rdbtnSoortChocolade.Text = "Soort chocolade";
            this.rdbtnSoortChocolade.UseVisualStyleBackColor = true;
            this.rdbtnSoortChocolade.CheckedChanged += new System.EventHandler(this.rdbtnSoortChocolade_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 29);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 15);
            this.label7.TabIndex = 36;
            this.label7.Text = "Zoek op:";
            // 
            // rdbtnArtikelNaam
            // 
            this.rdbtnArtikelNaam.AutoSize = true;
            this.rdbtnArtikelNaam.Location = new System.Drawing.Point(99, 29);
            this.rdbtnArtikelNaam.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rdbtnArtikelNaam.Name = "rdbtnArtikelNaam";
            this.rdbtnArtikelNaam.Size = new System.Drawing.Size(57, 19);
            this.rdbtnArtikelNaam.TabIndex = 35;
            this.rdbtnArtikelNaam.TabStop = true;
            this.rdbtnArtikelNaam.Text = "Naam";
            this.rdbtnArtikelNaam.UseVisualStyleBackColor = true;
            this.rdbtnArtikelNaam.CheckedChanged += new System.EventHandler(this.rdbtnArtikelNaam_CheckedChanged);
            // 
            // cmbZoekOpSoortChocolade
            // 
            this.cmbZoekOpSoortChocolade.FormattingEnabled = true;
            this.cmbZoekOpSoortChocolade.Location = new System.Drawing.Point(502, 58);
            this.cmbZoekOpSoortChocolade.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbZoekOpSoortChocolade.Name = "cmbZoekOpSoortChocolade";
            this.cmbZoekOpSoortChocolade.Size = new System.Drawing.Size(213, 23);
            this.cmbZoekOpSoortChocolade.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(368, 58);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 15);
            this.label6.TabIndex = 33;
            this.label6.Text = "Soort chocolade:";
            // 
            // btnZoekArtikel
            // 
            this.btnZoekArtikel.Location = new System.Drawing.Point(841, 34);
            this.btnZoekArtikel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnZoekArtikel.Name = "btnZoekArtikel";
            this.btnZoekArtikel.Size = new System.Drawing.Size(190, 31);
            this.btnZoekArtikel.TabIndex = 29;
            this.btnZoekArtikel.Text = "Zoeken";
            this.btnZoekArtikel.UseVisualStyleBackColor = true;
            this.btnZoekArtikel.Click += new System.EventHandler(this.btnZoekArtikel_Click);
            // 
            // txtZoekArtikelOpNaam
            // 
            this.txtZoekArtikelOpNaam.Location = new System.Drawing.Point(502, 25);
            this.txtZoekArtikelOpNaam.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtZoekArtikelOpNaam.Name = "txtZoekArtikelOpNaam";
            this.txtZoekArtikelOpNaam.Size = new System.Drawing.Size(213, 23);
            this.txtZoekArtikelOpNaam.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(387, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 15);
            this.label5.TabIndex = 27;
            this.label5.Text = "Naam";
            // 
            // btnToonStock
            // 
            this.btnToonStock.Location = new System.Drawing.Point(10, 31);
            this.btnToonStock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnToonStock.Name = "btnToonStock";
            this.btnToonStock.Size = new System.Drawing.Size(201, 31);
            this.btnToonStock.TabIndex = 42;
            this.btnToonStock.Text = "Tonen";
            this.btnToonStock.UseVisualStyleBackColor = true;
            this.btnToonStock.Click += new System.EventHandler(this.btnToonStock_Click);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 8;
            this.ID.Name = "ID";
            this.ID.Width = 150;
            // 
            // Categorie
            // 
            this.Categorie.HeaderText = "Categorie";
            this.Categorie.MinimumWidth = 8;
            this.Categorie.Name = "Categorie";
            this.Categorie.Width = 150;
            // 
            // Artikelnaam
            // 
            this.Artikelnaam.HeaderText = "Artikelnaam";
            this.Artikelnaam.MinimumWidth = 8;
            this.Artikelnaam.Name = "Artikelnaam";
            this.Artikelnaam.Width = 150;
            // 
            // Chocoladesoort
            // 
            this.Chocoladesoort.HeaderText = "Chocoladesoort";
            this.Chocoladesoort.MinimumWidth = 8;
            this.Chocoladesoort.Name = "Chocoladesoort";
            this.Chocoladesoort.Width = 150;
            // 
            // Gewicht
            // 
            this.Gewicht.HeaderText = "Gewicht per stuk (g)";
            this.Gewicht.MinimumWidth = 8;
            this.Gewicht.Name = "Gewicht";
            this.Gewicht.Width = 150;
            // 
            // Houdbaarheid
            // 
            this.Houdbaarheid.HeaderText = "Houdbaarheid";
            this.Houdbaarheid.MinimumWidth = 8;
            this.Houdbaarheid.Name = "Houdbaarheid";
            this.Houdbaarheid.Width = 150;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnToonStock);
            this.groupBox4.Location = new System.Drawing.Point(887, 312);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Size = new System.Drawing.Size(223, 85);
            this.groupBox4.TabIndex = 43;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Stock";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 38);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 15);
            this.label9.TabIndex = 40;
            this.label9.Text = "Aantal toevoegen aan stock:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnToevoegenAanStock);
            this.groupBox3.Controls.Add(this.txtAantalInStock);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(897, 408);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(223, 163);
            this.groupBox3.TabIndex = 42;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Aantal";
            // 
            // btnToevoegenAanStock
            // 
            this.btnToevoegenAanStock.Location = new System.Drawing.Point(13, 124);
            this.btnToevoegenAanStock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnToevoegenAanStock.Name = "btnToevoegenAanStock";
            this.btnToevoegenAanStock.Size = new System.Drawing.Size(190, 28);
            this.btnToevoegenAanStock.TabIndex = 41;
            this.btnToevoegenAanStock.Text = "Toevoegen";
            this.btnToevoegenAanStock.UseVisualStyleBackColor = true;
            this.btnToevoegenAanStock.Click += new System.EventHandler(this.btnToevoegenAanStock_Click_2);
            // 
            // txtAantalInStock
            // 
            this.txtAantalInStock.Location = new System.Drawing.Point(8, 77);
            this.txtAantalInStock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAantalInStock.Name = "txtAantalInStock";
            this.txtAantalInStock.Size = new System.Drawing.Size(196, 23);
            this.txtAantalInStock.TabIndex = 39;
            // 
            // FrmAssortiment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1143, 630);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvStock);
            this.Controls.Add(this.btnStockSluiten);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Name = "FrmAssortiment";
            this.Text = "Assortiment";
            this.Load += new System.EventHandler(this.FrmAssortiment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnStockItemVerwijderen;
        private System.Windows.Forms.Button btnStockItemUpdaten;
        private System.Windows.Forms.Button btnStockSluiten;
        private System.Windows.Forms.Button btnStockItemToevoegen;
        private System.Windows.Forms.ComboBox cmbCategorie;
        private System.Windows.Forms.Label lbCategorie;
        private System.Windows.Forms.DataGridView dgvStock;
        private System.Windows.Forms.ComboBox cmbSoortChocolade;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGewichtInKg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPrijsPerStuk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtArtikelNaam;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rdbtnArtikelNaam;
        private System.Windows.Forms.ComboBox cmbZoekOpSoortChocolade;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnZoekArtikel;
        private System.Windows.Forms.TextBox txtZoekArtikelOpNaam;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rdbtnSoortChocolade;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnToonStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Categorie;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artikelnaam;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chocoladesoort;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gewicht;
        private System.Windows.Forms.DataGridViewTextBoxColumn Houdbaarheid;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnToevoegenAanStock;
        private System.Windows.Forms.TextBox txtAantalInStock;
    }
}