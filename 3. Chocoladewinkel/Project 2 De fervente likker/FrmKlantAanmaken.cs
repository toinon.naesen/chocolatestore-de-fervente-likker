﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_2_De_fervente_likker
{
    public partial class FrmKlantAanmaken : Form
    {
        //Declarties
        List<Klant> mijnDoorgekregenKlantenlijst;
        Klant newKlant;
        public FrmKlantAanmaken()
        {
            InitializeComponent();
        }

        public FrmKlantAanmaken(List<Klant> localKlantenlijst)
        {
            InitializeComponent();

            mijnDoorgekregenKlantenlijst = localKlantenlijst;
        }

        public string[] arrProvincies = new string[] { "Antwerpen", "Brussel", "Oost-Vlaanderen", "West-Vlaanderen","Limburg", "Vlaams-Brabant", "Waals-Brabant", "Luik", "Henegouwen", "Namen", "Luxemburg" };
        //List<Klant> mijnKlantLijst = new List<Klant>();
        public int klantid = 1;
        private void btnAanmaken_Click(object sender, EventArgs e)
        {
            try
            {
                int postcode = 0, huisNummer = 0;
                double gsm = 0, vastTel = 0;
                if (txtGSMnummer.Text.Contains("+"))
                {
                    txtGSMnummer.Text.Remove(0, 1);
                }
                if ((string.IsNullOrEmpty(txtVoornaam.Text) && string.IsNullOrEmpty(txtAchternaam.Text) && string.IsNullOrEmpty(txtEmail.Text) && string.IsNullOrEmpty(txtGemeente.Text) && string.IsNullOrEmpty(txtStraatnaam.Text) && string.IsNullOrEmpty(txtGSMnummer.Text) && string.IsNullOrEmpty(txtHuisnummer.Text) && string.IsNullOrEmpty(txtPostcode.Text) && string.IsNullOrEmpty(txtVasteTelefoon.Text)))
                {
                    MessageBox.Show("Voornaam, achternaam, email, gemeente, straatnaam, Gsmnummer, huisnummer, postcode of vaste telefoon niet ingevuld.", "Tekstbox leeg", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!(double.TryParse(txtGSMnummer.Text, out gsm) && int.TryParse(txtHuisnummer.Text, out huisNummer) && int.TryParse(txtPostcode.Text, out postcode) && double.TryParse(txtVasteTelefoon.Text, out vastTel)))
                {
                    MessageBox.Show("GSM, huisnummer, postcode of vaste telefoon Geen geldig nummer ingegeven.", "Geen compleet nummer detected.", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if ((string.IsNullOrEmpty(txtGebruikersnaam.Text) && string.IsNullOrEmpty(txtWachtwoord.Text)))
                {
                    MessageBox.Show("Gelieve de inloggegevens in te vullen voor aanmaken acc.", "gebruikersnaam of wachtwoord vergeten ingeven.", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (!(txtEmail.Text.Contains('@') && txtEmail.Text.Contains('.')))
                {
                    MessageBox.Show("Dat is geen geldig email adres", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (txtVoornaam.Text.Any(char.IsDigit) || txtAchternaam.Text.Any(char.IsDigit))
                {
                    MessageBox.Show("Voornaam of achternaam kunnen geen numerieke input hebben. Tenzij je vader Elon Musk is.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {
                    newKlant = new Klant(txtVoornaam.Text, txtAchternaam.Text, txtStraatnaam.Text, huisNummer.ToString(), postcode.ToString(), txtGemeente.Text, cmbProvincie.SelectedItem.ToString(), gsm, vastTel, txtEmail.Text, klantid, txtGebruikersnaam.Text, txtWachtwoord.Text);
                    mijnDoorgekregenKlantenlijst.Add(newKlant);
                    klantid++;
                }
                this.Close();
            }
            catch (OverflowException)
            {
                MessageBox.Show("U heeft een te grote waarde ingegeven.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (FormatException)
            {
                MessageBox.Show("Geef de juiste waarde in", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch (Exception)
            {
                MessageBox.Show("Fout.", "Let op", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            


        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            txtAchternaam.Text = "";
            txtEmail.Text = "";
            txtGebruikersnaam.Text = "";
            txtGemeente.Text = "";
            txtGSMnummer.Text = "";
            txtHuisnummer.Text = "";
            txtPostcode.Text = "";
            txtStraatnaam.Text = "";
            txtVasteTelefoon.Text = "";
            txtVoornaam.Text = "";
            txtWachtwoord.Text = "";
            this.Close();
        }

        private void FrmKlanten_Load(object sender, EventArgs e)
        {
            Array.Sort(arrProvincies);
            cmbProvincie.Items.AddRange(arrProvincies);
        }

        private void cbEmailUsername_CheckedChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Gelieve eerst een E-mail in te vullen voor alleer deze te gebruiken als gebruikersnaam.", "E-mail not found.", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                txtGebruikersnaam.Text = txtEmail.Text;
            }
        }

        private void btnsluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
